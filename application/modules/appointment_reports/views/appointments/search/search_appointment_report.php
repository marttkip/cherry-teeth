       
        <section class="panel panel-featured panel-featured-info">
            <header class="panel-heading">
            	<h2 class="panel-title pull-right">Appointment List:</h2>
            	<h2 class="panel-title">Search</h2>
            </header>             

          <!-- Widget content -->
                <div class="panel-body">
			<?php
            echo form_open("reports/search_appointment_reports", array("class" => "form-horizontal"));
            ?>
            <div class="row">
                <div class="col-md-3">
                    
                    <div class="form-group">
                        <label class="col-lg-4 control-label">Date From: </label>
                        
                        <div class="col-lg-8">
                        	<div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </span>
                                <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="visit_date_from" placeholder="Visit Date From">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    
                    <div class="form-group">
                        <label class="col-lg-4 control-label">Date To: </label>
                        
                        <div class="col-lg-8">
                        	<div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </span>
                                <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="visit_date_to" placeholder="Visit Date To">
                            </div>
                        </div>
                    </div>
                    
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label class="col-lg-4 control-label">File No: </label>
                        
                        <div class="col-lg-8">
                            <input type="text" class="form-control" name="file_number" placeholder="File No">
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    
                    <div class="form-group">
                        <label class="col-lg-4 control-label">Dentist: </label>
                        
                        <div class="col-lg-8">
                        	<select id='doctor_id' name='doctor_id' class='form-control '>
                                <option value=''>None - Please a doctor</option>
                                <?php
												
									if(count($doctors) > 0){
										foreach($doctors as $row):
											$fname = $row->personnel_fname;
											$onames = $row->personnel_onames;
											$personnel_id = $row->personnel_id;
											
										
											echo "<option value='".$personnel_id."'>".$onames." ".$fname."</option>";
											
										endforeach;
									}
								?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-1">
                	<div class="form-group">
                        <div class="center-align">
                            <button type="submit" class="btn btn-info">Search Report</button>
                        </div>
                    </div>
                </div>
            </div>
            <br>
         
            	
           
            
            
            <?php
            echo form_close();
            ?>
          </div>
		</section>