<!-- search -->
<?php //echo $this->load->view('patients/search_apoointments', '', TRUE);?>
<!-- end search -->

 <section class="panel">
    <header class="panel-heading">
          <h4 class="pull-left"><i class="icon-reorder"></i>Appointments For</h4>
          <div class="widget-icons pull-right">
          
          </div>
          <div class="clearfix"></div>
        </header>
      <div class="panel-body">
          <div class="padd">
          
<?php
		$search = $this->session->userdata('appointment_search');
		
		if(!empty($search))
		{
			echo '<a href="'.site_url().'reception/close_appointments_search/'.$visit.'" class="btn btn-warning">Close Search</a>';
		}
		$result = '';
		
		//if users exist display them
		if ($query->num_rows() > 0)
		{
			$count = $page;
			
			$result .= 
				'
					<table class="table table-hover table-bordered ">
					  <thead>
						<tr>
						  <th>#</th>
						  <th>Name</th>
						  <th>Category</th>
						  <th>Number</th>						  
						  <th>Account</th>
						  <th>Appointment Date</th>
						  <th>Appointment Time</th>
						  <th>Appointment Period (D H:m:s)</th>
						  <th>Description</th>
						  <th>Status</th>
						</tr>
					  </thead>
					  <tbody>
				';
			
			// $personnel_query = $this->personnel_model->get_all_personnel();
			
			foreach ($query->result() as $res)
			{
				

				$visit_date = date('jS M Y',strtotime($res->appointment_date));
				$appointment_start_time = $res->appointment_start_time; 
				$appointment_end_time = $res->appointment_end_time; 
				$time_start = $res->appointment_date_time_start; 
				$time_end = $res->appointment_date_time_end;
				$patient_id = $res->patient_id;
				$patient_othernames = $res->patient_othernames;
				$patient_surname = $res->patient_surname;	
				$category_id = $res->category_id;	
				$patient_number = $res->patient_number;				
				$visit_id = $res->visit_id;
				$appointment_id = $res->appointment_id;
				$resource_id = $res->resource_id;
				$event_name = $res->event_name;
				$event_description = $res->event_description;
				$appointment_status = $res->appointment_status;
				$procedure_done = $res->procedure_done;
				$new_patient_number = '';//$res->new_patient_number;
				$uncategorised_patient_number = '';//$res->uncategorised_patient_number;
				$resource_id = $res->resource_id;
				$patient_data = $patient_surname.' '.$patient_othernames;
				$patient_phone1 = $res->patient_phone1;
				$patient_email = $res->patient_email;
				$visit_type_name = $res->visit_type_name;
				$time_in_clinic = NULL;//$res->time_in_clinic;
				$time_out_clinic = NULL;//$res->time_out_clinic;


				if(!empty($time_out_clinic) )
				{

					// var_dump($time_out_clinic);die();
					$time_out_clinic = date('H:i a',strtotime($res->time_out_clinic));
					$seconds = strtotime($res->time_out_clinic) - strtotime($res->time_in_clinic);//$row->waiting_time;
					$days    = floor($seconds / 86400);
					$hours   = floor(($seconds - ($days * 86400)) / 3600);
					$minutes = floor(($seconds - ($days * 86400) - ($hours * 3600))/60);
					$seconds = floor(($seconds - ($days * 86400) - ($hours * 3600) - ($minutes*60)));
					
					//$total_time = date('H:i',(strtotime($row->visit_time_out) - strtotime($row->visit_time)));//date('H:i',$row->waiting_time);
					$total_time = $days.' '.$hours.':'.$minutes.':'.$seconds;
				}
				else
				{
					$visit_time_out = '-';
					$total_time = '-';
				}
				

				if($appointment_status == 0)
				{
					$color = 'blue';
					$status_name = 'No show';
				}
				else if($appointment_status == 1)
				{
					$color = 'blue';
					$status_name = 'No show';
				}
				else if($appointment_status == 2)
				{
					$color = 'green';
					$status_name = 'Honoured';
				}
				else if($appointment_status == 3)
				{
					$color = 'red';
					$status_name = 'Cancelled';
				}
				else if($appointment_status == 4)
				{
					$color = 'purple';
					$status_name = 'Honoured';
				}
				else if($appointment_status == 5)
				{
					$color = 'black';
					$status_name = 'Not honoured';
				}
				else if($appointment_status == 6)
				{
					$color = 'DarkGoldenRod';
					$status_name = 'Notified';
				}
				else if($appointment_status == 7)
				{
					$color = 'purple';
					$status_name = 'Honoured';
				}
				else
				{
					$color = 'orange';
					$status_name = '';
				}


				if(empty($patient_data))
				{
					$patient_data = '';
				}
				if(empty($procedure_done))
				{
					$procedure_done = '';
				}
				

				if($category_id == 1)
				{
					// new patient
					$patient_number = $new_patient_number;
					$number_color = 'N.P';
					$buttons = '<td><a href="'.site_url().'reception/delete_patient/'.$patient_id.'" class="btn btn-sm btn-danger" onclick="return confirm(\'Are you sure you want to delete this patient details ? \')"><i class="fa fa-trash"></i></a></td>';
				}
				else if($category_id == 2)
				{
					$patient_number = $patient_number;
					$number_color = 'E.P';
					$buttons = '';
				}
				else
				{
					$patient_number = $uncategorised_patient_number;
					$number_color = 'U.P';
					$buttons = '';
				}
				$count++;
				
				$result .= 
				'
					<tr>
						<td>'.$count.'</td>
						<td>'.$patient_surname.'</td>
						<td>'.$number_color.'</td>
						<td>'.$patient_number.'</td>
						<td>'.$visit_type_name.'</td>
						<td>'.$visit_date.'</td>
						<td>'.$appointment_start_time.' - '.$appointment_end_time.'</td>
						<td>'.$total_time.'</td>
						<td>'.$event_description.'</td>
						<td style="background-color:'.$color.';color:white;">'.$status_name.'</td>
						
					</tr> 
				';
			}
			

			$result .= 
			'
						  </tbody>
						</table>
			';
		}
		
		else
		{
			$result .= "There are no appointment patients";
		}
		
		$error = $this->session->userdata('error_message');
		$success = $this->session->userdata('success_message');
		
		if(!empty($error))
		{
			echo '<div class="alert alert-danger">'.$error.'</div>';
			$this->session->unset_userdata('error_message');
		}
		
		if(!empty($success))
		{
			echo '<div class="alert alert-success">'.$success.'</div>';
			$this->session->unset_userdata('success_message');
		}

		echo $result;
		?>

          </div>
          
          <div class="widget-foot">
                                
				<?php if(isset($links)){echo $links;}?>
            
                <div class="clearfix"></div> 
            
            </div>
        </div>
        <!-- Widget ends -->
       

  </section>

  <script type="text/javascript">
	

	function check_date(visit_id){
	     var datess=document.getElementById("scheduledate"+visit_id).value;
	     var doctor_id=document.getElementById("doctor_id"+visit_id).value;

	    
	     if(datess && doctor_id){
	     	load_schedule(visit_id);
	     	load_patient_appointments_two(visit_id);
		  $('#show_doctor').fadeToggle(1000); return false;
		 }
		 else{
		  alert('Select Date and a Doctor First')
		 }
	}

	function load_schedule(visit_id){
		var config_url = $('#config_url').val();
		var datess=document.getElementById("scheduledate"+visit_id).value;
		var doctor= document.getElementById("doctor_id"+visit_id).value;

		var url= config_url+"reception/doc_schedule/"+doctor+"/"+datess;
		
		  $('#doctors_schedule'+visit_id).load(url);
		  $('#doctors_schedule'+visit_id).fadeIn(1000); return false;	
	}
	function load_patient_appointments(visit_id){
		var patient_id = $('#patient_id'+visit_id).val();
		var current_date = $('#current_date'+visit_id).val();

		var url= config_url+"reception/patient_schedule/"+patient_id+"/"+current_date;
		
		$('#patient_schedule'+visit_id).load(url);
		$('#patient_schedule'+visit_id).fadeIn(1000); return false;	

		$('#patient_schedule2'+visit_id).load(url);
		$('#patient_schedule2'+visit_id).fadeIn(1000); return false;	
	}
	function load_patient_appointments_two(visit_id){
		var patient_id = $('#patient_id'+visit_id).val();
		var current_date = $('#current_date'+visit_id).val();

		var url= config_url+"reception/patient_schedule/"+patient_id+"/"+current_date;
		
		$('#patient_schedule2'+visit_id).load(url);
		$('#patient_schedule2'+visit_id).fadeIn(1000); return false;	
	}
	function schedule_appointment(appointment_id)
	{
		if(appointment_id == '1')
		{
			$('#appointment_details').css('display', 'block');
		}
		else
		{
			$('#appointment_details').css('display', 'none');
		}
	}

	function update_appointment(visit_id,patient_id)
	{
		var config_url = document.getElementById("config_url").value;

        var data_url = config_url+"reception/update_appointment_accounts/"+patient_id+"/"+visit_id;

		var visit_date = $('#scheduledate'+visit_id).val();   
       	var doctor_id = $('#doctor_id'+visit_id).val(); 
       	var timepicker_start = $('#timepicker_start'+visit_id).val(); 
       	var timepicker_end = $('#timepicker_end'+visit_id).val();   
       	var procedure_done = $('#procedure_done'+visit_id).val(); 
       	var room_id = $('#room_id'+visit_id).val(); 
		$.ajax({
	    type:'POST',
	    url: data_url,
	    data:{visit_date: visit_date,doctor_id: doctor_id, timepicker_start: timepicker_start, timepicker_end: timepicker_end, procedure_done: procedure_done, room_id: room_id},
	    dataType: 'text',
	    success:function(data){

	    	window.location = config_url+'appointments';
	    },
	    error: function(xhr, status, error) {

	   		 alert(error);
	    }

	    });
	}
	function start_appointment_visit(visit_id)
	{
		var config_url = document.getElementById("config_url").value;

        var data_url = config_url+"reception/initiate_visit_appointment/"+visit_id;


		var insurance_description = $('#insurance_description'+visit_id).val();   
       	var insurance_limit = $('#insurance_limit'+visit_id).val(); 
       	var insurance_number = $('#insurance_number'+visit_id).val(); 
       	var mcc = $('#mcc'+visit_id).val();   
       	var visit_type_id = $('#visit_type_id2'+visit_id).val(); 

       	// alert(visit_type_id);
		$.ajax({
	    type:'POST',
	    url: data_url,
	    data:{insurance_limit: insurance_limit,insurance_description: insurance_description, insurance_number: insurance_number, mcc: mcc, visit_type_id: visit_type_id, visit_id: visit_id},
	    dataType: 'text',
	    success:function(data){

	    	var data = jQuery.parseJSON(data);
            
            var status = data.status;
            if(status == 1)
            {
	    		window.location = config_url+'queue';
            }
            else
            {

	    		window.location = config_url+'appointments';
            }
	    },
	    error: function(xhr, status, error) {

	   		 alert(error);
	    }

	    });
	}
	function get_visit_type(visit_id)
	{
		var visit_type_id = document.getElementById("visit_type_id2"+visit_id).value;
	
		
		if(visit_type_id != 1)
		{
			$('#insured_company2'+visit_id).css('display','block');
		}
		else
		{
			$('#insured_company2'+visit_id).css('display', 'none');
		}
		
		
	}
</script>