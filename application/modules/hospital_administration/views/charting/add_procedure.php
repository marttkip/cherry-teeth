<div class="row" style="margin-top:10px;">
	<div class="col-md-12">
		<div class="pull-right">
		 <a href="<?php echo site_url()?>administration/charting" class="btn btn-sm btn-primary"> Back to charting List </a>

		</div>
	</div>
</div>
<div class="row">
    <div class="col-md-12">
   
 <section class="panel">
    <header class="panel-heading">
                <h4 class="pull-left"><i class="icon-reorder"></i><?php echo $title;?></h4>
                <div class="widget-icons pull-right">
                  <a href="#" class="wminimize"><i class="icon-chevron-up"></i></a> 
                </div>
                <div class="clearfix"></div>
              </header>             

          <!-- Widget content -->
                <div class="panel-body">
          
			
            <div class="center-align">
              <?php
                $error = $this->session->userdata('error_message');
                $validation_error = validation_errors();
                $success = $this->session->userdata('success_message');
                
                if(!empty($error))
                {
                  echo '<div class="alert alert-danger">'.$error.'</div>';
                  $this->session->unset_userdata('error_message');
                }
                
                if(!empty($validation_error))
                {
                  echo '<div class="alert alert-danger">'.$validation_error.'</div>';
                }
                
                if(!empty($success))
                {
                  echo '<div class="alert alert-success">'.$success.'</div>';
                  $this->session->unset_userdata('success_message');
                }
              ?>
            </div>

            <?php echo form_open_multipart($this->uri->uri_string(), array("class" => "form-horizontal", "role" => "form"));?>
                 <div class="row">
                    <div class="col-md-5">
                        <div class="form-group">
                            <label class="col-lg-4 control-label">Procedure name</label>
                            <div class="col-lg-8">
                            	<input type="text" class="form-control" name="procedure_name" placeholder="Procedure Name">
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-5">
                         <div class="form-group">
                                <label class="col-lg-6 col-md-6  control-label">Procedure Image</label>
                                <div class="col-lg-6 col-md-6" >
                                    
                                    <div class="row">
                                    
                                        <div class="col-md-4 col-sm-4 col-xs-4">
                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width:200px; height:200px;">
                                                    <img src="http://placehold.it/200x200">
                                                </div>
                                                <div>
                                                    <span class="btn btn-file btn-info"><span class="fileinput-new">Select Image</span><span class="fileinput-exists">Change</span><input type="file" name="post_image"></span>
                                                    <a href="#" class="btn btn-danger fileinput-exists" data-dismiss="fileinput">Remove</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                    </div>
                     
                    <div class="col-md-2">
                        <div class="center-align">
                          <button type="submit" class="btn btn-info"> Add procedure</button>
                        </div>
                    </div>
                </div>
            	<?php echo form_close(); ?>
            
            </div>
        
		</section>
  </div>
</div>