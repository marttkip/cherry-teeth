<div class="row" style="margin-top:10px;">
	<div class="col-md-12">
		<div class="pull-right">
		 <a href="<?php echo site_url()?>administration/charting-notations/<?php echo $dental_procedure_id?>" class="btn btn-sm btn-primary"> Back to <?php echo $dental_procedure_name;?> </a>

		</div>
	</div>
</div>
<div class="row">
    <div class="col-md-12">
   
 <section class="panel">
    <header class="panel-heading">
                <h4 class="pull-left"><i class="icon-reorder"></i><?php echo $title;?></h4>
                <div class="widget-icons pull-right">
                  <a href="#" class="wminimize"><i class="icon-chevron-up"></i></a> 
                </div>
                <div class="clearfix"></div>
              </header>             

          <!-- Widget content -->
                <div class="panel-body">
          
			
            <div class="center-align">
              <?php
                $error = $this->session->userdata('error_message');
                $validation_error = validation_errors();
                $success = $this->session->userdata('success_message');
                
                if(!empty($error))
                {
                  echo '<div class="alert alert-danger">'.$error.'</div>';
                  $this->session->unset_userdata('error_message');
                }
                
                if(!empty($validation_error))
                {
                  echo '<div class="alert alert-danger">'.$validation_error.'</div>';
                }
                
                if(!empty($success))
                {
                  echo '<div class="alert alert-success">'.$success.'</div>';
                  $this->session->unset_userdata('success_message');
                }
              ?>
              <?php
            $res = $this->charting_model->get_charting_notation_data($notation_id);

                if(count($res) > 0){
                    foreach ($res as $key_res):
                          $tooth_id = $key_res->tooth_id;
                          $surface_id = $key_res->surface_id;
                          $image_connotation_red = $key_res->image_connotation_red;
                          $image_connotation_blue = $key_res->image_connotation_blue;
                          $image_connotation_green = $key_res->image_connotation_green;
                    endforeach;
                }
             ?>
            </div>

            <?php echo form_open_multipart($this->uri->uri_string(), array("class" => "form-horizontal", "role" => "form"));?>
                <div class="row">
                    <div class="col-md-6">
                          <div class="form-group">
                              <label class="col-lg-4 control-label">Tooth Number</label>
                              <div class="col-lg-8">
                                  <input type="text" class="form-control" name="tooth_id" placeholder="" value="<?php echo $tooth_id?>" >
                              </div>
                          </div>
                      </div>
                      <div class="col-md-6">
                         <div class="form-group">
                              <label class="col-lg-4 control-label">Surface</label>
                              <div class="col-lg-8">
                                  <input type="text" class="form-control" name="surface_id" placeholder="" value="<?php echo $surface_id?>" >
                              </div>
                          </div>
                       </div>
                       <div class="col-md-4">
                          <div class="form-group">
                                <label class="col-lg-6 col-md-6  control-label">Image  Treatment Plan (TP)</label>
                                <div class="col-lg-6 col-md-6" >
                                    
                                    <div class="row">
                                    	<input type="hidden" name="current_image_one" value="<?php echo $image_connotation_red;?>" />
                                    
                                        <div class="col-md-4 col-sm-4 col-xs-4">
                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width:200px; height:200px;">
                                                    <img src="<?php echo base_url()."assets/dentine/".$image_connotation_red;?>">
                                                </div>
                                                <div>
                                                    <span class="btn btn-file btn-info"><span class="fileinput-new">Select Image</span><span class="fileinput-exists">Change</span><input type="file" name="post_image_one"></span>
                                                    <a href="#" class="btn btn-danger fileinput-exists" data-dismiss="fileinput">Remove</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                      </div>
                      <div class="col-md-4">
                          <div class="form-group">
                                <label class="col-lg-6 col-md-6  control-label">Image  Completed (CP)</label>
                                <div class="col-lg-6 col-md-6" >
                                    
                                    <div class="row">
                                    	<input type="hidden" name="current_image_two" value="<?php echo $image_connotation_blue;?>" />
                                    
                                        <div class="col-md-4 col-sm-4 col-xs-4">
                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width:200px; height:200px;">
                                                    <img src="<?php echo base_url()."assets/dentine/".$image_connotation_blue;?>">
                                                </div>
                                                <div>
                                                    <span class="btn btn-file btn-info"><span class="fileinput-new">Select Image</span><span class="fileinput-exists">Change</span><input type="file" name="post_image_two"></span>
                                                    <a href="#" class="btn btn-danger fileinput-exists" data-dismiss="fileinput">Remove</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                      </div>
                      <div class="col-md-4">
                          <div class="form-group">
                                <label class="col-lg-6 col-md-6  control-label">Image Exam (EX) </label>
                                <div class="col-lg-6 col-md-6" >
                                    
                                    <div class="row">
                                      <input type="hidden" name="current_image_two" value="<?php echo $image_connotation_green;?>" />
                                    
                                        <div class="col-md-4 col-sm-4 col-xs-4">
                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width:200px; height:200px;">
                                                    <img src="<?php echo base_url()."assets/dentine/".$image_connotation_green;?>">
                                                </div>
                                                <div>
                                                    <span class="btn btn-file btn-info"><span class="fileinput-new">Select Image</span><span class="fileinput-exists">Change</span><input type="file" name="post_image_three"></span>
                                                    <a href="#" class="btn btn-danger fileinput-exists" data-dismiss="fileinput">Remove</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                      </div>
                </div>

                <div class="center-align" style="margin-top:10px;">
                  <button type="submit" class="btn btn-info">Edit <?php echo strtolower($dental_procedure_name);?> charge</button>
                </div>

            	<?php echo form_close(); ?>
            
            </div>
        
		</section>
  </div>
</div>