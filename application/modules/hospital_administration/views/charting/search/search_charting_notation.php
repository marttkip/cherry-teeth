
 <section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Search <?php echo $title;?></h2>
    </header>             

          <!-- Widget content -->
                <div class="panel-body">
          
			<?php echo form_open("hospital_administration/charting/charting_notations_search/".$dental_procedure_id, array("class" => "form-horizontal"));?>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="col-lg-4 control-label">Teeth No: </label>
                        
                        <div class="col-lg-8">
                          	 <input type="text" class="form-control" name="teeth_id" placeholder=" Service charge name">
                        </div>
                    </div>
                </div>
                
               
                
                <div class="col-md-4">
                    <div class="center-align">
                        <button type="submit" class="btn btn-info"><i class="fa fa-search"></i> Search</button>
                    </div>
                </div>
            </div>
            <?php
            echo form_close();
            ?>
            </div>
        
		</section>