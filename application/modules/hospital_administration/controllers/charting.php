<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once "./application/modules/hospital_administration/controllers/hospital_administration.php";

class charting extends Hospital_administration 
{
	var $csv_path;
	var $dental_path;
	function __construct()
	{
		parent:: __construct();
		
		$this->load->model('reception/reception_model');
		
		$this->load->model('charting_model');
		$this->load->model('admin/file_model');
		$this->load->library('image_lib');
		
		$this->csv_path = realpath(APPPATH . '../assets/csv');
		$this->dental_path = realpath(APPPATH . '../assets/dentine');
	}

	public function index($order = 'dental_procedure_name', $order_method = 'ASC')
	{
		//check if branch has parent
		$this->db->where('branch_code', $this->session->userdata('branch_code'));
		$query = $this->db->get('branch');
		$branch_code = $this->session->userdata('branch_code');
		
		if($query->num_rows() > 0)
		{
			$row = $query->row();
			$branch_parent = $row->branch_parent;
			
			if(!empty($branch_parent))
			{
				$branch_code = $branch_parent;
			}
		}


		// this is it
		//$where = 'dental_procedure_delete = 0 AND service.branch_code = "'.$branch_code.'"';
		$where = 'dental_procedure_delete = 0';
		$service_search = $this->session->userdata('service_search');
		
		if(!empty($service_search))
		{
			$where .= $service_search;
		}
		
		$segment = 5;
		$table = 'dental_procedure';
		//pagination
		$this->load->library('pagination');
		$config['base_url'] = site_url().'administration/charting/'.$order.'/'.$order_method;
		$config['total_rows'] = $this->reception_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->charting_model->get_all_charting($table, $where, $config["per_page"], $page, $order, $order_method);
		
		//change of order method 
		if($order_method == 'DESC')
		{
			$order_method = 'ASC';
		}
		
		else
		{
			$order_method = 'DESC';
		}
		
        $v_data["departments"] = $this->departments_model->all_departments();
		$v_data['order'] = $order;
		$v_data['order_method'] = $order_method;
		
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		
		$data['title'] = 'charting';
		$v_data['title'] = 'charting';
		$v_data['module'] = 0;
		
		$data['content'] = $this->load->view('charting/charting', $v_data, true);
		
		
		$data['sidebar'] = 'admin_sidebar';
		
		
		$this->load->view('admin/templates/general_page', $data);
		// end of it
	}
	
	public function charting_notations($dental_procedure_id)
	{
		// this is it
		$order = 'tooth_id';
		$order_method = 'ASC';
		$where = 'notation_delete = 0 AND dental_procedure.dental_procedure_id = charting_notations.dental_procedure_id  AND charting_notations.dental_procedure_id = '.$dental_procedure_id;
		$service_charge_search = $this->session->userdata('charting_notation_search');
		
		if(!empty($service_charge_search))
		{
			$where .= $service_charge_search;
			// var_dump($where);die();
		}
		
		$segment = 4;
		$table = 'charting_notations,dental_procedure';
		//pagination
		$this->load->library('pagination');
		$config['base_url'] = site_url().'administration/charting-notations/'.$dental_procedure_id;
		$config['total_rows'] = $this->reception_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->charting_model->get_all_charting_notations($table, $where, $config["per_page"], $page, $order, $order_method);
		// $v_data["department_id"] = $this->charting_model->get_department_id($service_id);
		
		//change of order method 
		if($order_method == 'DESC')
		{
			$order_method = 'ASC';
		}
		
		else
		{
			$order_method = 'DESC';
		}
		
		$v_data['order'] = $order;
		$v_data['order_method'] = $order_method;
		
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		$service_name = $this->charting_model->get_procedure_names($dental_procedure_id);
		$data['title'] = $v_data['title'] = $service_name.' charges';
		// $v_data['visit_types'] = $this->charting_model->get_visit_types();
		
		$v_data['dental_procedure_id'] = $dental_procedure_id;
		$v_data['dental_procedure_name'] = $service_name;
		$data['content'] = $this->load->view('charting/charting_notations', $v_data, true);
		
		$data['sidebar'] = 'admin_sidebar';
		
		
		$this->load->view('admin/templates/general_page', $data);
		// end of it
	}
	
	public function add_procedure()
	{
		// $this->form_validation->set_rules('department_id', 'Department', 'trim|xss_clean');
		$this->form_validation->set_rules('procedure_name', 'Dental name', 'trim|required|xss_clean');

		if ($this->form_validation->run())
		{
			$resize['width'] = 100;
			$resize['height'] = 100;
			
			if(is_uploaded_file($_FILES['post_image']['tmp_name']))
			{
				$posts_path = $this->dental_path;
				/*
					-----------------------------------------------------------------------------------------
					Upload image
					-----------------------------------------------------------------------------------------
				*/
				$response = $this->file_model->upload_file($posts_path, 'post_image', $resize);
				if($response['check'])
				{
					$file_name = $response['file_name'];
					$thumb_name = $response['thumb_name'];
				}
			
				else
				{
					$this->session->set_userdata('error_message', $response['error']);
					
				}
			}
			
			else{
				$file_name = '';
			}


			$result = $this->charting_model->submit_procedure($file_name);
			if($result == FALSE)
			{
				$this->session->set_userdata("error_message", "Unable to add this service. Please try again");
			}
			else
			{
				$this->session->set_userdata("success_message", "Successfully created a service ");
			}
			redirect('administration/charting');
		}
		
        // $v_data["departments"] = $this->departments_model->all_departments();
		
		$data['title'] = 'Add procedure';
		$v_data['title'] = 'Add procedure';
		$v_data['service_id'] = 0;
		$data['content'] = $this->load->view('charting/add_procedure',$v_data,TRUE);
		$this->load->view('admin/templates/general_page', $data);	
	}
	
	public function edit_procedure($procedure_id)
	{
		$this->form_validation->set_rules('procedure_name', 'Procedure name', 'trim|required|xss_clean');
		// $this->form_validation->set_message("is_unique", "A unique name is requred.");

		if ($this->form_validation->run())
		{

			$resize['width'] = 600;
			$resize['height'] = 800;

			$posts_path = $this->dental_path;
			
			
			//upload attachment
			$attachment_name = '';
			if(is_uploaded_file($_FILES['post_image']['tmp_name']))
			{
				//echo 'uploaded'; die();
				$posts_path = $this->dental_path;
				
				//delete original image
				$this->file_model->delete_file($posts_path."\\".$this->input->post('current_image'), $posts_path);
				/*
				/*
					-----------------------------------------------------------------------------------------
					Upload image
					-----------------------------------------------------------------------------------------
				*/
				$response = $this->file_model->upload_any_file($posts_path, 'post_image');
				if($response['check'])
				{
					$attachment_name = $response['file_name'];
				}
			
				else
				{
					$this->session->set_userdata('error_message', $response['error']);
				}
			}
			

			// var_dump($_FILES['post_image']['tmp_name']);die();

			$procedure_name = $this->input->post('procedure_name');
			$visit_data = array('dental_procedure_name'=>$procedure_name, 'image_conotation'=>$attachment_name);
			$this->db->where('dental_procedure_id',$procedure_id);
			if($this->db->update('dental_procedure', $visit_data))
			{
				$this->session->set_userdata("success_message", "Service updated successfully");
				
			}
			else
			{
				$this->session->set_userdata("error_message", "Sorry could not update service");
				
			}
			redirect('administration/charting');
			
			
		}
		
        // $v_data["departments"] = $this->departments_model->all_departments();
		
		$service_name = $this->charting_model->get_procedure_names($procedure_id);
		// $v_data['dept_id'] = $this->charting_model->get_service_department($service_id);
		$data['title'] = $v_data['title'] = 'Edit '.$service_name;
		
		$v_data['dental_procedure_id'] = $procedure_id;
		$v_data['procedure_name'] = $service_name;
		$data['content'] = $this->load->view('charting/edit_procedure',$v_data,TRUE);
		$this->load->view('admin/templates/general_page', $data);
	}
	
	public function add_charting_notation($dental_procedure_id)
	{
		$this->form_validation->set_rules('tooth_id', 'Service Surface', 'trim|required|xss_clean');
		$this->form_validation->set_rules('surface_id', 'Surface', 'trim|xss_clean');

		if ($this->form_validation->run())
		{

			$resize['width'] = 100;
			$resize['height'] = 100;
			
			if(is_uploaded_file($_FILES['post_image_one']['tmp_name']))
			{
				$posts_path = $this->dental_path;
				/*
					-----------------------------------------------------------------------------------------
					Upload image
					-----------------------------------------------------------------------------------------
				*/
				$response = $this->file_model->upload_file($posts_path, 'post_image_one', $resize);
				if($response['check'])
				{
					$file_name = $response['file_name'];
					$thumb_name = $response['thumb_name'];
				}
			
				else
				{
					$this->session->set_userdata('error_message', $response['error']);
					
				}
			}
			
			else{
				$file_name = '';
			}



			$resize['width'] = 100;
			$resize['height'] = 100;
			
			if(is_uploaded_file($_FILES['post_image_two']['tmp_name']))
			{
				$posts_path = $this->dental_path;
				/*
					-----------------------------------------------------------------------------------------
					Upload image
					-----------------------------------------------------------------------------------------
				*/
				$response = $this->file_model->upload_file($posts_path, 'post_image_two', $resize);
				if($response['check'])
				{
					$file_name_two = $response['file_name'];
					$thumb_name = $response['thumb_name'];
				}
			
				else
				{
					$this->session->set_userdata('error_message', $response['error']);
					
				}
			}
			
			else{
				$file_name_two = '';
			}



			$resize['width'] = 100;
			$resize['height'] = 100;
			
			if(is_uploaded_file($_FILES['post_image_three']['tmp_name']))
			{
				$posts_path = $this->dental_path;
				/*
					-----------------------------------------------------------------------------------------
					Upload image
					-----------------------------------------------------------------------------------------
				*/
				$response = $this->file_model->upload_file($posts_path, 'post_image_three', $resize);
				if($response['check'])
				{
					$file_name_three = $response['file_name'];
					$thumb_name = $response['thumb_name'];
				}
			
				else
				{
					$this->session->set_userdata('error_message', $response['error']);
					
				}
			}
			
			else{
				$file_name_three = '';
			}

			// var_dump(expression)
			$result = $this->charting_model->submit_charting_notation($dental_procedure_id,$file_name,$file_name_two,$file_name_three);
			if($result == FALSE)
			{
				$this->session->set_userdata("error_message", "Unable to add charting notations. Please try again");
			}
			else
			{
				$this->session->set_userdata("success_message","Successfully created a service charge");
			}
			redirect('administration/charting-notations/'.$dental_procedure_id);
		}
		
		$service_name = $this->charting_model->get_procedure_names($dental_procedure_id);
		$data['title'] = $v_data['title'] = 'Add '.$service_name.' notations';
		
		$v_data['dental_procedure_id'] = $dental_procedure_id;
		$v_data['dental_procedure_name'] = $service_name;
		$v_data['type'] = $this->reception_model->get_types();
		
		$data['content'] = $this->load->view('charting/add_charting_notation',$v_data,TRUE);
		$this->load->view('admin/templates/general_page', $data);	
	}
	
	public function edit_charting_notation($dental_procedure_id, $notation_id)
	{
		$this->form_validation->set_rules('tooth_id', 'Tooth id', 'trim|required|xss_clean');
		$this->form_validation->set_rules('surface_id', 'Surface Id', 'trim|xss_clean');

		if ($this->form_validation->run())
		{


			$resize['width'] = 100;
			$resize['height'] = 100;
			$file_name = '';
			if(is_uploaded_file($_FILES['post_image_one']['tmp_name']))
			{
				$posts_path = $this->dental_path;

				// $this->file_model->delete_file($posts_path."\\".$this->input->post('current_image_one'), $posts_path);
				/*
					-----------------------------------------------------------------------------------------
					Upload image
					-----------------------------------------------------------------------------------------
				*/
				$response = $this->file_model->upload_file($posts_path, 'post_image_one', $resize);
				if($response['check'])
				{
					$file_name = $response['file_name'];
					$thumb_name = $response['thumb_name'];
				}
			
				else
				{
					$this->session->set_userdata('error_message', $response['error']);
					
				}
			}
			
			else{
				$file_name = '';
			}



			$resize['width'] = 100;
			$resize['height'] = 100;

			$file_name_two = '';
			if(is_uploaded_file($_FILES['post_image_two']['tmp_name']))
			{
				$posts_path = $this->dental_path;

				// $this->file_model->delete_file($posts_path."\\".$this->input->post('current_image_two'), $posts_path);
				/*
					-----------------------------------------------------------------------------------------
					Upload image
					-----------------------------------------------------------------------------------------
				*/
				$response = $this->file_model->upload_file($posts_path, 'post_image_two', $resize);

				// var_dump($response);die();
				if($response['check'])
				{
					$file_name_two = $response['file_name'];
					$thumb_name = $response['thumb_name'];
				}
			
				else
				{
					$this->session->set_userdata('error_message', $response['error']);
					
				}
			}
			
			else{
				$file_name_two = '';
			}


			$resize['width'] = 100;
			$resize['height'] = 100;

			$file_name_three = '';
			if(is_uploaded_file($_FILES['post_image_three']['tmp_name']))
			{
				$posts_path = $this->dental_path;

				// $this->file_model->delete_file($posts_path."\\".$this->input->post('current_image_two'), $posts_path);
				/*
					-----------------------------------------------------------------------------------------
					Upload image
					-----------------------------------------------------------------------------------------
				*/
				$response = $this->file_model->upload_file($posts_path, 'post_image_three', $resize);

				// var_dump($response);die();
				if($response['check'])
				{
					$file_name_three = $response['file_name'];
					$thumb_name = $response['thumb_name'];
				}
			
				else
				{
					$this->session->set_userdata('error_message', $response['error']);
					
				}
			}
			
			else{
				$file_name_three = '';
			}
			// var_dump($_FILES['post_image_two']['tmp_name']);die();

			$surface_id = $this->input->post('surface_id');
			$tooth_id = $this->input->post('tooth_id');
			$surface_id = $this->input->post('surface_id');

			if(empty($surface_id))
			{
				$surface_id = 0;
			}
			
			$visit_data = array('tooth_id'=>$tooth_id,'surface_id'=>$surface_id);


			if(!empty($file_name_two))
			{
				$visit_data['image_connotation_blue'] =$file_name_two;
			}
			if(!empty($file_name_three))
			{
				$visit_data['image_connotation_green'] =$file_name_three;
			}

			if(!empty($file_name))
			{
				$visit_data['image_connotation_red'] =$file_name;
			}
			$this->db->where('notation_id',$notation_id);
			$this->db->update('charting_notations', $visit_data);
			
			
			
			$this->session->set_userdata("success_message","Successfully updated charting notations charge");
				
			redirect('administration/charting-notations/'.$dental_procedure_id);
		}
		
		$dental_procedure_name = $this->charting_model->get_procedure_names($dental_procedure_id);
		$data['title'] = $v_data['title'] = 'Edit '.$dental_procedure_name.' notations';
		
		$v_data['dental_procedure_id'] = $dental_procedure_id;
		$v_data['dental_procedure_name'] = $dental_procedure_name;
		$v_data['type'] = $this->reception_model->get_types();
		$v_data['notation_id'] = $notation_id;
		$data['content'] = $this->load->view('charting/edit_charting_notation',$v_data,TRUE);
		$this->load->view('admin/templates/general_page', $data);	
	}
	
	public function delete_charting_notation($notation_id,$dental_procedure_id)
	{
		// $visit_data = array('visit_charge_delete'=>1);
		$this->db->where(array("notation_id"=>$notation_id));
		$this->db->delete('charting_notations');

		redirect('administration/charting-notations/'.$dental_procedure_id);
	}


	public function delete_procedure($dental_procedure_id)
	{
		// $visit_data = array('visit_charge_delete'=>1);
		$this->db->where(array("dental_procedure_id"=>$dental_procedure_id));
		$this->db->delete('dental_procedure');

		$this->db->where(array("dental_procedure_id"=>$dental_procedure_id));
		$this->db->delete('charting_notations');
		
		redirect('administration/charting');
	}
	
	public function service_search()
	{
		$service_name = $this->input->post('service_name');
		$department_id = $this->input->post('department_id');
		
		if(!empty($service_name))
		{
			$service_name = ' AND service.service_name LIKE \'%'.$service_name.'%\' ';
		}
		else
		{
			$service_name = '';
		}
		
		if(!empty($department_id))
		{
			$department_id = ' AND service.department_id = \''.$department_id.'\' ';
		}
		else
		{
			$department_id = '';
		}
		
		$search = $service_name.$department_id;
		$this->session->set_userdata('service_search', $search);
		
		redirect('administration/charting');
	}
	
	public function close_service_search()
	{
		$this->session->unset_userdata('service_search');
		
		redirect('administration/charting');
	}
	
	public function charting_notations_search($dental_procedure_id)
	{
		$teeth_id = $this->input->post('teeth_id');
		
		if(!empty($teeth_id))
		{
			$teeth_id = ' AND charting_notations.tooth_id LIKE \'%'.$teeth_id.'%\' ';
		}
		else
		{
			$teeth_id = '';
		}
		
		
		// var_dump($teeth_id);die();
		$search = $teeth_id;

		$this->session->set_userdata('charting_notation_search', $search);
		
		redirect('administration/charting-notations/'.$dental_procedure_id);
	}
	
	public function close_charting_conotations_search($service_id)
	{
		$this->session->unset_userdata('charting_notation_search');
		
		redirect('administration/charting-notations/'.$service_id);
	}
	
	public function delete_service($service_id)
	{
		if($this->charting_model->delete_service($service_id))
		{
			$this->session->set_userdata('service_success_message', 'The service has been deleted successfully');

		}
		else
		{
			$this->session->set_userdata('service_error_message', 'The service could not be deleted');
		}
		
			redirect('administration/charting');
	}
	
	public function delete_service_charge($service_id, $service_charge_id)
	{
		if($this->charting_model->delete_service_charge($service_charge_id))
		{
			$this->session->set_userdata('success_message', 'The charge has been deleted successfully');

		}
		else
		{

			$this->session->set_userdata('error_message', 'The charge could not be deleted');
		}
		redirect('administration/service-charges/'.$service_id);
	}
    
	/*
	*
	*	Activate an existing service
	*	@param int $service_id
	*
	*/
	public function activate_service($service_id)
	{
		$this->charting_model->activate_service($service_id);
		$this->session->set_userdata('success_message', 'Service activated successfully');
		redirect('administration/charting');
	}
    
	/*
	*
	*	Deactivate an existing service
	*	@param int $service_id
	*
	*/
	public function deactivate_service($service_id)
	{
		$this->charting_model->deactivate_service($service_id);
		$this->session->set_userdata('success_message', 'Service disabled successfully');
		redirect('administration/charting');
	}
    
	/*
	*
	*	Activate an existing service_charge
	*	@param int $service_charge_id
	*
	*/
	public function activate_service_charge($service_id, $service_charge_id)
	{
		$this->charting_model->activate_service_charge($service_charge_id);
		$this->session->set_userdata('success_message', 'Charge activated successfully');
		redirect('administration/service-charges/'.$service_id);
	}
    
	/*
	*
	*	Deactivate an existing service_charge
	*	@param int $service_charge_id
	*
	*/
	public function deactivate_service_charge($service_id, $service_charge_id)
	{
		$this->charting_model->deactivate_service_charge($service_charge_id);
		$this->session->set_userdata('success_message', 'Charge disabled successfully');
		redirect('administration/service-charges/'.$service_id);
	}
	
	public function import_lab_charges($service_id)
	{
		if($this->charting_model->import_lab_charges($service_id))
		{
		}
		
		else
		{
		}
		
		redirect('administration/service-charges/'.$service_id);
	}
	
	public function import_bed_charges($service_id)
	{
		if($this->charting_model->import_bed_charges($service_id))
		{
		}
		
		else
		{
		}
		
		redirect('administration/service-charges/'.$service_id);
	}
	
	public function import_pharmacy_charges($service_id)
	{
		if($this->charting_model->import_pharmacy_charges($service_id))
		{
		}
		
		else
		{
		}
		
		redirect('administration/service-charges/'.$service_id);
	}
	
	function import_charges_template()
	{
		//export products template in excel 
		 $this->charting_model->import_charges_template();
	}
	
	function import_charges($service_id)
	{
		//open the add new product
		$v_data['service_id'] = $service_id;
		$v_data['title'] = 'Import Charges';
		$data['title'] = 'Import Charges';
		$data['content'] = $this->load->view('charting/import_charges', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}
	
	function do_charges_import($service_id)
	{
		if(isset($_FILES['import_csv']))
		{
			if(is_uploaded_file($_FILES['import_csv']['tmp_name']))
			{
				//import products from excel 
				$response = $this->charting_model->import_csv_charges($this->csv_path, $service_id);
				
				if($response == FALSE)
				{
				}
				
				else
				{
					if($response['check'])
					{
						$v_data['import_response'] = $response['response'];
					}
					
					else
					{
						$v_data['import_response_error'] = $response['response'];
					}
				}
			}
			
			else
			{
				$v_data['import_response_error'] = 'Please select a file to import.';
			}
		}
		
		else
		{
			$v_data['import_response_error'] = 'Please select a file to import.';
		}
		
		//open the add new product
		$v_data['service_id'] = $service_id;
		$v_data['title'] = 'Import Charges';
		$data['title'] = 'Import Charges';
		$data['content'] = $this->load->view('charting/import_charges', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}


	
}

?>