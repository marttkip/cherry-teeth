<?php


$journal_details = $this->transfer_model->get_journal_details($account_id);

if($journal_details->num_rows() > 0)
{
	foreach ($journal_details->result() as $key => $value) {
		# code...
		$account_from_id = $value->account_from_id;
		$account_to_id = $value->account_to_id;
		//$account_payment_description = $value->account_payment_description;
		$amount_paid = $value->amount_paid;
		$account_payment_description = $value->journal_entry_description;
		//$receipt_number = $value->receipt_number;
		$payment_date = $value->payment_date;
		$document_id = $value->document_number;
	}
}


?>

<div class="panel-body">
	<?php echo form_open_multipart($this->uri->uri_string(), array("class" => "form-horizontal", "role" => "form", "id"=>"edit-journal-data"));?>
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<label class="col-lg-4 control-label">Account to Debit</label>
				<div class="col-lg-8">
					<select id="account_from_id" name="account_from_id" class="form-control" onchange="get_accounty_type_list(this.value)" required>

						<?php
						// $changed = '<option value="">'.$account_to_id.'</option>';
						//$changed = '<option value="">-- Account --</option>';

						if($accounts->num_rows() > 0)
						{
							if($accounts->num_rows() > 0)
							{   
								foreach($accounts->result() as $row):
		                                        // $company_name = $row->company_name;
									$account_name = $row->account_name;
									$account_id = $row->account_id;

									if($account_id == $account_to_id)
									{
										echo "<option value=".$account_id." selected> ".$account_name."</option>";
									}
									else
									{
										echo "<option value=".$account_id."> ".$account_name."</option>";
									}


								endforeach; 
							} 
						}
						?>


					</select>
				</div>
			</div>

			<div class="form-group">
				<label class="col-lg-4 control-label">Transfer date: </label>

				<div class="col-lg-8">
					<div class="input-group">
						<span class="input-group-addon">
							<i class="fa fa-calendar"></i>
						</span>
						<input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="payment_date" placeholder="Transfer Date" value="<?php echo $payment_date;?>" id="datepicker2" required>
					</div>
				</div>
			</div>

		</div>
		<div class="col-md-6">

			<!-- Activate checkbox -->


			<div class="form-group">
				<label class="col-lg-4 control-label">Account to Credit: </label>

				<div class="col-lg-8">
					<select name="account_to_id" class="form-control select2" id="charge_to_id" required>

							<?php
						// $changed = '<option value="">'.$account_to_id.'</option>';
						//$changed = '<option value="">-- Account --</option>';

						if($accounts->num_rows() > 0)
						{
							if($accounts->num_rows() > 0)
							{   
								foreach($accounts->result() as $row):
		                                        // $company_name = $row->company_name;
									$account_name = $row->account_name;
									$account_id = $row->account_id;

									if($account_id == $account_from_id)
									{
										echo "<option value=".$account_id." selected> ".$account_name."</option>";
									}
									else
									{
										echo "<option value=".$account_id."> ".$account_name."</option>";
									}


								endforeach; 
							} 
						}
						?>

					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-lg-4 control-label">Amount *</label>
				<div class="col-lg-8">
					<input type="text" class="form-control" name="amount" placeholder="Amount" value="<?php echo $amount_paid;?>" required>
				</div>
			</div>
			<input type="hidden" value="<?php echo $document_id  ;?>" name="account_id">
			<div class="form-group">
				<label class="col-lg-4 control-label">Description *</label>
				<div class="col-lg-8">
					<textarea class="form-control" name="description" placeholder="Description" required="required" ><?php echo $account_payment_description;?></textarea>
				</div>
			</div>
			<div class="form-actions center-align">
				<button class="submit btn btn-primary btn-sm" type="submit" onclick="return confirm('Are you sure you want to perform this entry ? ')">
					Transfer
				</button>
			</div>
		</div>

	</div>
	<?php echo form_close();?>
</div>


<ul>
	<li style="margin-bottom: 5px;">
		<div class="row">
			<div class="col-md-12 center-align">
				<a  class="btn btn-sm btn-info" onclick="close_side_bar()"><i class="fa fa-folder-closed"></i> CLOSE SIDEBAR</a>
			</div>
		</div>
	</li>
</ul>