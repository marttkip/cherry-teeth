<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// require_once "./application/modules/administration/controllers/administration.php";
error_reporting(0);
class Reports extends MX_Controller
{	
	function __construct()
	{
		parent:: __construct();
		$this->load->model('reception/reception_model');
		$this->load->model('reports/reports_model');
		$this->load->model('accounts/accounts_model');
		$this->load->model('site/site_model');
		$this->load->model('admin/sections_model');
		$this->load->model('admin/dashboard_model');
		$this->load->model('admin/admin_model');
		$this->load->model('admin/email_model');
		$this->load->model('nurse/nurse_model');
		$this->load->model('reception/database');
		$this->load->model('administration/reports_model');
		$this->load->model('administration/personnel_model');
	}
	
	public function visit_report()
	{
		$where = 'visit.patient_id = patients.patient_id AND visit_type.visit_type_id = visit.visit_type AND visit.visit_delete = 0 AND visit.inpatient = 0 AND patients.rip_status =0 AND (visit.close_card = 0 OR visit.close_card = 2)';
		$table = 'visit, patients, visit_type';
		$visit_report_search = $this->session->userdata('visit_report_search');
		
		if(!empty($visit_report_search))
		{
			$where .= $visit_report_search;
		}
		else
		{
			// $where .= ' AND visit.visit_date = "'.date('Y-m-d').'"';
		}
		$segment = 3;
		//pagination
		$this->load->library('pagination');
		$config['base_url'] = site_url().'records/outpatient-report';
		$config['total_rows'] = $this->reception_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->reports_model->get_all_visits($table, $where, $config["per_page"], $page, 'ASC');
		
		$page_title = 'Visit Report'; 
		$data['title'] = $v_data['title'] = $page_title;
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		
		
		$data['content'] = $this->load->view('visit_report', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);

	}

	public function inpatient_report()
	{
		$where = 'visit.patient_id = patients.patient_id AND visit_type.visit_type_id = visit.visit_type AND visit.visit_delete = 0 AND visit.inpatient = 1 AND patients.rip_status =0 AND (visit.close_card = 0 OR visit.close_card = 2)';
		$table = 'visit, patients, visit_type';
		$inpatient_report_search = $this->session->userdata('inpatient_report_search');
		
		if(!empty($inpatient_report_search))
		{
			$where .= $inpatient_report_search;
		}
		else
		{
			// $where .= ' AND visit.visit_date = "'.date('Y-m-d').'"';

		}
		$segment = 3;
		//pagination
		$this->load->library('pagination');
		$config['base_url'] = site_url().'records/inpatient-report';
		$config['total_rows'] = $this->reception_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->reports_model->get_all_visits($table, $where, $config["per_page"], $page, 'ASC');
		
		$page_title = 'Visit Report'; 
		$data['title'] = $v_data['title'] = $page_title;
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		
		
		$data['content'] = $this->load->view('inpatient_report', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);

	}


	public function sick_off_report()
	{
		$where = 'patient_leave.visit_id = visit.visit_id AND patients.patient_id = visit.patient_id AND patient_leave.leave_type_id = leave_type.leave_type_id ';
		$table = 'patients,patient_leave,visit, leave_type';
		$sick_off_report_search = $this->session->userdata('sick_off_report_search');
		
		if(!empty($sick_off_report_search))
		{
			$where .= $sick_off_report_search;
		}
		else
		{
			$where .= ' AND patient_leave.start_date = "'.date('Y-m-d').'"';
		}
		//echo $where; die();
		$segment = 3;
		//pagination
		$this->load->library('pagination');
		$config['base_url'] = site_url().'records/sick-off-report';
		$config['total_rows'] = $this->reception_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->reports_model->get_all_visits_sick_offs($table, $where, $config["per_page"], $page, 'ASC');
		
		$page_title = 'Sick Off Report'; 
		$data['title'] = $v_data['title'] = $page_title;
		$v_data['query'] = $query;
		$v_data['page'] = $page;

		$department = $this->reports_model->get_all_departments();
		$departments = '';
		if($department->num_rows() > 0)
		{
			foreach ($department->result() as $department_test_rs):
				//var_dump($department_test_rs); die();
			  $department_name = $department_test_rs->department_name;
	
			  $departments .="<option value='".$department_name."'>".$department_name."</option>";
	
			endforeach;
		}
		
		$this->db->order_by('leave_type_name');
		$leave_types = $this->db->get('leave_type');
		$l_types = '';
		if($leave_types->num_rows() > 0)
		{
			foreach ($leave_types->result() as $rs):
				//var_dump($department_test_rs); die();
			  $leave_type_name = $rs->leave_type_name;
			  $leave_type_id = $rs->leave_type_id;
	
			  $l_types .="<option value='".$leave_type_id."'>".$leave_type_name."</option>";
	
			endforeach;
		}

		$v_data['l_types'] = $l_types;
		$v_data['departments'] = $departments;
		$data['content'] = $this->load->view('sick_off_report', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}

	public function search_visit_reports()
	{
		$visit_date_from = $this->input->post('visit_date_from');
		$visit_date_to = $this->input->post('visit_date_to');
		$visit_search_title ='';
		if(!empty($visit_date_from) && !empty($visit_date_to))
		{
			$visit_date = ' AND visit.visit_date BETWEEN \''.$visit_date_from.'\' AND \''.$visit_date_to.'\'';

			$visit_search_title = 'Visit From '.$visit_date_from.' To '.$visit_date_to.'';
		}
		
		else if(!empty($visit_date_from))
		{
			$visit_date = ' AND visit.visit_date = \''.$visit_date_from.'\'';
			$visit_search_title = 'Visit From '.$visit_date_from.' ';
		}
		
		else if(!empty($visit_date_to))
		{
			$visit_date = ' AND visit.visit_date = \''.$visit_date_to.'\'';
			$visit_search_title = 'Visit To '.$visit_date_to.'';
		}
		
		else
		{
			$visit_date = '';

		}
		
		$search = $visit_date;
		
		$this->session->set_userdata('visit_report_search', $search);
		$this->session->set_userdata('visit_title_search', $visit_search_title);
		
		redirect('records/outpatient-report');
	}
	public function search_inpatient_reports()
	{
		$visit_date_from = $this->input->post('visit_date_from');
		$visit_date_to = $this->input->post('visit_date_to');
		$visit_search_title ='';
		if(!empty($visit_date_from) && !empty($visit_date_to))
		{
			$visit_date = ' AND visit.visit_date BETWEEN \''.$visit_date_from.'\' AND \''.$visit_date_to.'\'';

			$visit_search_title = 'Visit From '.$visit_date_from.' To '.$visit_date_to.'';
		}
		
		else if(!empty($visit_date_from))
		{
			$visit_date = ' AND visit.visit_date = \''.$visit_date_from.'\'';
			$visit_search_title = 'Visit From '.$visit_date_from.' ';
		}
		
		else if(!empty($visit_date_to))
		{
			$visit_date = ' AND visit.visit_date = \''.$visit_date_to.'\'';
			$visit_search_title = 'Visit To '.$visit_date_to.'';
		}
		
		else
		{
			$visit_date = '';

		}
		
		$search = $visit_date;
		
		$this->session->set_userdata('inpatient_report_search', $search);
		$this->session->set_userdata('inpatient_title_search', $visit_search_title);
		
		redirect('records/inpatient-report');
	}

	public function search_sick_off_reports()
	{
		$payroll_number = $this->input->post('payroll_number');
		$leave_type_id = $this->input->post('leave_type_id');
		$department_name = $this->input->post('department_name');
		$visit_date_from = $this->input->post('visit_date_from');
		$visit_date_to = $this->input->post('visit_date_to');
		$visit_search_title = '';
		
		if(!empty($payroll_number))
		{
			$visit_search_title .= ' Payroll number '.$payroll_number;
			$payroll_number = ' AND patients.strath_no = \''.$payroll_number.'\'';
		}
		
		if(!empty($leave_type_id))
		{
			$this->db->where('leave_type_id', $leave_type_id);
			$query = $this->db->get('leave_type');
			$leave_type_name = '';
			if($query->num_rows() > 0)
			{
				$row = $query->row();
				$leave_type_name = $row->leave_type_name;
			}
			$visit_search_title .= ' Leave type '.$leave_type_name;
			$leave_type_id = ' AND patient_leave.leave_type_id = \''.$leave_type_id.'\'';
		}
		
		if(!empty($department_name))
		{
			$visit_search_title .= ' Department '.$department_name;
			$department_name = ' AND visit.department_name = \''.$department_name.'\'';
		}
		if(!empty($visit_date_from) && !empty($visit_date_to))
		{
			$visit_date = ' AND patient_leave.start_date >= \''.$visit_date_from.'\' AND patient_leave.start_date <= \''.$visit_date_to.'\'';

			$visit_search_title = 'Start Date From '.$visit_date_from.' To '.$visit_date_to.'';
		}
		
		else if(!empty($visit_date_from))
		{
			$visit_date = ' AND patient_leave.start_date = \''.$visit_date_from.'\'';
			$visit_search_title = 'Start From '.$visit_date_from.' ';
		}
		
		else if(!empty($visit_date_to))
		{
			$visit_date = ' AND patient_leave.start_date = \''.$visit_date_to.'\'';
			$visit_search_title = 'Start To '.$visit_date_to.'';
		}

		$search = $visit_date.$payroll_number.$department_name.$leave_type_id;

		$this->session->set_userdata('sick_off_report_search', $search);
		$this->session->set_userdata('sick_off_title_search', $visit_search_title);
		
		redirect('records/sick-off-report');
	}

	public function close_visit_search()
	{
		# code...
		$this->session->unset_userdata('visit_report_search');
		$this->session->unset_userdata('visit_title_search');

		redirect('records/outpatient-report');
	}
	public function close_inpatient_search()
	{
		# code...
		$this->session->unset_userdata('inpatient_report_search');
		$this->session->unset_userdata('inpatient_title_search');

		redirect('records/inpatient-report');
	}

	public function close_sick_off_search()
	{
		# code...
		$this->session->unset_userdata('sick_off_report_search');
		$this->session->unset_userdata('sick_off_title_search');

		redirect('records/sick-off-report');
	}

	public function print_visit_report()
	{

		$where = 'visit.patient_id = patients.patient_id AND visit_type.visit_type_id = visit.visit_type AND visit.visit_delete = 0 AND visit.inpatient = 0 AND patients.rip_status =0 AND (visit.close_card = 0 OR visit.close_card = 2)';
		$table = 'visit, patients, visit_type';
		$visit_report_search = $this->session->userdata('visit_report_search');
		
		if(!empty($visit_report_search))
		{
			$where .= $visit_report_search;
		
			if(!empty($table_search))
			{
				$table .= $table_search;
			}
			
		}
		else
		{
			// $where .= ' AND visit.visit_date = "'.date('Y-m-d').'"';
		}

		$query = $this->reports_model->get_all_visits_content($table, $where,'visit.visit_time' ,'ASC');


		$page_title = 'Visit Report'; 
		$data['title'] = $v_data['title'] = $page_title;
		$v_data['query'] = $query;

		$v_data['contacts'] = $this->site_model->get_contacts();
		
		$this->load->view('visit_report_print', $v_data);


	}
	public function print_inpatient_report()
	{

		$where = 'visit.patient_id = patients.patient_id AND visit_type.visit_type_id = visit.visit_type AND visit.visit_delete = 0 AND visit.inpatient = 1 AND patients.rip_status =0 AND (visit.close_card = 0 OR visit.close_card = 2)';
		$table = 'visit, patients, visit_type';
		$inpatient_report_search = $this->session->userdata('inpatient_report_search');
		
		if(!empty($inpatient_report_search))
		{
			$where .= $inpatient_report_search;
		
			if(!empty($table_search))
			{
				$table .= $table_search;
			}
			
		}
		else
		{
			// $where .= ' AND visit.visit_date = "'.date('Y-m-d').'"';
		}
		$query = $this->reports_model->get_all_visits_content($table, $where,'visit.visit_time' ,'ASC');


		$page_title = 'Inpatient Report'; 
		$data['title'] = $v_data['title'] = $page_title;
		$v_data['query'] = $query;

		$v_data['contacts'] = $this->site_model->get_contacts();
		
		$this->load->view('inpatient_report_print', $v_data);


	}

	public function print_sick_off_report()
	{
		$where = 'patient_leave.visit_id = visit.visit_id AND patients.patient_id = visit.patient_id   ';
		$table = 'patients, patient_leave,visit';
		$sick_off_report_search = $this->session->userdata('sick_off_report_search');
		
		if(!empty($sick_off_report_search))
		{
			$where .= $sick_off_report_search;
		}
		else
		{
			$where .= ' AND visit.visit_date = "'.date('Y-m-d').'"';
		}



		$query = $this->reports_model->get_all_sick_off_content($table, $where,'patient_leave.from_date' ,'ASC');


		$page_title = 'Sick Off Report'; 
		$data['title'] = $v_data['title'] = $page_title;
		$v_data['query'] = $query;

		$v_data['contacts'] = $this->site_model->get_contacts();
		
		$this->load->view('sick_off_report_print', $v_data);
	}
	
	public function leave_reports($order = 'patient_leave.start_date',$order_method = 'DESC')
	{
		$where = 'visit.patient_id = patients.patient_id AND visit.visit_id = patient_leave.visit_id AND patient_leave.leave_type_id = leave_type.leave_type_id';
		$table = 'visit, patients, patient_leave, leave_type';
		
		$leave_search = $this->session->userdata('leave_report_search');
		if(!empty($leave_search))
		{
			$where .= $leave_search;
		}
		else
		{
			$where .='  AND visit.visit_date = "'.date('Y-m-d').'"';
		}
		$segment = 5;
		//pagination
		$this->load->library('pagination');
		$config['base_url'] = site_url().'records/leave-reports/'.$order.'/'.$order_method;
		$config['total_rows'] = $this->reception_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->reports_model->get_all_patient_leave($table, $where, $config["per_page"], $page, $order, $order_method);
		
		$page_title = 'Patient Leave Report'; 
		$data['title'] = $v_data['title'] = $page_title;
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		
		
		$data['content'] = $this->load->view('patient_leave_report', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);

	}
	public function search_leave_reports()
	{
		$payroll_number = $this->input->post('payroll_number');
		$visit_date_from = $this->input->post('visit_date_from');
		$visit_date_to = $this->input->post('visit_date_to');
		
		if(!empty($payroll_number))
		{
			$payroll_number = ' AND patients.strath_no = \''.$payroll_number.'\'';
		}
		
		if(!empty($visit_date_from) && !empty($visit_date_to))
		{
			$visit_date = ' AND patient_leave.start_date >= \''.$visit_date_from.'\' AND patient_leave.end_date <= \''.$visit_date_to.'\'';
		}
		
		else if(!empty($visit_date_from))
		{
			$visit_date = ' AND patient_leave.start_date >= \''.$visit_date_from.'\'';
		}
		
		else if(!empty($visit_date_to))
		{
			$visit_date = ' AND patient_leave.end_date <= \''.$visit_date_to.'\'';
		}

		$search = $visit_date.$payroll_number;

		$this->session->set_userdata('leave_report_search', $search);
		redirect('records/leave-reports');
	}
	public function close_leave_search()
	{
		$this->session->unset_userdata('leave_report_search');
		redirect('records/leave-reports');
	}
	public function patient_statistics()
	{
	}


	public function rip_patients()
	{
		$where = 'rip_status = 1';
		$table = 'patients';
		$visit_report_search = $this->session->userdata('rip_patient_report');
		
		if(!empty($visit_report_search))
		{
			$where .= $visit_report_search;
		}
		else
		{
			// $where .= ' AND visit.visit_date = "'.date('Y-m-d').'"';
		}
		$segment = 3;
		//pagination
		$this->load->library('pagination');
		$config['base_url'] = site_url().'records/rip-report';
		$config['total_rows'] = $this->reception_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->reports_model->get_all_patient_rip($table, $where, $config["per_page"], $page, 'ASC');
		
		$page_title = "Patient's RIP Report"; 
		$data['title'] = $v_data['title'] = $page_title;
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		
		
		$data['content'] = $this->load->view('patients_report', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);

	}

	public function export_outpatient_report()
	{
		$this->reports_model->export_outpatient_report();
	}
	public function export_inpatient_report()
	{
		$this->reports_model->export_inpatient_report();
	}

	public function procedures_report()
	{

		$where = 'visit_charge.service_charge_id = service_charge.service_charge_id AND visit.visit_id = visit_charge.visit_id';
		$table = 'visit_charge,service_charge,visit';
		$visit_report_search = $this->session->userdata('procedure_report_search');
		
		if(!empty($visit_report_search))
		{
			$where .= $visit_report_search;
		}
		else
		{
			// $where .= ' AND visit.visit_date = "'.date('Y-m-d').'"';
		}
		$segment = 3;
		//pagination
		$this->load->library('pagination');
		$config['base_url'] = site_url().'management-reports/procedures-report';
		$config['total_rows'] = $this->reception_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->reports_model->get_all_procedures_visit($table, $where, $config["per_page"], $page, 'ASC');
		
		$page_title = 'Procedures Report'; 
		$data['title'] = $v_data['title'] = $page_title;
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		
		
		$data['content'] = $this->load->view('procedures_report', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);

	}

	public function search_procedures_report()
	{
		$visit_date_from = $this->input->post('visit_date_from');
		$visit_date_to = $this->input->post('visit_date_to');
		$procedure_name = $this->input->post('procedure_name');
		$visit_search_title ='';
		if(!empty($visit_date_from) && !empty($visit_date_to))
		{
			$visit_date = ' AND visit.visit_date BETWEEN \''.$visit_date_from.'\' AND \''.$visit_date_to.'\'';

			$visit_search_title = 'Visit From '.$visit_date_from.' To '.$visit_date_to.'';
		}
		
		else if(!empty($visit_date_from))
		{
			$visit_date = ' AND visit.visit_date = \''.$visit_date_from.'\'';
			$visit_search_title = 'Visit From '.$visit_date_from.' ';
		}
		
		else if(!empty($visit_date_to))
		{
			$visit_date = ' AND visit.visit_date = \''.$visit_date_to.'\'';
			$visit_search_title = 'Visit To '.$visit_date_to.'';
		}
		
		else
		{
			$visit_date = '';

		}
		

		if(!empty($procedure_name))
		{
			$procedure_name = ' AND service_charge.service_charge_name LIKE \'%'.$procedure_name.'%\'';
			$visit_search_title .= ' Procedure '.$procedure_name.'';
		}
		
		else
		{
			$procedure_name = '';

		}

		
		$search = $visit_date.$procedure_name;
		
		$this->session->set_userdata('procedure_report_search', $search);
		$this->session->set_userdata('procedure_title_search', $visit_search_title);
		
		redirect('management-reports/procedures-report');
	}

	public function close_procedures_search()
	{
		# code...
		$this->session->unset_userdata('procedure_report_search');
		$this->session->unset_userdata('procedure_title_search');

		redirect('management-reports/procedures-report');
	}

	public function export_procedures_report($service_charge_id = NULL)
	{
		$this->reports_model->export_procedures_report($service_charge_id);
	}
	public function export_visit_procedures_report($service_charge_id = NULL)
	{
		$this->reports_model->export_visit_procedures_report($service_charge_id);
	}
	public function general_report()
	{

		 
		
		$v_data['general_report'] = '';
		$data['content'] = $this->load->view('general_report', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}

	public function branch_report()
	{

		$branch_report_search = $this->session->userdata('branch_report_search');
		if(!empty($branch_report_search))
		{
			$combination = $this->session->userdata('year').'-'.$this->session->userdata('month').'-01';

			$first_day = date("Y-m-01", strtotime($combination));
			$last_day = date("Y-m-t", strtotime($combination));
		}
		else
		{

			$dt = date('Y-m-d');

			$first_day = date("Y-m-01", strtotime($dt));
			$last_day = date("Y-m-t", strtotime($dt));

		}
		
		$v_data['title'] = 'Branch Report for '.date('M Y', strtotime($first_day));
		$data['title'] = 'Branch Report '.date('M Y', strtotime($first_day));
		$branch_id = $this->session->userdata('branch_id');

		$v_data['first_day'] = $first_day;
		$v_data['last_day'] = $last_day;
		$v_data['branch_id'] = $branch_id;
		$data['content'] = $this->load->view('branch_report', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}

	public function search_branch_report()
	{
		$month_id = $this->input->post('month');
		$year = $this->input->post('year');

		$this->session->set_userdata('branch_report_search',TRUE);
		$this->session->set_userdata('year',$year);
		$this->session->set_userdata('month',$month_id);

		redirect('management-reports/branch-report');
	}
	public function close_branch_report()
	{
		$this->session->unset_userdata('branch_report_search');
		$this->session->unset_userdata('year');
		$this->session->unset_userdata('month');

		redirect('management-reports/branch-report');
	}
	public function print_branch_report()
	{
		$branch_report_search = $this->session->userdata('branch_report_search');
		if(!empty($branch_report_search))
		{
			$combination = $this->session->userdata('year').'-'.$this->session->userdata('month').'-01';

			$first_day = date("Y-m-01", strtotime($combination));
			$last_day = date("Y-m-t", strtotime($combination));
		}
		else
		{

			$dt = date('Y-m-d');

			$first_day = date("Y-m-01", strtotime($dt));
			$last_day = date("Y-m-t", strtotime($dt));

		}
		$branch_id = $this->session->userdata('branch_id');

		if($branch_id == 0)
		{
			$branch_id = NULL;
		}
		$v_data['contacts'] = $this->site_model->get_contacts($branch_id);
		
		$v_data['title'] = 'Branch Report for '.date('M Y', strtotime($first_day));
		$data['title'] = 'Branch Report '.date('M Y', strtotime($first_day));


		$v_data['first_day'] = $first_day;
		$v_data['last_day'] = $last_day;
		$v_data['branch_id'] = $branch_id;
		$this->load->view('print_branch_report', $v_data);

	}

	public function send_branch_report()
	{

		
		
		$branches = $this->site_model->get_all_branches();
		// var_dump($branches);die();
		if($branches->num_rows() > 0)
		{
			foreach ($branches->result() as $key => $value) {
				# code...
				$branch_id = $value->branch_id;
				$branch_name = $value->branch_name;
				$doctor_email = $value->doctor_email;

				$dt = date('2021-01-d');
				$first_day = date('Y-m-d', strtotime('first day of previous month'));//date("2021-01-01", strtotime($dt));
				$last_day = date('Y-m-d', strtotime('last day of previous month'));//date("2021-01-t", strtotime($dt));

				$v_data['contacts'] = $contacts = $this->site_model->get_contacts($branch_id);
				
				$v_data['title'] = $branch_name.' Report for '.date('M 2021', strtotime($first_day));
				$data['title'] =  $branch_name.' Report for '.date('M 2021', strtotime($first_day));


				$v_data['first_day'] = $first_day;
				$v_data['last_day'] = $last_day;
				$v_data['branch_id'] = $branch_id;

				// var_dump($v_data);die();
				$html = $this->load->view('print_branch_report', $v_data,true);
				// var_dump($html);die();

				$this->load->library('mpdf');
				
				$document_number = date("Ymdhis");
				$receipt_month_date = date('Y-m-d');
				$receipt_month = date('M',strtotime($receipt_month_date));
				$receipt_year = date('Y',strtotime($receipt_month_date));
				$title = $document_number.'-'.$data['title'].'.pdf';
				$invoice = $title;
				// echo $first_day;die();
				
				$mpdf=new mPDF();
				$mpdf->WriteHTML($html);
				$mpdf->Output($title, 'F');

				while(!file_exists($title))
				{

				}
				$message['text'] =$text;


				$sender_email = 'info@1809ltd.co.ke';//$this->config->item('sender_email');//$contacts['email'];
				$shopping = "";
				$from = $sender_email; 
				
				$button = '';
				$sender['email']= $sender_email; 
				$sender['name'] = $contacts['company_name'];
				$receiver['name'] = $subject;
				$payslip = $title;

				$sender_email = $sender_email;
				$tenant_email = 'marttkip@gmail.com/'.$doctor_email;//$this->config->item('recepients_email');
				$email_array = explode('/', $tenant_email);
				$total_rows_email = count($email_array);

				for($x = 0; $x < $total_rows_email; $x++)
				{
					$receiver['email'] = $email_tenant = $email_array[$x];


					$message['subject'] = $data['title'];
					$message['text'] = ' <p>Good evening <br>
											Please find attached report for the month 
											</p>
										';
					$shopping = ""; 
					
					$button = '';
					
					$this->email_model->send_sendgrid_mail($receiver, $sender, $message, $payslip);
					

				}
				
				unlink($payslip);

				// var_dump($payslip);die();

			}
		}
		
		$response['status'] = 'success';

		echo json_encode($response);
	}

	public function expenses_report()
	{


		$where = '(v_general_ledger_by_date.transactionClassification = "Purchases" OR v_general_ledger_by_date.transactionClassification = "Direct Payments" OR v_general_ledger_by_date.transactionClassification = "Creditors Invoices Payments")';
		$table = 'v_general_ledger_by_date';
		$visit_report_search = $this->session->userdata('export_payment_search');
		
		if(!empty($visit_report_search))
		{
			$where .= $visit_report_search;
		}
		else
		{
			$where .= ' AND MONTH(v_general_ledger_by_date.transactionDate) = "'.date('m').'" AND YEAR(v_general_ledger_by_date.transactionDate) = "'.date('Y').'"';
		}

		$branch_session = $this->session->userdata('branch_id');

	    if($branch_session > 0)
	    {
	      $where .= ' AND v_general_ledger_by_date.branch_id = '.$branch_session;
	    }
		$segment = 3;
		//pagination
		$this->load->library('pagination');
		$config['base_url'] = site_url().'management-reports/expenses-report';
		$config['total_rows'] = $this->reception_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->reports_model->get_expense_report($table, $where, $config["per_page"], $page, 'ASC');
		// var_dump($query);die();
		$page_title = 'Expense Report'; 

		$search_title = $this->session->userdata('expense_search_title');
		
		if(!empty($search_title))
		{
			$page_title = $search_title;
		}
		else
		{
			$page_title = 'Expenses for the month of '.date('M').' '.date('Y').'';
		}
		$data['title'] = $v_data['title'] = $page_title;
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		
		
		$data['content'] = $this->load->view('expenses_report', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}
	public function search_expenses_report()
	{
		$date_from = $this->input->post('date_from');
		$date_to = $this->input->post('date_to');
		

		$search_title = 'Expense report for: ';
		if(!empty($date_from) && !empty($date_to))
		{
			$date = ' AND v_general_ledger_by_date.transactionDate >= \''.$date_from.'\' AND v_general_ledger_by_date.transactionDate <= \''.$date_to.'\'';

			$search_title .= ' Date '.$date_from.' to '.$date_to.' ';
		}
		
		else if(!empty($date_from))
		{
			$date = ' AND v_general_ledger_by_date.transactionDate >= \''.$date_from.'\'';
			$search_title .= ' Date '.$date_from.' ';
		}
		
		else if(!empty($date_to))
		{
			$date = ' AND v_general_ledger_by_date.transactionDate <= \''.$date_to.'\'';
			$search_title .= ' Date '.$date_to.' ';
		}

		$search = $date;

		$this->session->set_userdata('export_payment_search', $search);
		$this->session->set_userdata('expense_search_title', $search_title);
		redirect('management-reports/expenses-report');
	}
	public function close_expense_search_report()
	{
		$this->session->unset_userdata('export_payment_search');
		$this->session->unset_userdata('expense_search_title');
		redirect('management-reports/expenses-report');
	}
	public function print_expenses_report()
	{
		$search = $this->session->userdata('export_payment_search');
		$search_title = $this->session->userdata('expense_search_title');
		
		if(!empty($search_title))
		{
			$title = $search_title;
		}
		else
		{
			$title = 'Expenses for the month of '.date('M').' '.date('Y').'';
		}
		$branch_id = $this->session->userdata('branch_id');
		$v_data['contacts'] = $this->site_model->get_contacts($branch_id);
		$v_data['title'] = $search_title;
		$this->load->view('print_expenses_report', $v_data);
	}
	public function export_expenses_report()
	{
		$this->reports_model->export_expense_transactions();
	}

	
}
?>