 <?php

$month = $this->reports_model->get_months();
$months_list = '<option value="0">Select a month</option>';
foreach($month->result() as $res)
{
  $month_id = $res->month_id;
  $month_name = $res->month_name;
  if($month_id < 10)
  {
    $month_id = '0'.$month_id;
  }
  $month = date('M');

  if($month == $month_name)
  {
    $months_list .= '<option value="'.$month_id.'" selected>'.$month_name.'</option>';
  }
  else {
    $months_list .= '<option value="'.$month_id.'">'.$month_name.'</option>';
  }



}


$start = 2020;
$end_year = 2030;
$year_list = '<option value="0">Select a Year</option>';
for ($i=$start; $i < $end_year; $i++) {
  // code...
  $year= date('Y');

  if($year == $i)
  {
    $year_list .= '<option value="'.$i.'" selected>'.$i.'</option>';
  }
  else {
    $year_list .= '<option value="'.$i.'">'.$i.'</option>';
  }
}


?>
        <section class="panel panel-featured panel-featured-info">
            <header class="panel-heading">
            	<h2 class="panel-title pull-right">Visit Search:</h2>
            	<h2 class="panel-title">Search</h2>
            </header>             

          <!-- Widget content -->
                <div class="panel-body">
			<?php
            echo form_open("reports/search_branch_report", array("class" => "form-horizontal"));
            ?>
            <div class="row">
            	
                <div class="col-md-4">
                    
                    <div class="form-group">
                        <label class="col-lg-4 control-label">Month: </label>
                        
                        <div class="col-lg-8">
                        	<select class="form-control" name="month" id="month">
                        		<?php echo $months_list;?>
                        	</select>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    
                    <div class="form-group">
                        <label class="col-lg-4 control-label">Year: </label>
                        
                        <div class="col-lg-8">
                        	<select class="form-control" name="year" id="year">
                        		<?php echo $year_list;?>
                        	</select>
                        </div>
                    </div>
                    
                </div>
                <div class="col-md-2">
              		<div class="form-group">
                          <div class="center-align">
                              <button type="submit" class="btn btn-info">Search Report</button>
                          </div>
                  	</div>
              		
              	</div>
                <div class="col-md-2">
                  <div class="form-group">
                          <div class="center-align">
                              <a href="<?php echo site_url().'print-branch-report'?>" target="_blank" class="btn btn-info">Print Report</a>
                          </div>
                    </div>
                  
                </div>
            </div>
          
            
            
            <?php
            echo form_close();
            ?>
          </div>
		</section>