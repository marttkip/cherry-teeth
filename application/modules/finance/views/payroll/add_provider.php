<?php
//provider data
$provider_name = set_value('provider_name');
$provider_email = set_value('provider_email');
$provider_phone = set_value('provider_phone');
$provider_location = set_value('provider_location');
$provider_building = set_value('provider_building');
$provider_floor = set_value('provider_floor');
$provider_address = set_value('provider_address');
$provider_post_code = set_value('provider_post_code');
$provider_city = set_value('provider_city');
$start_date = set_value('start_date');
$provider_contact_person_name = set_value('provider_contact_person_name');
$provider_contact_person_onames = set_value('provider_contact_person_onames');
$provider_contact_person_phone1 = set_value('provider_contact_person_phone1');
$provider_contact_person_phone2 = set_value('provider_contact_person_phone2');
$provider_contact_person_email = set_value('provider_contact_person_email');
$provider_description = set_value('provider_description');
$opening_balance  = set_value('opening_balance');
$balance_brought_forward = set_value('balance_brought_forward');
?>
           <section class="panel">
                <header class="panel-heading">
                    <h3 class="panel-title"><?php echo $title;?> </h3>
                    <div class="box-tools pull-right">
                        <a href="<?php echo site_url();?>accounting/providers" class="btn btn-sm btn-primary" ><i class="fa fa-arrow-left"></i> Back to provider bills</a>
                    </div>
                </header>

              <div class="panel-body">

                    <!-- Adding Errors -->
                    <?php
						$success = $this->session->userdata('success_message');
						$error = $this->session->userdata('error_message');

						if(!empty($success))
						{
							echo '
								<div class="alert alert-success">'.$success.'</div>
							';

							$this->session->unset_userdata('success_message');
						}

						if(!empty($error))
						{
							echo '
								<div class="alert alert-danger">'.$error.'</div>
							';

							$this->session->unset_userdata('error_message');
						}

						$validation_errors = validation_errors();

						if(!empty($validation_errors))
						{
							echo '<div class="alert alert-danger"> Oh snap! '.$validation_errors.' </div>';
						}

						$validation_errors = validation_errors();

						if(!empty($validation_errors))
						{
							echo '<div class="alert alert-danger"> Oh snap! '.$validation_errors.' </div>';
						}
                    ?>

                    <?php echo form_open($this->uri->uri_string(), array("class" => "form-horizontal", "role" => "form"));?>
<div class="row">
	<div class="col-md-6">

        <div class="form-group">
            <label class="col-lg-5 control-label">provider Name: </label>

            <div class="col-lg-7">
            	<input type="text" class="form-control" name="provider_name" placeholder="provider Name" value="<?php echo $provider_name;?>">
            </div>
        </div>

        <div class="form-group">
            <label class="col-lg-5 control-label">Email: </label>

            <div class="col-lg-7">
            	<input type="text" class="form-control" name="provider_email" placeholder="Email" value="<?php echo $provider_email;?>">
            </div>
        </div>

        <div class="form-group">
            <label class="col-lg-5 control-label">Phone: </label>

            <div class="col-lg-7">
            	<input type="text" class="form-control" name="provider_phone" placeholder="Phone" value="<?php echo $provider_phone;?>">
            </div>
        </div>
        <div class="form-group">
            <label class="col-lg-5 control-label">Opening Balance: </label>

            <div class="col-lg-7">
                <input type="text" class="form-control" name="opening_balance" placeholder="Opening Balance" value="<?php echo $opening_balance;?>">
            </div>
        </div>
        <div class="form-group">
			<label class="col-lg-5 control-label">Prepayment ?</label>
			<div class="col-lg-3">
				<div class="radio">
					<label>
					<input id="optionsRadios5" type="radio" value="1" name="debit_id">
					Yes
					</label>
				</div>
			</div>
			<div class="col-lg-3">
				<div class="radio">
					<label>
					<input id="optionsRadios6" type="radio" value="2" name="debit_id" checked="checked">
					No
					</label>
				</div>
			</div>
		</div>
      <div class="form-group">
        <label class="col-md-5 control-label">Start date: </label>

        <div class="col-md-7">
            <div class="input-group">
                <span class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                </span>
                <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="provider_account_date" placeholder="Transaction date" id="datepicker" value="<?php echo $start_date;?>" autocomplete="off">
            </div>
        </div>
      </div>
        <div class="form-group">
            <label class="col-lg-5 control-label">Location: </label>

            <div class="col-lg-7">
            	<input type="text" class="form-control" name="provider_location" placeholder="Location" value="<?php echo $provider_location;?>">
            </div>
        </div>

        <div class="form-group">
            <label class="col-lg-5 control-label">Building: </label>

            <div class="col-lg-7">
            	<input type="text" class="form-control" name="provider_building" placeholder="Building" value="<?php echo $provider_building;?>">
            </div>
        </div>


        <div class="form-group">
            <label class="col-lg-5 control-label">Floor: </label>

            <div class="col-lg-7">
            	<input type="text" class="form-control" name="provider_floor" placeholder="Floor" value="<?php echo $provider_floor;?>">
            </div>
        </div>

        <div class="form-group">
            <label class="col-lg-5 control-label">Address: </label>

            <div class="col-lg-7">
            	<input type="text" class="form-control" name="provider_address" placeholder="Address" value="<?php echo $provider_address;?>">
            </div>
        </div>

	</div>

    <div class="col-md-6">

        <div class="form-group">
            <label class="col-lg-5 control-label">Post code: </label>

            <div class="col-lg-7">
            	<input type="text" class="form-control" name="provider_post_code" placeholder="Post code" value="<?php echo $provider_post_code;?>">
            </div>
        </div>

        <div class="form-group">
            <label class="col-lg-5 control-label">City: </label>

            <div class="col-lg-7">
            	<input type="text" class="form-control" name="provider_city" placeholder="City" value="<?php echo $provider_city;?>">
            </div>
        </div>

        <div class="form-group">
            <label class="col-lg-5 control-label">Contact First Name: </label>

            <div class="col-lg-7">
            	<input type="text" class="form-control" name="provider_contact_person_name" placeholder="Contact First Name" value="<?php echo $provider_contact_person_name;?>">
            </div>
        </div>

        <div class="form-group">
            <label class="col-lg-5 control-label">Contact Other Names: </label>

            <div class="col-lg-7">
            	<input type="text" class="form-control" name="provider_contact_person_onames" placeholder="Contact Other Names" value="<?php echo $provider_contact_person_onames;?>">
            </div>
        </div>

        <div class="form-group">
            <label class="col-lg-5 control-label">Contact Phone 1: </label>

            <div class="col-lg-7">
            	<input type="text" class="form-control" name="provider_phone" placeholder="Contact Phone 1" value="<?php echo $provider_phone;?>">
            </div>
        </div>

        <div class="form-group">
            <label class="col-lg-5 control-label">Contact Phone 2: </label>

            <div class="col-lg-7">
            	<input type="text" class="form-control" name="provider_phone" placeholder="Contact Phone 2" value="<?php echo $provider_phone;?>">
            </div>
        </div>

        <div class="form-group">
            <label class="col-lg-5 control-label">Contact Email: </label>

            <div class="col-lg-7">
            	<input type="text" class="form-control" name="provider_contact_person_email" placeholder="Contact Email" value="<?php echo $provider_contact_person_email;?>">
            </div>
        </div>

    </div>
</div>

<div class="row" style="margin-top:10px;">
	<div class="col-md-12">

        <div class="form-group">
            <label class="col-lg-2 control-label">Description: </label>

            <div class="col-lg-9">
            	<textarea class="form-control" name="provider_description" rows="5"><?php echo $provider_phone;?></textarea>
            </div>
        </div>
    </div>
</div>

<div class="row" style="margin-top:10px;">
	<div class="col-md-12">
        <div class="form-actions text-center">
            <button class="submit btn btn-primary" type="submit">
                Add provider
            </button>
        </div>
    </div>
</div>
    <?php echo form_close();?>
</div>
</section>
