        <section class="panel panel-featured panel-featured-info">
            <header class="panel-heading">
            	<h2 class="panel-title pull-right">Active branch: <?php echo $branch_name;?></h2>
            	<h2 class="panel-title">Search</h2>
            </header>             

          <!-- Widget content -->
                <div class="panel-body">
			<?php
            echo form_open("administration/reports/search_cash_reports", array("class" => "form-horizontal"));
            ?>
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label class="col-lg-4 control-label">Payment Method: </label>
                        
                        <div class="col-lg-8">
                            <select class="form-control" name="payment_method_id">
                            	<option value="">---Select Payment Method---</option>
                                <?php
                                $total_cash_breakdown = 0;
                              
                                if($payment_methods->num_rows() > 0)
                                {
                                    foreach($payment_methods->result() as $res)
                                    {
                                        $method_name = $res->payment_method;
                                        $payment_method_id = $res->payment_method_id;
                                       
                                        
                                    
                                        echo 
                                        '
                                        <option value="'.$payment_method_id.'">'.$method_name.'</option>
                                        ';
                                    }
                                    
                                   
                                }
                              
                                ?>
                            </select>
                        </div>
                    </div>

                  
                </div>
                
                <div class="col-md-3">
                    
                    <div class="form-group">
                        <label class="col-lg-4 control-label"> Date From: </label>
                        
                        <div class="col-lg-8">
                        	<div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </span>
                                <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="visit_date_from" placeholder="Visit Date From" autocomplete="off">
                            </div>
                        </div>
                    </div>
                    
                    
                    
                </div>
                
                <div class="col-md-3">
                    
                    <div class="form-group">
                        <label class="col-lg-4 control-label"> Date To: </label>
                        
                        <div class="col-lg-8">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </span>
                                <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="visit_date_to" placeholder="Visit Date To" autocomplete="off">
                            </div>
                        </div>
                    </div>
                </div> 
                <div class="col-md-2">
                    
                    
                    
                    <div class="form-group">
                        <label class="col-lg-4 control-label">Name: </label>
                        
                        <div class="col-lg-8">
                            
                            <input type="text" name="patient_name" class="form-control" id="patient_name" placeholder="Patient Name">
                                
            
                        </div>
                    </div>
                    
                </div>

                <div class="col-md-1">  
                    
                    <div class="form-group">
                        <div class="col-lg-8 col-lg-offset-4">
                        	<div class="center-align">
                           		<button type="submit" class="btn btn-info">Search</button>
            				</div>
                        </div>
                    </div>
                </div>
            </div>
            
            
            <?php
            echo form_close();
            ?>
          </div>
		</section>