<?php

//patient details
$visit_type = $patient['visit_type'];
$patient_type = $patient['patient_type'];
$patient_othernames = $patient['patient_othernames'];
$patient_surname = $patient['patient_surname'];
$patient_surname = $patient['patient_surname'];
$patient_number = $patient['patient_number'];
$gender = $patient['gender'];
$patient_insurance_number = $patient['patient_insurance_number'];
$inpatient = $patient['inpatient'];
$visit_type_name = $patient['visit_type_name'];
$patient_date_of_birth = $patient['patient_date_of_birth'];

if(!empty($patient_date_of_birth) AND $patient_date_of_birth != "0000-00-00")
{
    $patient_age1 = $this->reception_model->calculate_age($patient_date_of_birth);
}
else
{
    $patient_age1 = '';

}

$today = date('jS F Y H:i a',strtotime(date("Y:m:d h:i:s")));
$visit_date = date('jS F Y',strtotime($this->accounts_model->get_visit_date($visit_id)));

//doctor
$doctor = $this->accounts_model->get_att_doctor($visit_id);

//served by
$served_by = $this->accounts_model->get_personnel($this->session->userdata('personnel_id'));


                   
?>


<!DOCTYPE html>
<html lang="en">
    <head>
        <title><?php echo $contacts['company_name'];?> | Sick Leave</title>
        <!-- For mobile content -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" media="all"/>
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css" media="all"/>
        <style type="text/css">
            .receipt_spacing{letter-spacing:0px; font-size: 12px;}
            .center-align{margin:0 auto; text-align:center;}
            
            .receipt_bottom_border{border-bottom: #888888 medium solid;}
            .row .col-md-12 table {
                border:solid #000 !important;
                border-width:1px 0 0 1px !important;
                font-size:15px;
            }
            .row .col-md-12 th, .row .col-md-12 td {
                border:solid #000 !important;
                border-width:0 1px 1px 0 !important;
            }
            .table thead > tr > th, .table tbody > tr > th, .table tfoot > tr > th, .table thead > tr > td, .table tbody > tr > td, .table tfoot > tr > td
            {
                 padding: 2px;
            }
            .col-md-4
            {
                width:32%;
            }
             .col-md-6
            {
                width:50%;
            }
            .col-print-1 {width:8%;  float:left;}
            .col-print-2 {width:16%; float:left;}
            .col-print-3 {width:25%; float:left;}
            .col-print-4 {width:33%; float:left;}
            .col-print-5 {width:42%; float:left;}
            .col-print-6 {width:50%; float:left;}
            .col-print-7 {width:58%; float:left;}
            .col-print-8 {width:66%; float:left;}
            .col-print-9 {width:75%; float:left;}
            .col-print-10{width:83%; float:left;}
            .col-print-11{width:92%; float:left;}
            .col-print-12{width:100%; float:left;}
            
            .row .col-md-12 .title-item{float:left;width: 130px; font-weight:bold; text-align:right; padding-right: 20px;}
            .title-img{float:left; padding-left:30px;}
            img.logo{max-height:70px; margin:0 auto;}

        </style>
    </head>
    <body class="receipt_spacing ">
        <!-- <div class="watermark" style="background:url(<?php echo base_url().'assets/logo/'.$contacts['logo'];?>) no-repeat;"></div> -->
        <!-- <div class="watermark"> -->
         <!--  <img src="<?php echo base_url().'assets/logo/'.$contacts['logo'];?>" width="100%" height="100%" style="position:fixed; opacity: 0.1;z-index: -1; position: fixed;" /> -->
        <!-- </div> -->
            <div class="content-view">
                
          
               <div class="row receipt_bottom_border" >
                    <div class="col-md-12" style="margin-left: 2px;">
                         <div class="col-print-4 ">
                             <img src="<?php echo base_url().'assets/logo/'.$contacts['logo'];?>" alt="<?php echo $contacts['company_name'];?>" class="img-responsive logo"  />
                        </div>
                        <div class="col-print-4">
                            &nbsp;
                        </div>
                        <div class="col-print-4">
                            <p style="font-size: 11px;margin-top:10px;">
                            
                                <?php echo $contacts['location'];?>, <?php echo $contacts['building'];?>, <?php echo $contacts['floor'];?><br/>
                                Address :<strong> P.O. Box <?php echo $contacts['address'];?> <?php echo $contacts['post_code'];?>, <?php echo $contacts['city'];?></strong> <br/>
                                Office Line: <strong> <?php echo $contacts['phone'];?></strong> <br/>
                                E-mail: <strong><?php echo $contacts['email'];?>.</strong><br/>
                            </p>
                        </div>
                       
                        
                    </div>
                </div>
              <div class="row receipt_bottom_border" >
                    <div class="col-md-12 center-align">
                        <h3>PRESCRIPTION FORM</h3>
                    </div>
                </div>
                
                <!-- Patient Details -->
                <div class="row " style="margin-bottom: 10px; padding-right: 20px;padding-left: 20px;">
                    <div class="col-md-12 pull-left">
                        <h4><strong >Patient's Name:</strong> 
                            <span style="text-decoration: dotted underline ;"><?php echo $patient_surname.' '.$patient_othernames; ?></span>
                        </h4>
                    </div>
                    
                    
                    <div class="col-md-12">
                       
                        <h4><strong>Card No. </strong><span style="text-decoration: dotted underline ;" > <?php echo $patient_number; ?> </span>
                            <strong>Date </strong> <span style="text-decoration: dotted underline ;"><?php echo $visit_date; ?></span>
                            <strong>Age </strong> <span style="text-decoration: dotted underline ;"><?php echo $patient_age1; ?></span>
                        </h4>
                           
                    </div>
                </div>
                <div class="row " style="padding-left:20px;padding-right: 20px;" >
                    <div class="col-md-12" style="padding:20px; border: 1px solid grey;">
                       <?php
                        $rs_pa = $this->nurse_model->get_prescription_notes_visit($visit_id);
                        $sick_leave_days = 0;
                        $sick_leave_start_date = date('Y-m-d');
                        $sick_leave_note ='';
                        if(count($rs_pa) >0){
                            foreach ($rs_pa as $r2):
                                # code...
                                $sick_leave_note = $r2->visit_prescription;
                                // get the visit charge

                            endforeach;

                        }
                        ?>
                         <h2 style="margin-top:0px"> Rx</h2>
                         <div style="margin-left:10px">
                             <h4><?php echo $sick_leave_note;?></h4>
                         </div>
                        
                    </div>
                </div>
                <?php
                $this->session->unset_userdata('selected_drugs');
                
                
                if($doctor == '-')
                {

                }
                else
                {
                ?>
                    <div class="row" style="font-style:bold; font-size:11px; padding-left: 20px;margin-top: 10px;">
                        <!-- <div class="col-md-12 pull-left"> -->
                            <div class="col-md-6 pull-left" style="border: 1px solid grey;">
                                <p>Approved By: </p>
                                <p><strong>Dr. <?php echo $doctor;?></strong></p>
                                <br>

                                <p>Sign : ................................ </p>
                            </div>
                        <!-- </div> -->
                    </div>
                <?php
                }
                ?>
                <div class="row" style="font-style:italic; font-size:11px; margin-top: 10px; ">
                    <div class="col-md-12 ">
                        <div class="col-md-6 pull-left">
                                Prepared by: <?php echo $served_by;?> 
                        </div>
                        <div class="col-md-6 pull-right">
                                <?php echo date('jS M Y H:i a'); ?> Thank you
                        </div>
                    </div>
                    
                </div>
            
        </div>
    </body>
    
</html>
