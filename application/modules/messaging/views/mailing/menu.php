<menu id="content-menu" class="inner-menu" role="menu">
	<div class="nano has-scrollbar">
		<div class="nano-content" tabindex="0" style="right: -17px;">

			<div class="inner-menu-toggle-inside">
				<a href="#" class="inner-menu-collapse">
					<i class="fa fa-chevron-up d-inline-block d-md-none"></i><i class="fa fa-chevron-left d-none d-md-inline-block"></i> Hide Menu
				</a>
				<a href="#" class="inner-menu-expand" data-open="inner-menu">
					Show Menu <i class="fa fa-chevron-down"></i>
				</a>
			</div>

			<div class="inner-menu-content">
				<hr class="separator">
				<a  class="btn btn-block btn-primary btn-md pt-2 pb-2 text-3" onclick="compose_email()">
					<i class="fa fa-envelope mr-1"></i>
					Compose Message
				</a>
				<br>
				<ul class="list-unstyled mt-4 pt-5">
					<li>
						<a  class="menu-item active" onclick="get_emails_view()">Outgoing Messages</a>
					</li>
					<!-- <li>
						<a href="" class="menu-item">Outgoing SMS's </a>
					</li> -->
				
				</ul>

				<!-- <hr class="separator"> -->

				<!-- <a href="mailbox-compose.html" class="btn btn-block btn-success btn-md pt-2 pb-2 text-3">
					<i class="fa fa-sms mr-1"></i>
					Compose SMS
				</a>
				<br>

				<ul class="list-unstyled mt-4 pt-5">
					<li>
						<a href="" class="menu-item active">Sent SMS </a>
					</li>

					<li>
						<a href="" class="menu-item">Unsent SMS </a>
					</li>
				</ul> -->

				

		
				<hr class="separator">

			
			</div>
		</div>
	<div class="nano-pane" style="opacity: 1; visibility: visible;"><div class="nano-slider" style="height: 400px; transform: translate(0px, 0px);"></div></div></div>
</menu>