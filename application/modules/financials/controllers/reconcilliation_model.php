<?php

class Reconcilliation_model extends CI_Model
{
	public function get_all_reconcilliations()
	{
		$this->db->where('bank_reconcilliation.account_id = account.account_id');
		$query = $this->db->get('bank_reconcilliation,account');

		return $query;
	}

	public function get_child_accounts($parent_account_name)
    {
    	$this->db->from('account');
		$this->db->select('*');
		$this->db->where('account_name = "'.$parent_account_name.'" AND account.account_status = 1');
		$query = $this->db->get();
		
		if($query->num_rows() > 0)  
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$account_id = $value->account_id;
			}
			//retrieve all users
			$this->db->from('account');
			$this->db->select('*');
			$this->db->where('parent_account = '.$account_id.' AND account.account_status = 1');
			$query = $this->db->get();
			
			return $query;    	


		}
		else
		{
			return FALSE;
		}

    }

    public function add_reconcilliation()
    {
    	$data['account_id'] = $this->input->post('account_id');
    	$data['recon_date'] = $this->input->post('recon_date');
    	$data['opening_balance'] = $this->input->post('opening_balance');
    	$data['interest_earned'] = $this->input->post('interest_earned');
    	$data['service_charged'] = $this->input->post('service_charged');
    	$data['interest_account_id'] = $this->input->post('interest_account_id');
    	$data['interest_date'] = $this->input->post('interest_date');
    	$data['charged_date'] = $this->input->post('charged_date');
    	$data['expense_account_id'] = $this->input->post('expense_account_id');
    	$data['created_by'] = $this->session->userdata('personnel_id');

    	if($this->db->insert('bank_reconcilliation',$data))
    	{
    		$recon_id = $this->db->insert_id();

    		return $recon_id;

    	}
    	else
    	{
    		return FALSE;
    	}

    }

    public function get_money_out($recon_id)
    {
 		$this->db->from('bank_reconcilliation');
		$this->db->select('*');
		$this->db->where('recon_id = '.$recon_id.'');
		$query = $this->db->get();
		
		if($query->num_rows() > 0)  
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$account_id = $value->account_id;
			}
		}


	



		$select_statement = "
							SELECT
								data.transactionDate AS payment_date,
								data.referenceCode AS cheque,
								data.transactionDescription AS payee,
								data.amount_paid AS amount,
								data.transaction_type AS type,
								data.transactionid AS transactionid,
								data.accountId AS account_id,
								data.recon_id AS recon_id
								FROM (
										SELECT
											`creditor_payment`.`creditor_payment_id` AS `transactionid`,
											`creditor_payment`.`reference_number` AS `referenceCode`,
										  	`creditor_payment`.`creditor_id` AS `recepientId`,
											`creditor_payment`.`account_from_id` AS `accountId`,
											`account`.`account_name` AS `accountName`,
											CONCAT(`creditor`.`creditor_name`)  AS `transactionDescription`,
											`creditor_payment`.`total_amount` AS `amount_paid`,
											`creditor_payment`.`transaction_date` AS `transactionDate`,
											1 AS `transaction_type`,
											creditor_payment.recon_id AS `recon_id`
										FROM
											(
												(
													(
													 creditor_payment
														
													)
													JOIN account ON(
														(
															account.account_id = creditor_payment.account_from_id
														)
													)
												)
												JOIN `account_type` ON(
													(
														account_type.account_type_id = account.account_type_id
													)
												)
												JOIN `creditor` ON(
													(
														creditor.creditor_id = creditor_payment.creditor_id
													)
												)
											)
										WHERE creditor_payment.creditor_payment_status = 1
										AND creditor_payment.account_from_id = $account_id

										UNION ALL

										SELECT
											`account_payments`.`account_payment_id` AS `transactionid`,
											`account_payments`.`receipt_number` AS `referenceCode`,
										  	`account_payments`.`account_to_id` AS `recepientId`,
											`account_payments`.`account_from_id` AS `accountId`,
											`account`.`account_name` AS `accountName`,
											'Direct Payment'  AS `transactionDescription`,
											`account_payments`.`amount_paid` AS `amount_paid`,
											`account_payments`.`payment_date` AS `transactionDate`,
											2 AS `transaction_type`,
											account_payments.recon_id AS `recon_id`
										FROM
											(
												(
													(
													 account_payments
														
													)
													JOIN account ON(
														(
															account.account_id = account_payments.account_from_id
														)
													)
												)
												JOIN `account_type` ON(
													(
														account_type.account_type_id = account.account_type_id
													)
												)
												
											)
										WHERE account_payments.account_payment_deleted = 0 
										AND account_payments.account_from_id = $account_id

										UNION ALL

										SELECT
											`finance_transfer`.`finance_transfer_id` AS `transactionid`,
											`finance_transfer`.`reference_number` AS `referenceCode`,
										  	`finance_transfered`.`account_to_id` AS `recepientId`,
											`finance_transfer`.`account_from_id` AS `accountId`,
											`account`.`account_name` AS `accountName`,
											'Account Transfers'  AS `transactionDescription`,
											`finance_transfer`.`finance_transfer_amount` AS `amount_paid`,
											`finance_transfer`.`transaction_date` AS `transactionDate`,
											3 AS `transaction_type`,
											finance_transfer.recon_id AS `recon_id`
										FROM
											(
												(
													(
													 finance_transfer,finance_transfered
														
													)
													JOIN account ON(
														(
															account.account_id = finance_transfer.account_from_id
														)
													)
												)
												JOIN `account_type` ON(
													(
														account_type.account_type_id = account.account_type_id
													)
												)
												
											)
										WHERE finance_transfer.finance_transfer_deleted = 0 AND finance_transfer.finance_transfer_id = finance_transfered.finance_transfer_id AND finance_transfer.account_from_id = $account_id

									

									) AS data  ";
		
		$query = $this->db->query($select_statement);

		return $query;


    }


     public function get_money_in($recon_id)
    {
    	$this->db->from('bank_reconcilliation');
		$this->db->select('*');
		$this->db->where('recon_id = '.$recon_id.'');
		$query = $this->db->get();
		
		if($query->num_rows() > 0)  
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$account_id = $value->account_id;
			}
		}

	



		$select_statement = "
							SELECT
								data.transactionDate AS payment_date,
								data.referenceCode AS cheque,
								data.transactionDescription AS payee,
								data.amount_paid AS amount,
								data.transaction_type AS type,
								data.transactionid AS transactionid,
								data.accountId AS account_id,
								data.recon_id AS recon_id
								FROM (
										

										SELECT
											`finance_transfered`.`finance_transfered_id` AS `transactionid`,
											`finance_transfer`.`reference_number` AS `referenceCode`,
										  	`finance_transfer`.`account_from_id` AS `recepientId`,
											`finance_transfered`.`account_to_id` AS `accountId`,
											`account`.`account_name` AS `accountName`,
											'Money Transfer'  AS `transactionDescription`,
											`finance_transfer`.`finance_transfer_amount` AS `amount_paid`,
											`finance_transfer`.`transaction_date` AS `transactionDate`,
											1 AS `transaction_type`,
											finance_transfered.recon_id AS `recon_id`
										FROM
											(
												(
													(
													 finance_transfer,finance_transfered
														
													)
													JOIN account ON(
														(
															account.account_id = finance_transfered.account_to_id
														)
													)
												)
												JOIN `account_type` ON(
													(
														account_type.account_type_id = account.account_type_id
													)
												)
												
											)
										WHERE finance_transfer.finance_transfer_deleted = 0 AND finance_transfer.finance_transfer_id = finance_transfered.finance_transfer_id  AND finance_transfered.account_to_id = $account_id

										UNION ALL

										SELECT
											`journal_entry`.`journal_entry_id` AS `transactionid`,
											`journal_entry`.`document_number` AS `referenceCode`,
										  	`journal_entry`.`account_from_id` AS `recepientId`,
											`journal_entry`.`account_to_id` AS `accountId`,
											`account`.`account_name` AS `accountName`,
											'Capital Injection'  AS `transactionDescription`,
											`journal_entry`.`amount_paid` AS `amount_paid`,
											`journal_entry`.`payment_date` AS `transactionDate`,
											2 AS `transaction_type`,
											journal_entry.recon_id AS `recon_id`
										FROM
											(
												(
													(
													 journal_entry
														
													)
													JOIN account ON(
														(
															account.account_id = journal_entry.account_to_id
														)
													)
												)
												JOIN `account_type` ON(
													(
														account_type.account_type_id = account.account_type_id
													)
												)
												
											)
										WHERE journal_entry.journal_entry_deleted = 0  AND journal_entry.account_to_id = $account_id
									

									) AS data";
		
		$query = $this->db->query($select_statement);

		return $query;


    }

    public function get_recon_details($recon_id)
    {
    	$this->db->from('bank_reconcilliation,account');
		$this->db->select('*');
		$this->db->where('bank_reconcilliation.account_id = account.account_id AND recon_id = '.$recon_id.'');
		$query = $this->db->get();
		
		return $query;
    }


    public function get_money_out_total($recon_id)
    {
 		$this->db->from('bank_reconcilliation');
		$this->db->select('*');
		$this->db->where('recon_id = '.$recon_id.'');
		$query = $this->db->get();
		
		if($query->num_rows() > 0)  
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$account_id = $value->account_id;
			}
		}


	



		$select_statement = "
							SELECT
								data.transactionDate AS payment_date,
								data.referenceCode AS cheque,
								data.transactionDescription AS payee,
								SUM(data.amount_paid) AS amount,
								data.transaction_type AS type,
								data.transactionid AS transactionid,
								data.accountId AS account_id
								FROM (
										SELECT
											`creditor_payment`.`creditor_payment_id` AS `transactionid`,
											`creditor_payment`.`reference_number` AS `referenceCode`,
										  	`creditor_payment`.`creditor_id` AS `recepientId`,
											`creditor_payment`.`account_from_id` AS `accountId`,
											`account`.`account_name` AS `accountName`,
											CONCAT(`creditor`.`creditor_name`)  AS `transactionDescription`,
											SUM(`creditor_payment`.`total_amount`) AS `amount_paid`,
											`creditor_payment`.`transaction_date` AS `transactionDate`,
											1 AS `transaction_type`
										FROM
											(
												(
													(
													 creditor_payment
														
													)
													JOIN account ON(
														(
															account.account_id = creditor_payment.account_from_id
														)
													)
												)
												JOIN `account_type` ON(
													(
														account_type.account_type_id = account.account_type_id
													)
												)
												JOIN `creditor` ON(
													(
														creditor.creditor_id = creditor_payment.creditor_id
													)
												)
											)
										WHERE creditor_payment.creditor_payment_status = 1
										AND creditor_payment.account_from_id = $account_id AND creditor_payment.recon_id = $recon_id 

										UNION ALL

										SELECT
											`account_payments`.`account_payment_id` AS `transactionid`,
											`account_payments`.`receipt_number` AS `referenceCode`,
										  	`account_payments`.`account_to_id` AS `recepientId`,
											`account_payments`.`account_from_id` AS `accountId`,
											`account`.`account_name` AS `accountName`,
											'Direct Payment'  AS `transactionDescription`,
											SUM(`account_payments`.`amount_paid`) AS `amount_paid`,
											`account_payments`.`payment_date` AS `transactionDate`,
											2 AS `transaction_type`
										FROM
											(
												(
													(
													 account_payments
														
													)
													JOIN account ON(
														(
															account.account_id = account_payments.account_from_id
														)
													)
												)
												JOIN `account_type` ON(
													(
														account_type.account_type_id = account.account_type_id
													)
												)
												
											)
										WHERE account_payments.account_payment_deleted = 0 
										AND account_payments.account_from_id = $account_id AND account_payments.recon_id = $recon_id 

										UNION ALL

										SELECT
											`finance_transfer`.`finance_transfer_id` AS `transactionid`,
											`finance_transfer`.`reference_number` AS `referenceCode`,
										  	`finance_transfered`.`account_to_id` AS `recepientId`,
											`finance_transfer`.`account_from_id` AS `accountId`,
											`account`.`account_name` AS `accountName`,
											'Account Transfers'  AS `transactionDescription`,
											SUM(`finance_transfer`.`finance_transfer_amount`) AS `amount_paid`,
											`finance_transfer`.`transaction_date` AS `transactionDate`,
											2 AS `transaction_type`
										FROM
											(
												(
													(
													 finance_transfer,finance_transfered
														
													)
													JOIN account ON(
														(
															account.account_id = finance_transfer.account_from_id
														)
													)
												)
												JOIN `account_type` ON(
													(
														account_type.account_type_id = account.account_type_id
													)
												)
												
											)
										WHERE finance_transfer.finance_transfer_deleted = 0 AND finance_transfer.finance_transfer_id = finance_transfered.finance_transfer_id AND finance_transfer.account_from_id = $account_id AND finance_transfer.recon_id = $recon_id 

									

									) AS data  ";
		
		$query = $this->db->query($select_statement);

		$amount = 0;

		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$amount = $value->amount;
			}
		}

		// var_dump($amount);die();

		return $amount;


    }

    public function get_money_in_total($recon_id)
    {


    	$this->db->from('bank_reconcilliation');
		$this->db->select('*');
		$this->db->where('recon_id = '.$recon_id.'');
		$query = $this->db->get();
		
		if($query->num_rows() > 0)  
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$account_id = $value->account_id;
			}
		}

	



		$select_statement = "
							SELECT
								data.transactionDate AS payment_date,
								data.referenceCode AS cheque,
								data.transactionDescription AS payee,
								SUM(data.amount_paid) AS amount,
								data.transaction_type AS type,
								data.transactionid AS transactionid,
								data.accountId AS account_id
								FROM (
										

										SELECT
											`finance_transfer`.`finance_transfer_id` AS `transactionid`,
											`finance_transfer`.`reference_number` AS `referenceCode`,
										  	`finance_transfer`.`account_from_id` AS `recepientId`,
											`finance_transfered`.`account_to_id` AS `accountId`,
											`account`.`account_name` AS `accountName`,
											'Money Transfer'  AS `transactionDescription`,
											SUM(`finance_transfer`.`finance_transfer_amount`) AS `amount_paid`,
											`finance_transfer`.`transaction_date` AS `transactionDate`,
											1 AS `transaction_type`
										FROM
											(
												(
													(
													 finance_transfer,finance_transfered
														
													)
													JOIN account ON(
														(
															account.account_id = finance_transfered.account_to_id
														)
													)
												)
												JOIN `account_type` ON(
													(
														account_type.account_type_id = account.account_type_id
													)
												)
												
											)
										WHERE finance_transfer.finance_transfer_deleted = 0 AND finance_transfer.finance_transfer_id = finance_transfered.finance_transfer_id  AND finance_transfered.account_to_id = $account_id AND finance_transfered.recon_id = $recon_id 

										UNION ALL

										SELECT
											`journal_entry`.`journal_entry_id` AS `transactionid`,
											`journal_entry`.`document_number` AS `referenceCode`,
										  	`journal_entry`.`account_from_id` AS `recepientId`,
											`journal_entry`.`account_to_id` AS `accountId`,
											`account`.`account_name` AS `accountName`,
											'Capital Injection'  AS `transactionDescription`,
											SUM(`journal_entry`.`amount_paid`) AS `amount_paid`,
											`journal_entry`.`payment_date` AS `transactionDate`,
											2 AS `transaction_type`
										FROM
											(
												(
													(
													 journal_entry
														
													)
													JOIN account ON(
														(
															account.account_id = journal_entry.account_to_id
														)
													)
												)
												JOIN `account_type` ON(
													(
														account_type.account_type_id = account.account_type_id
													)
												)
												
											)
										WHERE journal_entry.journal_entry_deleted = 0  AND journal_entry.account_to_id = $account_id AND journal_entry.recon_id = $recon_id 
									

									) AS data";
		
		$query = $this->db->query($select_statement);


		$amount = 0;

		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$amount = $value->amount;
			}
		}

		// var_dump($amount);die();

		return $amount;
    }

    function get_all_accounts($account_id=NULL)
	{
		
		 if(!empty($account_id))
	      {
	        $add = ' AND account_id <> '.$account_id;
	      }
	      else
	      {
	        $add ='';
	      }
	      $this->db->from('account');
	      $this->db->select('*');
	      $this->db->where('parent_account <> 0'.$add);
	      $this->db->order_by('parent_account','ASC');
	      $query = $this->db->get();

       return $query;
	}
	public function get_account_name($from_account_id)
	{
	$account_name = '';
	$this->db->select('account_name');
	$this->db->where('account_id = '.$from_account_id);
	$query = $this->db->get('account');

	$account_details = $query->row();
	$account_name = $account_details->account_name;

	return $account_name;
	}
}
?>