<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once "./application/modules/admin/controllers/admin.php";
// error_reporting(0);
class Payroll extends admin
{
	function __construct()
	{
		parent:: __construct();

    	$this->load->model('financials/budget_model');
    	$this->load->model('company_financial_model');
    	$this->load->model('payroll_model');
    	$this->load->model('ledgers_model');
    	$this->load->model('admin/dashboard_model');
	}


	public function index()
	{
		$budget_year = $this->session->userdata('budget_year');

	    if(empty($budget_year))
	    {
	    	$data['title'] = 'BUDGET FOR '.date('Y');
	    	$budget_year = date('Y');
	    }
	    else
	    {
	    	$data['title'] = 'BUDGET FOR '.$budget_year;
	    }
	    $v_data['budget_year'] = $budget_year;
	    $v_data['title'] = $data['title'];
	    $data['content'] = $this->load->view('payroll/all_payroll_accounts', $v_data, true);
	    $this->load->view('admin/templates/general_page', $data);
	}
}
?>