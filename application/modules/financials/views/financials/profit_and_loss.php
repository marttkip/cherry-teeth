<?php echo $this->load->view('search/search_profit_and_loss','', true);?>
<?php

$grand_income = 0;
$income_result = '';



$revenue_rs = $this->ledgers_model->get_all_services();
// var_dump($revenue_rs);die();

if($revenue_rs->num_rows() > 0)
{
	$balance = 0;
	$total_amount = 0;
	// var_dump($revenue_rs->result());die();
	foreach ($revenue_rs->result() as $key => $value5) {
		# code...
		// get all transactions
		$service_id = $value5->service_id;
		$department_id = $value5->department_id;
		$service_name = $value5->service_name;



		// update the service Details
		$revenue_items_rs = $this->ledgers_model->get_receivables_transactions_per_service($service_id,1);


		if($revenue_items_rs->num_rows() > 0)
		{
			$balance = 0;
			$total_amount = 0;
			// var_dump($revenue_items_rs->result());die();
			foreach ($revenue_items_rs->result() as $key => $value6) 
			{

				// $dr_amount = $value6->dr_amount;
				$dr_amount = $value6->dr_amount;
				$cr_amount = $value6->cr_amount;
				
				$total_income = $dr_amount-$cr_amount;
				$grand_income += $total_income;

				$income_result .='<tr>
									<td class="text-left">'.strtoupper($service_name).'</td>
									<td class="text-right"><a href="'.site_url().'company-financials/services-bills/'.$service_id.'" >'.number_format($total_income,2).'</a></td>
									</tr>';

			

			}
			
			
		}
	}

}
$income_result .='<tr>
						<td class="text-left"><b>INCOME</b></td>
						<td class="text-right" style="border-top:#3c8dbc solid 2px;"><b>'.number_format($grand_income,2).'</b></td>
					</tr>';
$other_income_id = $this->company_financial_model->get_parent_account_id('Other Incomes');

$other_account_rs = $this->ledgers_model->get_all_child_accounts($other_income_id);

$grand_other_income = 0;

if($other_account_rs->num_rows() > 0)
{
	$balance = 0;
	$total_amount = 0;
 	foreach ($other_account_rs->result() as $key => $value) 
 	{
 		# code...

 		$account_name = $value->account_name;
 		$account_id = $value->account_id;
 		

		$expense_of_account_rs = $this->ledgers_model->get_account_transactions($account_id);

			// var_dump($parent_account_id);die();
		if($expense_of_account_rs->num_rows() > 0)
		{
			$balance = 0;
			$total_amount = 0;
			// var_dump($expense_of_account_rs->result());die();
			foreach ($expense_of_account_rs->result() as $key => $value4) {
				# code...
				// get all transactions

				$dr_amount = $value4->dr_amount;
				$cr_amount = $value4->cr_amount;
				$accountName = $value4->accountName;

				$total_other_income = $cr_amount;
				$grand_other_income += $total_other_income;

				$income_result .='<tr>
									<td class="text-left">'.strtoupper($account_name).'</td>
									<td class="text-right"><a href="'.site_url().'accounting/expense-ledger/'.$account_id.'" >'.number_format($total_other_income,2).'</a></td>
									</tr>';
			

			}
			
		}
	}
}


$income_result .='<tr>
						<td class="text-left"><b>OTHER INCOME</b></td>
						<td class="text-right" style="border-top:#3c8dbc solid 2px;"><b>'.number_format($grand_other_income,2).'</b></td>
					</tr>';

$income_result .='<tr>
						<td class="text-left"><b>TOTAL INCOME</b></td>
						<td class="text-right" style="border-top:#3c8dbc solid 2px;"><b>'.number_format($grand_income+$grand_other_income,2).'</b></td>
					</tr>';


$salary = $this->company_financial_model->get_salary_expenses();
// $nssf = $this->company_financial_model->get_statutories(1);
// $nhif = $this->company_financial_model->get_statutories(2);
// $paye_amount = $this->company_financial_model->get_statutories(3);
$relief =0;// $this->company_financial_model->get_statutories(4);
$loans = 0;//$this->company_financial_model->get_statutories(5);

// $paye = $paye_amount - $relief;

$salary -= $relief;
$other_deductions = $salary;// - ($nssf+$nhif+$paye_amount+$relief);

// $total_operational_amount += $salary+$nssf+$nhif+$paye_amount;
$total_operational_amount += $salary;
// $operation_result .= $non_pharm;
$operation_result .='<tr>
						<td class="text-left"><b>Total Operation Cost</b></td>
						<td class="text-right" style="border-top:#3c8dbc solid 2px;"><b>'.number_format($total_operational_amount,2).'</b></td>
					</tr>';

// get cost of goods
$parent_account_id = $this->company_financial_model->get_parent_account_id('Cost of Goods');

$account_rs = $this->ledgers_model->get_all_child_accounts($parent_account_id);

// var_dump($account_rs->result());die();
$goods_result ='';
$grand_goods = 0;
if($account_rs->num_rows() > 0)
{
 	foreach ($account_rs->result() as $key => $value) 
 	{
 		# code...

 		$account_name = $value->account_name;
 		$account_id = $value->account_id;


		$expense_of_account_rs = $this->ledgers_model->get_expense_account_transactions($account_id,1);

			// var_dump($parent_account_id);die();
		if($expense_of_account_rs->num_rows() > 0)
		{
			$balance = 0;
			$total_amount = 0;
			// var_dump($expense_of_account_rs->result());die();
			foreach ($expense_of_account_rs->result() as $key => $value4) {
				# code...
				// get all transactions

				$dr_amount = $value4->dr_amount;
				$cr_amount = $value4->cr_amount;
			

				

				$total_goods = $dr_amount-$cr_amount;
				$grand_goods += $total_goods;
				// $grand_balance += $balance;
				$goods_result .='<tr>
									<td class="text-left">'.strtoupper($account_name).'</td>
									<td class="text-right"><a href="'.site_url().'accounting/expense-ledger/'.$account_id.'" >'.number_format($total_goods,2).'</a></td>
									</tr>';
			

			}
			

		}
	}
}
$goods_result .='<tr>
						<td class="text-left"><b>TOTAL GOODS SOLD</b></td>
						<td class="text-right" style="border-top:#3c8dbc solid 2px;"><b>'.number_format($grand_goods,2).'</b></td>
					</tr>';


$parent_account_id2 = $this->company_financial_model->get_parent_account_id('Expense Accounts');
$account_rs2 = $this->ledgers_model->get_all_child_accounts($parent_account_id2);

$grand_expense = 0;
$operation_result ='';
$total_operational_amount = 0;
if($account_rs2->num_rows() > 0)
{
 	foreach ($account_rs2->result() as $key => $value) 
 	{
 		# code...

 		$account_name = $value->account_name;
 		$account_id = $value->account_id;
 		 
		$expense_of_account_rs = $this->ledgers_model->get_expense_account_transactions($account_id,1);
		// var_dump($account_rs2);die();

		if($expense_of_account_rs->num_rows() > 0)
		{

			
			$balance = 0;
			$total_amount = 0;
			// var_dump($expense_of_account_rs->result());die();
			foreach ($expense_of_account_rs->result() as $key => $value4) {
				# code...
				// get all transactions

				$dr_amount = $value4->dr_amount;
				$cr_amount = $value4->cr_amount;
				$accountName = $value4->accountName;

				$total_expense = $dr_amount-$cr_amount;
				$grand_expense += $total_expense;

				
				$operation_result .='<tr>
										<td class="text-left">'.strtoupper($account_name).'</td>
										<td class="text-right"><a href="'.site_url().'accounting/expense-ledger/'.$account_id.'" >'.number_format($total_expense,2).'</a></td>
									</tr>';

			}
			

		}
	}
}


$parent_account_id2 = $this->company_financial_model->get_parent_account_id('Payroll');
$account_rs2 = $this->ledgers_model->get_all_child_accounts($parent_account_id2);

// $grand_expense = 0;
if($account_rs2->num_rows() > 0)
{
 	foreach ($account_rs2->result() as $key => $value) 
 	{
 		# code...

 		$account_name = $value->account_name;
 		$account_id = $value->account_id;
 		 
		$expense_of_account_rs = $this->ledgers_model->get_expense_account_transactions($account_id,1);
		// var_dump($account_rs2);die();

		if($expense_of_account_rs->num_rows() > 0)
		{

			
			$balance = 0;
			$total_amount = 0;
			// var_dump($expense_of_account_rs->result());die();
			foreach ($expense_of_account_rs->result() as $key => $value4) {
				# code...
				// get all transactions

				$dr_amount = $value4->dr_amount;
				$cr_amount = $value4->cr_amount;
				$accountName = $value4->accountName;

				$total_expense = $dr_amount-$cr_amount;
				$grand_expense += $total_expense;

				
				$operation_result .='<tr>
										<td class="text-left">'.strtoupper($account_name).'</td>
										<td class="text-right"><a href="'.site_url().'accounting/expense-ledger/'.$account_id.'" >'.number_format($total_expense,2).'</a></td>
									</tr>';

			}
			

		}
	}
}

$operation_result .='<tr>
						<td class="text-left"><b>TOTAL OPERATING EXPENSE</b></td>
						<td class="text-right" style="border-top:#3c8dbc solid 2px;"><b>'.number_format($grand_expense,2).'</b></td>
					</tr>';

$statement = $this->session->userdata('income_statement_title_search');

// var_dump($statement);die();

if(!empty($statement))
{
	$checked = $statement;
}
else {
	$checked = 'Reporting period: '.date('M j, Y', strtotime(date('Y-01-01'))).' to ' .date('M j, Y', strtotime(date('Y-m-d')));
}


$closing_stock =  $this->company_financial_model->get_opening_stock_value();
?>

<div class="text-center">
	<h3 class="box-title">Income Statement</h3>
	<h5 class="box-title"> <?php echo $checked?></h5>
	<h6 class="box-title">Created <?php echo date('M j, Y', strtotime(date('Y-m-d')));?></h6>
</div>

<div class="col-md-2">
	
</div>
<div class="col-md-8">

	<section class="panel">
			<?php 
			$search_status = $this->session->userdata('income_statement_search');

			if(!empty($search_status))
			{
				echo '<a href="'.site_url().'financials/company_financial/close_income_statement_search" class="btn btn-sm btn-warning">Close search</a>';
			}
			?>
				
			<!-- /.box-header -->
			<div class="panel-body">

				<h5 class="box-title" style="background-color:#3c8dbc;color:#fff;padding:5px;">INCOME</h5>
		    	<table class="table  table-striped table-condensed">
					<thead>
						<tr>
		        			<th class="text-left">Account</th>
							<th class="text-right">Balance</th>
						</tr>
					</thead>
					<tbody>
						<?php echo $income_result;?>
					</tbody>
				</table>


				<h5 class="box-title" style="background-color:#3c8dbc;color:#fff;padding:5px;">DIRECT COSTS</h5>
		    	<table class="table  table-striped table-condensed">
					<thead>
						<tr>
		        			<th class="text-left">Account</th>
							<th class="text-right">Balance</th>
						</tr>
					</thead>
					<tbody>
						

						
						<?php echo $goods_result?>
						

						
					</tbody>
				</table>

				<h5 class="box-title" style="background-color:#3c8dbc;color:#fff;padding:5px;">OPERATING EXPENSE</h5>
		    	<table class="table  table-striped table-condensed">
					<thead>
						<tr>
		        			<th class="text-left">Account</th>
							<th class="text-right">Balance</th>
						</tr>
					</thead>
					<tbody>
						
						<?php echo $operation_result;?>

					</tbody>
				</table>

				<!-- <h5 class="box-title">INTEREST (INCOME), EXPENSE & TAXES</h5> -->
		    	<table class="table  table-striped table-condensed">
					<thead>
						<tr>
		        			<th class="text-left"></th>
							<th class="text-right"></th>
						</tr>
					</thead>
					<tbody>

						<tr>
		        			<th class="text-left"><strong>NET PROFIT</strong></th>
							<th class="text-right"><?php echo number_format($grand_income - $grand_goods - $grand_expense,2)?></th>
						</tr>
					</tbody>
				</table>
	    	</div>
	</section>
</div>
<div class="col-md-2">
	
</div>
