CREATE OR REPLACE VIEW v_transactions AS
SELECT
visit_invoice.visit_invoice_id AS transaction_id,
visit.visit_id AS reference_id,
visit_invoice.visit_invoice_number AS reference_code,
'' AS transactionCode,
visit.patient_id AS patient_id,
'' AS supplier_id,
'' AS supplier_name,
'' AS parent_service,
visit_charge.service_charge_id AS child_service,
visit.personnel_id AS personnel_id,
'' AS personnel_name,
visit_invoice.bill_to AS payment_type,
visit_type.visit_type_name AS payment_type_name,
'' AS payment_method_id,
'' AS payment_method_name,
'' AS account_parent,
'0' AS account_id,
'' AS account_classification,
'' AS account_name,
'' AS transaction_name,
CONCAT( "<strong>Invoice </strong> <br> Invoice Number :  ", visit_invoice.visit_invoice_number,"<br> <strong>Bill to:</strong>  ", visit_type.visit_type_name ) AS transaction_description,
SUM(visit_charge.visit_charge_amount*visit_charge.visit_charge_units) AS dr_amount,
'0' AS cr_amount,
visit_invoice.created AS invoice_date,
visit_invoice.created AS transaction_date,
visit_charge.visit_charge_timestamp AS created_at,
visit_charge.charged AS `status`,
visit.branch_id AS branch_id,
'1' AS party,
'Revenue' AS transactionCategory,
'Invoice Patients' AS transactionClassification,
'visit_charge' AS transactionTable,
'visit' AS referenceTable
FROM
	visit_charge,visit_invoice,visit,service_charge,visit_type
	WHERE visit.visit_delete = 0 AND visit_charge.charged = 1  
	AND visit_invoice.visit_invoice_delete = 0
	AND visit_charge.visit_charge_delete = 0 
	AND visit_invoice.visit_invoice_id = visit_charge.visit_invoice_id
	AND service_charge.service_charge_id = visit_charge.service_charge_id
	AND visit_type.visit_type_id = visit_invoice.bill_to
	AND visit.visit_id = visit_charge.visit_id

GROUP BY visit_charge.visit_invoice_id

UNION ALL

-- Invocie based on patient Vist
SELECT
	payments.payment_id AS transaction_id,
	visit_invoice.visit_invoice_id AS reference_id,
	payments.confirm_number AS reference_code,
	payments.transaction_code AS transactionCode,
	payments.patient_id AS patient_id,
	'' AS supplier_id,
	'' AS supplier_name,
	'' AS parent_service,
	'' AS child_service,
	visit.personnel_id AS personnel_id,
	'' AS personnel_name,
	visit_invoice.bill_to AS payment_type,
	visit_type.visit_type_name AS payment_type_name,
	payments.payment_method_id AS payment_method_id,
	payment_method.payment_method AS payment_method_name,
	account.parent_account AS account_parent,
	account.account_id AS account_id,
	account.account_type_id AS account_classification,
	account.account_name AS account_name,
	CONCAT( "Invoice Payment", " : " ) AS transaction_name,
	CONCAT( "<strong>Payment</strong> <br> Invoice Number: ", visit_invoice.visit_invoice_number,"<br><strong>Method:</strong> ",payment_method.payment_method,"<br><strong>Receipt No:</strong> ",payments.confirm_number) AS transaction_description,
	'0' AS dr_amount,
	SUM(payment_item.payment_item_amount) AS cr_amount,
	visit_invoice.created AS invoice_date,
	payments.payment_date AS transaction_date,
	DATE(payments.time) AS created_at,
	payments.payment_status AS `status`,
	visit.branch_id AS branch_id,
	'2' AS party,
	'Revenue Payment' AS transactionCategory,
	'Invoice Patients Payment' AS transactionClassification,
	'payments' AS transactionTable,
	'visit' AS referenceTable
FROM
	payments,payment_item,visit_invoice,payment_method,visit,account,visit_type
	WHERE payments.cancel = 0 AND payments.payment_type = 1 AND payment_item.invoice_type = 1
	AND payments.payment_id = payment_item.payment_id 
	AND visit_invoice.visit_invoice_id = payment_item.visit_invoice_id 
	AND payments.payment_method_id = payment_method.payment_method_id
	AND visit.visit_id = visit_invoice.visit_id
	AND payment_method.account_id = account.account_id
	AND visit_invoice.bill_to = visit_type.visit_type_id
GROUP BY payment_item.payment_id

UNION ALL

-- Invocie based on patient Vist
SELECT
	payments.payment_id AS transaction_id,
	'' AS reference_id,
	payments.confirm_number AS reference_code,
	payments.transaction_code AS transactionCode,
	payments.patient_id AS patient_id,
	'' AS supplier_id,
	'' AS supplier_name,
	'' AS parent_service,
	'' AS child_service,
	'' AS personnel_id,
	'' AS personnel_name,
	'' AS payment_type,
	'' AS payment_type_name,
	payments.payment_method_id AS payment_method_id,
	payment_method.payment_method AS payment_method_name,
	account.parent_account AS account_parent,
	account.account_id AS account_id,
	account.account_type_id AS account_classification,
	account.account_name AS account_name,
	CONCAT( "Payment on account", " : " ) AS transaction_name,
	CONCAT( "<strong>Payment</strong> <br> On Account: <br><strong>Method:</strong> ",payment_method.payment_method,"<br><strong>Receipt No:</strong> ",payments.confirm_number) AS transaction_description,
	'0' AS dr_amount,
	SUM(payment_item.payment_item_amount) AS cr_amount,
	payments.payment_date AS invoice_date,
	payments.payment_date AS transaction_date,
	DATE(payments.time) AS created_at,
	payments.payment_status AS `status`,
	payments.branch_id AS branch_id,
	'2' AS party,
	'Revenue Payment' AS transactionCategory,
	'On account Patients Payment' AS transactionClassification,
	'payments' AS transactionTable,
	'visit' AS referenceTable
FROM
	payments,payment_item,payment_method,account
	WHERE payments.cancel = 0 AND payments.payment_type = 1 AND payment_item.invoice_type = 3
	AND payments.payment_id = payment_item.payment_id 
	AND payments.payment_method_id = payment_method.payment_method_id
	AND payment_method.account_id = account.account_id
GROUP BY payment_item.payment_id;
CREATE OR REPLACE VIEW v_transactions_by_date AS SELECT * FROM v_transactions ORDER BY transaction_date ASC;

CREATE OR REPLACE VIEW v_patient_account_balance 