CREATE OR REPLACE VIEW v_expenses_ledger AS
SELECT
	`creditor`.`creditor_id` AS `transactionId`,
	'' AS `referenceId`,
	'' AS `payingFor`,
	'' AS `referenceCode`,
	'' AS `transactionCode`,
	'' AS `patient_id`,
    `creditor`.`creditor_id` AS `recepientId`,
	'' AS `accountParentId`,
	'' AS `accountsclassfication`,
	'' AS `accountId`,
	'' AS `accountName`,
	'' AS `transactionName`,
	CONCAT('Opening Balance from',' ',`creditor`.`start_date`) AS `transactionDescription`,
  	0 AS `department_id`,
	`creditor`.`opening_balance` AS `dr_amount`,
	'0' AS `cr_amount`,
	`creditor`.`start_date` AS `transactionDate`,
	`creditor`.`start_date` AS `createdAt`,
	`creditor`.`start_date` AS `referenceDate`,
	`creditor`.`creditor_status` AS `status`,
	2 AS branch_id,
	'Expense' AS `transactionCategory`,
	'Creditor Opening Balance' AS `transactionClassification`,
	'' AS `transactionTable`,
	'creditor' AS `referenceTable`
FROM
creditor
WHERE debit_id = 2

UNION ALL

SELECT
	`creditor`.`creditor_id` AS `transactionId`,
	'' AS `referenceId`,
	'' AS `payingFor`,
	'' AS `referenceCode`,
	'' AS `transactionCode`,
	'' AS `patient_id`,
  `creditor`.`creditor_id` AS `recepientId`,
	'' AS `accountParentId`,
	'' AS `accountsclassfication`,
	'' AS `accountId`,
	'' AS `accountName`,
	'' AS `transactionName`,
	CONCAT('Opening Balance from',' ',`creditor`.`start_date`) AS `transactionDescription`,
  0 AS `department_id`,
	'0' AS `dr_amount`,
	`creditor`.`opening_balance` AS `cr_amount`,
	`creditor`.`start_date` AS `transactionDate`,
	`creditor`.`start_date` AS `createdAt`,
	`creditor`.`start_date` AS `referenceDate`,
	`creditor`.`creditor_status` AS `status`,
	2 AS branch_id,
	'Expense' AS `transactionCategory`,
	'Creditor Opening Balance' AS `transactionClassification`,
	'' AS `transactionTable`,
	'creditor' AS `referenceTable`
FROM
creditor
WHERE debit_id = 1

UNION ALL

SELECT
	`creditor_invoice_item`.`creditor_invoice_item_id` AS `transactionId`,
	`creditor_invoice`.`creditor_invoice_id` AS `referenceId`,
	'' AS `payingFor`,
	`creditor_invoice`.`invoice_number` AS `referenceCode`,
	`creditor_invoice`.`document_number` AS `transactionCode`,
	'' AS `patient_id`,
  `creditor_invoice`.`creditor_id` AS `recepientId`,
	`account`.`parent_account` AS `accountParentId`,
	`account_type`.`account_type_name` AS `accountsclassfication`,
	`creditor_invoice_item`.`account_to_id` AS `accountId`,
	`account`.`account_name` AS `accountName`,
	`creditor_invoice_item`.`item_description` AS `transactionName`,
	`creditor_invoice_item`.`item_description` AS `transactionDescription`,
	
  	'0' AS `department_id`,
	`creditor_invoice_item`.`total_amount` AS `dr_amount`,
	'0' AS `cr_amount`,
	`creditor_invoice`.`transaction_date` AS `transactionDate`,
	`creditor_invoice`.`created` AS `createdAt`,
	`creditor_invoice`.`transaction_date` AS `referenceDate`,
	`creditor_invoice_item`.`creditor_invoice_item_status` AS `status`,
	2 AS branch_id,
	'Expense' AS `transactionCategory`,
	'Creditors Invoices' AS `transactionClassification`,
	'creditor_invoice_item' AS `transactionTable`,
	'creditor_invoice' AS `referenceTable`
FROM
	(
		(
			(
				`creditor_invoice_item`,creditor_invoice,creditor
				
			)
			JOIN account ON(
				(
					account.account_id = creditor_invoice_item.account_to_id
				)
			)
		)
		JOIN `account_type` ON(
			(
				account_type.account_type_id = account.account_type_id
			)
		)
	)
WHERE 
creditor_invoice.creditor_invoice_id = creditor_invoice_item.creditor_invoice_id 
AND creditor_invoice.creditor_invoice_status = 1
AND creditor.creditor_id = creditor_invoice.creditor_id 
AND creditor_invoice.transaction_date >= creditor.start_date


UNION ALL

SELECT
	`creditor_credit_note_item`.`creditor_credit_note_item_id` AS `transactionId`,
	`creditor_credit_note`.`creditor_credit_note_id` AS `referenceId`,
	`creditor_credit_note_item`.`creditor_invoice_id` AS `payingFor`,
	`creditor_credit_note`.`invoice_number` AS `referenceCode`,
	`creditor_credit_note`.`document_number` AS `transactionCode`,
	'' AS `patient_id`,
  `creditor_credit_note`.`creditor_id` AS `recepientId`,
	`account`.`parent_account` AS `accountParentId`,
	`account_type`.`account_type_name` AS `accountsclassfication`,
	`creditor_credit_note`.`account_from_id` AS `accountId`,
	`account`.`account_name` AS `accountName`,
	`creditor_credit_note_item`.`description` AS `transactionName`,
	`creditor_credit_note_item`.`description` AS `transactionDescription`,
  0 AS `department_id`,
	0 AS `dr_amount`,
	`creditor_credit_note_item`.`credit_note_amount` AS `cr_amount`,
	`creditor_credit_note`.`transaction_date` AS `transactionDate`,
	`creditor_credit_note`.`created` AS `createdAt`,
	`creditor_invoice`.`transaction_date` AS `referenceDate`,
	`creditor_credit_note_item`.`creditor_credit_note_item_status` AS `status`,
	2 AS branch_id,
	'Expense Payment' AS `transactionCategory`,
	'Creditors Credit Notes' AS `transactionClassification`,
	'creditor_credit_note' AS `transactionTable`,
	'creditor_credit_note_item' AS `referenceTable`
FROM
	(
		(
			(
				`creditor_credit_note_item`,creditor_credit_note,creditor_invoice,creditor
				
			)
			JOIN account ON(
				(
					account.account_id = creditor_credit_note.account_from_id
				)
			)
			
		)
		JOIN `account_type` ON(
			(
				account_type.account_type_id = account.account_type_id
			)
		)
	)
WHERE 
	creditor_credit_note.creditor_credit_note_id = creditor_credit_note_item.creditor_credit_note_id 
	AND creditor_credit_note.creditor_credit_note_status = 1
	AND creditor_invoice.creditor_invoice_id = creditor_credit_note_item.creditor_invoice_id
	AND creditor_invoice.creditor_invoice_status = 1
	AND creditor.creditor_id = creditor_invoice.creditor_id 
	AND creditor_invoice.transaction_date >= creditor.start_date

	UNION ALL

	SELECT
	`order_supplier`.`order_supplier_id` AS `transactionId`,
	`orders`.`order_id` AS `referenceId`,
	'' AS `payingFor`,
	`orders`.`supplier_invoice_number` AS `referenceCode`,
	'' AS `transactionCode`,
	'' AS `patient_id`,
  `orders`.`supplier_id` AS `recepientId`,
	`account`.`parent_account` AS `accountParentId`,
	`account_type`.`account_type_name` AS `accountsclassfication`,
	`orders`.`account_id` AS `accountId`,
	`account`.`account_name` AS `accountName`,
	'Drug Purchase' AS `transactionName`,
	CONCAT('Purchase of supplies') AS `transactionDescription`,
  0 AS `department_id`,
	SUM(`order_supplier`.`less_vat`) AS `dr_amount`,
	'0' AS `cr_amount`,
	`orders`.`supplier_invoice_date` AS `transactionDate`,
	`orders`.`created` AS `createdAt`,
	`orders`.`supplier_invoice_date` AS `referenceDate`,
	`orders`.`order_approval_status` AS `status`,
	2 AS branch_id,
	'Purchases' AS `transactionCategory`,
	'Supplies Invoices' AS `transactionClassification`,
	'order_supplier' AS `transactionTable`,
	'orders' AS `referenceTable`
FROM
	(
		(
			(
				`order_supplier`
				JOIN `orders` ON(
					(
						orders.order_id = order_supplier.order_id
					)
				)
			)
			JOIN account ON(
				(
					account.account_id = orders.account_id
				)
			)


			JOIN order_item ON(
				(
					order_item.order_item_id = order_supplier.order_item_id
				)
			)
		JOIN product ON(
				(
					product.product_id = order_item.product_id
				)
			)
		)
		JOIN `account_type` ON(
			(
				account_type.account_type_id = account.account_type_id
			)
		)
	)

	WHERE orders.is_store = 0 AND orders.supplier_id > 0 and orders.order_approval_status = 7 AND order_supplier.order_item_id IN (select order_item.order_item_id FROM order_item)
	GROUP BY order_supplier.order_id

	UNION ALL

	SELECT
	`order_supplier`.`order_supplier_id` AS `transactionId`,
	`orders`.`order_id` AS `referenceId`,
	'' AS `payingFor`,
	`orders`.`supplier_invoice_number` AS `referenceCode`,
	`orders`.`reference_number`  AS `transactionCode`,
	'' AS `patient_id`,
  `orders`.`supplier_id` AS `recepientId`,
	`account`.`parent_account` AS `accountParentId`,
	`account_type`.`account_type_name` AS `accountsclassfication`,
	`orders`.`account_id` AS `accountId`,
	`account`.`account_name` AS `accountName`,
	'Credit' AS `transactionName`,
	CONCAT('Credit note of ',' ',`orders`.`reference_number`) AS `transactionDescription`,
		 0 AS `department_id`,
	'0' AS `dr_amount`,
	SUM(`order_supplier`.`less_vat`) AS `cr_amount`,
	`orders`.`supplier_invoice_date` AS `transactionDate`,
	`orders`.`created` AS `createdAt`,
	`orders`.`supplier_invoice_date` AS `referenceDate`,
	`orders`.`order_approval_status` AS `status`,
	2 AS branch_id,
	'Income' AS `transactionCategory`,
	'Supplies Credit Note' AS `transactionClassification`,
	'order_supplier' AS `transactionTable`,
	'orders' AS `referenceTable`

FROM
	(
		(
			(
				`order_supplier`
				JOIN `orders` ON(
					(
						orders.order_id = order_supplier.order_id
					)
				)
			)
			JOIN account ON(
				(
					account.account_id = orders.account_id
				)
			)


			JOIN order_item ON(
				(
					order_item.order_item_id = order_supplier.order_item_id
				)
			)
		JOIN product ON(
				(
					product.product_id = order_item.product_id
				)
			)
		)
		JOIN `account_type` ON(
			(
				account_type.account_type_id = account.account_type_id
			)
		)
	)

	WHERE orders.is_store = 3 AND orders.supplier_id > 0 and orders.order_approval_status = 7 AND order_supplier.order_item_id IN (select order_item.order_item_id FROM order_item)
	GROUP BY order_supplier.order_id

UNION ALL
-- providers
SELECT
	`provider`.`provider_id` AS `transactionId`,
	'' AS `referenceId`,
	'' AS `payingFor`,
	'' AS `referenceCode`,
	'' AS `transactionCode`,
	'' AS `patient_id`,
    `provider`.`provider_id` AS `recepientId`,
	'' AS `accountParentId`,
	'' AS `accountsclassfication`,
	'' AS `accountId`,
	'' AS `accountName`,
	'' AS `transactionName`,
	CONCAT('Opening Balance from',' ',`provider`.`start_date`) AS `transactionDescription`,
  	0 AS `department_id`,
	`provider`.`opening_balance` AS `dr_amount`,
	'0' AS `cr_amount`,
	`provider`.`start_date` AS `transactionDate`,
	`provider`.`start_date` AS `createdAt`,
	`provider`.`start_date` AS `referenceDate`,
	`provider`.`provider_status` AS `status`,
	2 AS branch_id,
	'Expense' AS `transactionCategory`,
	'provider Opening Balance' AS `transactionClassification`,
	'' AS `transactionTable`,
	'provider' AS `referenceTable`
FROM
provider
WHERE debit_id = 2

UNION ALL

SELECT
	`provider`.`provider_id` AS `transactionId`,
	'' AS `referenceId`,
	'' AS `payingFor`,
	'' AS `referenceCode`,
	'' AS `transactionCode`,
	'' AS `patient_id`,
  `provider`.`provider_id` AS `recepientId`,
	'' AS `accountParentId`,
	'' AS `accountsclassfication`,
	'' AS `accountId`,
	'' AS `accountName`,
	'' AS `transactionName`,
	CONCAT('Opening Balance from',' ',`provider`.`start_date`) AS `transactionDescription`,
  0 AS `department_id`,
	'0' AS `dr_amount`,
	`provider`.`opening_balance` AS `cr_amount`,
	`provider`.`start_date` AS `transactionDate`,
	`provider`.`start_date` AS `createdAt`,
	`provider`.`start_date` AS `referenceDate`,
	`provider`.`provider_status` AS `status`,
	2 AS branch_id,
	'Expense' AS `transactionCategory`,
	'provider Opening Balance' AS `transactionClassification`,
	'' AS `transactionTable`,
	'provider' AS `referenceTable`
FROM
provider
WHERE debit_id = 1

UNION ALL

SELECT
	`provider_invoice_item`.`provider_invoice_item_id` AS `transactionId`,
	`provider_invoice`.`provider_invoice_id` AS `referenceId`,
	'' AS `payingFor`,
	`provider_invoice`.`invoice_number` AS `referenceCode`,
	`provider_invoice`.`document_number` AS `transactionCode`,
	'' AS `patient_id`,
  `provider_invoice`.`provider_id` AS `recepientId`,
	`account`.`parent_account` AS `accountParentId`,
	`account_type`.`account_type_name` AS `accountsclassfication`,
	`provider_invoice_item`.`account_to_id` AS `accountId`,
	`account`.`account_name` AS `accountName`,
	`provider_invoice_item`.`item_description` AS `transactionName`,
	`provider_invoice_item`.`item_description` AS `transactionDescription`,
	
  	'0' AS `department_id`,
	`provider_invoice_item`.`total_amount` AS `dr_amount`,
	'0' AS `cr_amount`,
	`provider_invoice`.`transaction_date` AS `transactionDate`,
	`provider_invoice`.`created` AS `createdAt`,
	`provider_invoice`.`transaction_date` AS `referenceDate`,
	`provider_invoice_item`.`provider_invoice_item_status` AS `status`,
	2 AS branch_id,
	'Expense' AS `transactionCategory`,
	'providers Invoices' AS `transactionClassification`,
	'provider_invoice_item' AS `transactionTable`,
	'provider_invoice' AS `referenceTable`
FROM
	(
		(
			(
				`provider_invoice_item`,provider_invoice,provider
				
			)
			JOIN account ON(
				(
					account.account_id = provider_invoice_item.account_to_id
				)
			)
		)
		JOIN `account_type` ON(
			(
				account_type.account_type_id = account.account_type_id
			)
		)
	)
WHERE 
provider_invoice.provider_invoice_id = provider_invoice_item.provider_invoice_id 
AND provider_invoice.provider_invoice_status = 1
AND provider.provider_id = provider_invoice.provider_id 
AND provider_invoice.transaction_date >= provider.start_date


UNION ALL

SELECT
	`provider_credit_note_item`.`provider_credit_note_item_id` AS `transactionId`,
	`provider_credit_note`.`provider_credit_note_id` AS `referenceId`,
	`provider_credit_note_item`.`provider_invoice_id` AS `payingFor`,
	`provider_credit_note`.`invoice_number` AS `referenceCode`,
	`provider_credit_note`.`document_number` AS `transactionCode`,
	'' AS `patient_id`,
  `provider_credit_note`.`provider_id` AS `recepientId`,
	`account`.`parent_account` AS `accountParentId`,
	`account_type`.`account_type_name` AS `accountsclassfication`,
	`provider_credit_note`.`account_from_id` AS `accountId`,
	`account`.`account_name` AS `accountName`,
	`provider_credit_note_item`.`description` AS `transactionName`,
	`provider_credit_note_item`.`description` AS `transactionDescription`,
  0 AS `department_id`,
	0 AS `dr_amount`,
	`provider_credit_note_item`.`credit_note_amount` AS `cr_amount`,
	`provider_credit_note`.`transaction_date` AS `transactionDate`,
	`provider_credit_note`.`created` AS `createdAt`,
	`provider_invoice`.`transaction_date` AS `referenceDate`,
	`provider_credit_note_item`.`provider_credit_note_item_status` AS `status`,
	2 AS branch_id,
	'Expense Payment' AS `transactionCategory`,
	'providers Credit Notes' AS `transactionClassification`,
	'provider_credit_note' AS `transactionTable`,
	'provider_credit_note_item' AS `referenceTable`
FROM
	(
		(
			(
				`provider_credit_note_item`,provider_credit_note,provider_invoice,provider
				
			)
			JOIN account ON(
				(
					account.account_id = provider_credit_note.account_from_id
				)
			)
			
		)
		JOIN `account_type` ON(
			(
				account_type.account_type_id = account.account_type_id
			)
		)
	)
WHERE 
	provider_credit_note.provider_credit_note_id = provider_credit_note_item.provider_credit_note_id 
	AND provider_credit_note.provider_credit_note_status = 1
	AND provider_invoice.provider_invoice_id = provider_credit_note_item.provider_invoice_id
	AND provider_invoice.provider_invoice_status = 1
	AND provider.provider_id = provider_invoice.provider_id 
	AND provider_invoice.transaction_date >= provider.start_date

UNION ALL


SELECT
	`finance_purchase`.`finance_purchase_id` AS `transactionId`,
	'' AS `referenceId`,
	`finance_purchase`.`finance_purchase_id` AS `payingFor`,
	`finance_purchase`.`transaction_number` AS `referenceCode`,

	`finance_purchase`.`document_number` AS `transactionCode`,
	'' AS `patient_id`,
	`finance_purchase`.`creditor_id` AS `recepientId`,
	`account`.`parent_account` AS `accountParentId`,
	`account_type`.`account_type_name` AS `accountsclassfication`,
	`finance_purchase`.`account_to_id` AS `accountId`,
	`account`.`account_name` AS `accountName`,
	`finance_purchase`.`finance_purchase_description` AS `transactionName`,
	CONCAT(`account`.`account_name`, ' paying for invoice ',`finance_purchase`.`transaction_number`,' Ref. ', `finance_purchase`.`transaction_number`) AS `transactionDescription`,
	  0 AS `department_id`,
	`finance_purchase`.`finance_purchase_amount` AS `dr_amount`,
	0 AS `cr_amount`,
	`finance_purchase`.`transaction_date` AS `transactionDate`,
	`finance_purchase`.`created` AS `createdAt`,
	`finance_purchase`.`transaction_date` AS `referenceDate`,
	`finance_purchase`.`finance_purchase_status` AS `status`,
	2 AS branch_id,
	'Expense' AS `transactionCategory`,
	'Purchases' AS `transactionClassification`,
	'finance_purchase' AS `transactionTable`,
	'' AS `referenceTable`
FROM
	(
		(
			(
				`finance_purchase`,account
				
			)

		)
		JOIN `account_type` ON(
			(
				account_type.account_type_id = account.account_type_id
			)
		)
	)
WHERE finance_purchase.finance_purchase_deleted = 0 AND account.account_id = finance_purchase.account_to_id


UNION ALL


SELECT
	payroll_summary.payroll_summary_id AS `transactionId`,
	payroll_summary.payroll_id AS `referenceId`,
	'' AS `payingFor`,
	'' AS `referenceCode`,
	'' AS `transactionCode`,
	'' AS `patient_id`,
	'' AS `recepientId`,
	`account`.`parent_account` AS `accountParentId`,
	`account_type`.`account_type_name` AS `accountsclassfication`,
	`account`.`account_id` AS `accountId`,
	`account`.`account_name` AS `accountName`,
	CONCAT('Payroll') AS `transactionName`,
	CONCAT('Payroll') AS `transactionDescription`,
	  0 AS `department_id`,
	payroll_summary.total_payroll AS `dr_amount`,
	0 AS `cr_amount`,
	payroll_summary.payroll_created_for AS `transactionDate`,
	payroll_summary.payroll_created_for  AS `createdAt`,
	payroll_summary.payroll_created_for  AS `referenceDate`,
	`payroll`.`payroll_status` AS `status`,
	2 AS branch_id,
	'Payroll' AS `transactionCategory`,
	'Purchases' AS `transactionClassification`,
	'finance_purchase' AS `transactionTable`,
	'' AS `referenceTable`
FROM
payroll_summary,payroll,statutory_accounts,account,account_type
WHERE payroll.payroll_id = payroll_summary.payroll_id
AND statutory_accounts.statutory_account_id = 1
AND account.account_id = statutory_accounts.account_id
AND account_type.account_type_id = account.account_type_id


UNION ALL 


SELECT
	payroll_summary.payroll_summary_id AS `transactionId`,
	payroll_summary.payroll_id AS `referenceId`,
	'' AS `payingFor`,
	'' AS `referenceCode`,
	'' AS `transactionCode`,
	'' AS `patient_id`,
	'' AS `recepientId`,
	`account`.`parent_account` AS `accountParentId`,
	`account_type`.`account_type_name` AS `accountsclassfication`,
	`account`.`account_id` AS `accountId`,
	`account`.`account_name` AS `accountName`,
	CONCAT('PAYE') AS `transactionName`,
	CONCAT('PAYE') AS `transactionDescription`,
	  0 AS `department_id`,
	payroll_summary.paye AS `dr_amount`,
	0 AS `cr_amount`,
	payroll_summary.payroll_created_for AS `transactionDate`,
	payroll_summary.payroll_created_for  AS `createdAt`,
	payroll_summary.payroll_created_for  AS `referenceDate`,
	`payroll`.`payroll_status` AS `status`,
	2 AS branch_id,
	'PAYE' AS `transactionCategory`,
	'Purchases' AS `transactionClassification`,
	'finance_purchase' AS `transactionTable`,
	'' AS `referenceTable`
FROM
payroll_summary,payroll,statutory_accounts,account,account_type
WHERE payroll.payroll_id = payroll_summary.payroll_id
AND statutory_accounts.statutory_account_id = 2
AND account.account_id = statutory_accounts.account_id
AND account_type.account_type_id = account.account_type_id




UNION ALL 


SELECT
	payroll_summary.payroll_summary_id AS `transactionId`,
	payroll_summary.payroll_id AS `referenceId`,
	'' AS `payingFor`,
	'' AS `referenceCode`,
	'' AS `transactionCode`,
	'' AS `patient_id`,
	'' AS `recepientId`,
	`account`.`parent_account` AS `accountParentId`,
	`account_type`.`account_type_name` AS `accountsclassfication`,
	`account`.`account_id` AS `accountId`,
	`account`.`account_name` AS `accountName`,
	CONCAT('NSSF') AS `transactionName`,
	CONCAT('NSSF') AS `transactionDescription`,
	  0 AS `department_id`,
	payroll_summary.nssf AS `dr_amount`,
	0 AS `cr_amount`,
	payroll_summary.payroll_created_for AS `transactionDate`,
	payroll_summary.payroll_created_for  AS `createdAt`,
	payroll_summary.payroll_created_for  AS `referenceDate`,
	`payroll`.`payroll_status` AS `status`,
	2 AS branch_id,
	'PAYE' AS `transactionCategory`,
	'Purchases' AS `transactionClassification`,
	'finance_purchase' AS `transactionTable`,
	'' AS `referenceTable`
FROM
payroll_summary,payroll,statutory_accounts,account,account_type
WHERE payroll.payroll_id = payroll_summary.payroll_id
AND statutory_accounts.statutory_account_id = 3
AND account.account_id = statutory_accounts.account_id
AND account_type.account_type_id = account.account_type_id


UNION ALL




SELECT
	payroll_summary.payroll_summary_id AS `transactionId`,
	payroll_summary.payroll_id AS `referenceId`,
	'' AS `payingFor`,
	'' AS `referenceCode`,
	'' AS `transactionCode`,
	'' AS `patient_id`,
	'' AS `recepientId`,
	`account`.`parent_account` AS `accountParentId`,
	`account_type`.`account_type_name` AS `accountsclassfication`,
	`account`.`account_id` AS `accountId`,
	`account`.`account_name` AS `accountName`,
	CONCAT('NHIF') AS `transactionName`,
	CONCAT('NHIF') AS `transactionDescription`,
	  0 AS `department_id`,
	payroll_summary.nhif AS `dr_amount`,
	0 AS `cr_amount`,
	payroll_summary.payroll_created_for AS `transactionDate`,
	payroll_summary.payroll_created_for  AS `createdAt`,
	payroll_summary.payroll_created_for  AS `referenceDate`,
	`payroll`.`payroll_status` AS `status`,
	2 AS branch_id,
	'NHIF' AS `transactionCategory`,
	'Purchases' AS `transactionClassification`,
	'finance_purchase' AS `transactionTable`,
	'' AS `referenceTable`
FROM
payroll_summary,payroll,statutory_accounts,account,account_type
WHERE payroll.payroll_id = payroll_summary.payroll_id
AND statutory_accounts.statutory_account_id = 4
AND account.account_id = statutory_accounts.account_id
AND account_type.account_type_id = account.account_type_id;

CREATE OR REPLACE VIEW v_expenses_ledger_by_date AS SELECT * FROM v_expenses_ledger ORDER BY transactionDate;



