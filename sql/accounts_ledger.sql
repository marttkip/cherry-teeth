CREATE OR REPLACE VIEW v_account_ledger AS

SELECT
	`account`.`account_id` AS `transactionId`,
	'' AS `referenceId`,
	'' AS `payingFor`,
	'' AS `referenceCode`,
	'' AS `transactionCode`,
	'' AS `patient_id`,
    '' AS `recepientId`,
	account.parent_account AS `accountParentId`,
	`account_type`.`account_type_name` AS `accountsclassfication`,
	account.account_id AS `accountId`,
	account.account_name AS `accountName`,
	CONCAT('Opening Balance as from',' ',`account`.`start_date`) AS `transactionName`,
	CONCAT('Opening Balance as from',' ',' ',`account`.`start_date`) AS `transactionDescription`,
	`account`.`account_opening_balance` AS `dr_amount`,
	'0' AS `cr_amount`,
	`account`.`start_date` AS `transactionDate`,
	`account`.`start_date` AS `createdAt`,
	`account`.`account_status` AS `status`,
	branch_id AS `branch_id`,
	'Income' AS `transactionCategory`,
	'Account Opening Balance' AS `transactionClassification`,
	'' AS `transactionTable`,
	'account' AS `referenceTable`
FROM
account,account_type

WHERE 

account_type.account_type_id = account.account_type_id AND (account_type.account_type_name = 'Bank' OR account_type.account_type_name ='Capital')


UNION ALL

SELECT
  	`finance_transfered`.`finance_transfered_id` AS `transactionId`,
  	`finance_transfer`.`finance_transfer_id` AS `referenceId`,
  	'' AS `payingFor`,
  	`finance_transfer`.`reference_number` AS `referenceCode`,
  	`finance_transfer`.`document_number` AS `transactionCode`,
  	'' AS `patient_id`,
    '' AS `recepientId`,
  	`account`.`parent_account` AS `accountParentId`,
  	`account_type`.`account_type_name` AS `accountsclassfication`,
  	`finance_transfered`.`account_to_id` AS `accountId`,
  	`account`.`account_name` AS `accountName`,
  	`finance_transfered`.`remarks` AS `transactionName`,
  	 CONCAT('Amount Received from ',(SELECT account_name FROM account WHERE account_id = finance_transfer.account_from_id ),' Ref. ', `finance_transfer`.`reference_number`) AS `transactionDescription`,
  	`finance_transfered`.`finance_transfered_amount` AS `dr_amount`,
     0 AS `cr_amount`,
  	`finance_transfer`.`transaction_date` AS `transactionDate`,
  	`finance_transfer`.`created` AS `createdAt`,
  	`finance_transfer`.`finance_transfer_status` AS `status`,
  	`finance_transfer`.`branch_id` AS `branch_id`,
  	'Transfer' AS `transactionCategory`,
  	'Transfer' AS `transactionClassification`,
  	'finance_transfer' AS `transactionTable`,
  	'finance_transfered' AS `referenceTable`
  FROM
  `finance_transfer`,finance_transfered,account,account_type
   WHERE finance_transfer.finance_transfer_id = finance_transfered.finance_transfer_id 
  	AND account.account_id = finance_transfered.account_to_id
  	AND account_type.account_type_id = account.account_type_id
  	AND finance_transfer.finance_transfer_deleted = 0

UNION ALL


SELECT
	`finance_purchase_payment`.`finance_purchase_payment_id` AS `transactionId`,
	'' AS `referenceId`,
	`finance_purchase`.`finance_purchase_id` AS `payingFor`,
	`finance_purchase`.`transaction_number` AS `referenceCode`,
	`finance_purchase`.`document_number` AS `transactionCode`,
	'' AS `patient_id`,
  	finance_purchase.creditor_id AS `recepientId`,
	`account`.`parent_account` AS `accountParentId`,
	`account_type`.`account_type_name` AS `accountsclassfication`,
	`finance_purchase_payment`.`account_from_id` AS `accountId`,
	`account`.`account_name` AS `accountName`,
	`finance_purchase`.`finance_purchase_description` AS `transactionName`,
	CONCAT(`account`.`account_name`, ' paying for invoice ',`finance_purchase`.`transaction_number`,' Ref. ', `finance_purchase`.`transaction_number`) AS `transactionDescription`,
	0 AS `dr_amount`,
	`finance_purchase_payment`.`amount_paid` AS `cr_amount`,
	`finance_purchase`.`transaction_date` AS `transactionDate`,
	`finance_purchase`.`created` AS `createdAt`,
	`finance_purchase_payment`.`finance_purchase_payment_status` AS `status`,
	`finance_purchase`.`branch_id` AS `branch_id`,
	'Expense Payment' AS `transactionCategory`,
	'Purchase Payment' AS `transactionClassification`,
	'finance_purchase' AS `transactionTable`,
	'finance_purchase_payment' AS `referenceTable`
FROM
	(
		(
			(
				`finance_purchase_payment`,finance_purchase,account
				
			)
			
		)
		JOIN `account_type` ON(
			(
				account_type.account_type_id = account.account_type_id
			)
		)
	)
WHERE finance_purchase.finance_purchase_id = finance_purchase_payment.finance_purchase_id AND finance_purchase.finance_purchase_deleted = 0 AND account.account_id = finance_purchase_payment.account_from_id
UNION ALL


SELECT
	`finance_purchase`.`finance_purchase_id` AS `transactionId`,
	'' AS `referenceId`,
	`finance_purchase`.`finance_purchase_id` AS `payingFor`,
	`finance_purchase`.`transaction_number` AS `referenceCode`,

	`finance_purchase`.`document_number` AS `transactionCode`,
	'' AS `patient_id`,
	`finance_purchase`.`creditor_id` AS `recepientId`,
	`account`.`parent_account` AS `accountParentId`,
	`account_type`.`account_type_name` AS `accountsclassfication`,
	`finance_purchase`.`account_to_id` AS `accountId`,
	`account`.`account_name` AS `accountName`,
	`finance_purchase`.`finance_purchase_description` AS `transactionName`,
	CONCAT(`account`.`account_name`, ' paying for invoice ',`finance_purchase`.`transaction_number`,' Ref. ', `finance_purchase`.`transaction_number`) AS `transactionDescription`,
	`finance_purchase`.`finance_purchase_amount` AS `dr_amount`,
	0 AS `cr_amount`,
	`finance_purchase`.`transaction_date` AS `transactionDate`,
	`finance_purchase`.`created` AS `createdAt`,
	`finance_purchase`.`finance_purchase_status` AS `status`,
	`finance_purchase`.`branch_id` AS `branch_id`,
	'Expense' AS `transactionCategory`,
	'Purchases' AS `transactionClassification`,
	'finance_purchase' AS `transactionTable`,
	'' AS `referenceTable`
FROM
	(
		(
			(
				`finance_purchase`,account
				
			)

		)
		JOIN `account_type` ON(
			(
				account_type.account_type_id = account.account_type_id
			)
		)
	)
WHERE finance_purchase.finance_purchase_deleted = 0 AND account.account_id = finance_purchase.account_to_id



UNION ALL



  SELECT
  	`finance_transfer`.`finance_transfer_id` AS `transactionId`,
  	`finance_transfered`.`finance_transfered_id` AS `referenceId`,
  	'' AS `payingFor`,
  	`finance_transfer`.`reference_number` AS `referenceCode`,
  	`finance_transfer`.`document_number` AS `transactionCode`,
  	'' AS `patient_id`,
    '' AS `recepientId`,
  	`account`.`parent_account` AS `accountParentId`,
  	`account_type`.`account_type_name` AS `accountsclassfication`,
  	`finance_transfer`.`account_from_id` AS `accountId`,
  	`account`.`account_name` AS `accountName`,
  	`finance_transfer`.`remarks` AS `transactionName`,
  	CONCAT(' Amount Transfered to ',(SELECT account_name FROM account WHERE account_id = finance_transfered.account_to_id )) AS `transactionDescription`,
  	0 AS `dr_amount`,
  	`finance_transfer`.`finance_transfer_amount` AS `cr_amount`,
  	`finance_transfered`.`transaction_date` AS `transactionDate`,
  	`finance_transfered`.`created` AS `createdAt`,

  	`finance_transfer`.`finance_transfer_status` AS `status`,
  	`finance_transfer`.`branch_id` AS `branch_id`,
  	'Transfer' AS `transactionCategory`,
  	'Transfer' AS `transactionClassification`,
  	'finance_transfered' AS `transactionTable`,
  	'finance_transfer' AS `referenceTable`
  FROM
	`finance_transfer`,finance_transfered,account,account_type
  				
  WHERE finance_transfer.finance_transfer_id = finance_transfered.finance_transfer_id 
  	AND account.account_id = finance_transfer.account_from_id
  	AND account_type.account_type_id = account.account_type_id
  	AND finance_transfer.finance_transfer_deleted = 0

  UNION ALL


 SELECT
		`creditor_invoice_item`.`creditor_invoice_item_id` AS `transactionId`,
		`creditor_invoice`.`creditor_invoice_id` AS `referenceId`,
		'' AS `payingFor`,
		`creditor_invoice`.`invoice_number` AS `referenceCode`,
		`creditor_invoice`.`document_number` AS `transactionCode`,
		'' AS `patient_id`,
	  `creditor_invoice`.`creditor_id` AS `recepientId`,
		`account`.`parent_account` AS `accountParentId`,
		`account_type`.`account_type_name` AS `accountsclassfication`,
		`creditor_invoice_item`.`account_to_id` AS `accountId`,
		`account`.`account_name` AS `accountName`,
		`creditor_invoice_item`.`item_description` AS `transactionName`,
		'' AS `transactionDescription`,
		`creditor_invoice_item`.`total_amount` AS `dr_amount`,
		'0' AS `cr_amount`,
		`creditor_invoice`.`transaction_date` AS `transactionDate`,
		`creditor_invoice`.`created` AS `createdAt`,
		`creditor_invoice_item`.`creditor_invoice_item_status` AS `status`,
		'creditor.branch_id' AS `branch_id`,
		'Cost of Goods' AS `transactionCategory`,
		'Creditors Invoices' AS `transactionClassification`,
		'creditor_invoice_item' AS `transactionTable`,
		'creditor_invoice' AS `referenceTable`
	FROM
		(
			(
				(
					`creditor_invoice_item`,creditor_invoice,creditor
					
				)
				JOIN account ON(
					(
						account.account_id = creditor_invoice_item.account_to_id
					)
				)
			)
			JOIN `account_type` ON(
				(
					account_type.account_type_id = account.account_type_id
				)
			)
		)
	WHERE 

	creditor_invoice.creditor_invoice_id = creditor_invoice_item.creditor_invoice_id 
	AND creditor_invoice.creditor_invoice_status = 1
	AND creditor.creditor_id = creditor_invoice.creditor_id 
	AND creditor_invoice.transaction_date >= creditor.start_date




  UNION ALL
 

  SELECT
	`creditor_payment_item`.`creditor_payment_item_id` AS `transactionId`,
	`creditor_payment`.`creditor_payment_id` AS `referenceId`,
	`creditor_payment_item`.`creditor_invoice_id` AS `payingFor`,
	`creditor_payment`.`reference_number` AS `referenceCode`,
	`creditor_payment`.`document_number` AS `transactionCode`,
	'' AS `patient_id`,
  	`creditor_payment`.`creditor_id` AS `recepientId`,
	`account`.`parent_account` AS `accountParentId`,
	`account_type`.`account_type_name` AS `accountsclassfication`,
	`creditor_payment`.`account_from_id` AS `accountId`,
	`account`.`account_name` AS `accountName`,
	`creditor_payment_item`.`description` AS `transactionName`,
	CONCAT('Payment for invoice of ',' ',`creditor_invoice`.`invoice_number`,' Ref. ', `creditor_payment`.`reference_number`)  AS `transactionDescription`,
	0 AS `dr_amount`,
	`creditor_payment_item`.`amount_paid` AS `cr_amount`,
	`creditor_payment`.`transaction_date` AS `transactionDate`,
	`creditor_payment`.`created` AS `createdAt`,
	`creditor_payment_item`.`creditor_payment_item_status` AS `status`,
	'creditor_payment.branch_id' AS `branch_id`,
	'Creditor Payment' AS `transactionCategory`,
	'Creditors Invoices Payments' AS `transactionClassification`,
	'creditor_payment' AS `transactionTable`,
	'creditor_payment_item' AS `referenceTable`
FROM
	(
		(
			(
				`creditor_payment_item`,creditor_payment,creditor_invoice
				
			)
			JOIN account ON(
				(
					account.account_id = creditor_payment.account_from_id
				)
			)
		)
		JOIN `account_type` ON(
			(
				account_type.account_type_id = account.account_type_id
			)
		)
	
	)
	WHERE creditor_payment_item.invoice_type = 0 
	AND creditor_payment.creditor_payment_id = creditor_payment_item.creditor_payment_id 
	AND creditor_invoice.creditor_invoice_id = creditor_payment_item.creditor_invoice_id
	AND creditor_payment.creditor_payment_status = 1

UNION ALL

 SELECT
`creditor_payment_item`.`creditor_payment_item_id` AS `transactionId`,
`creditor_payment`.`creditor_payment_id` AS `referenceId`,
`creditor_payment_item`.`creditor_invoice_id` AS `payingFor`,
`creditor_payment`.`reference_number` AS `referenceCode`,
`creditor_payment`.`document_number` AS `transactionCode`,
'' AS `patient_id`,
`creditor_payment`.`creditor_id` AS `recepientId`,
`account`.`parent_account` AS `accountParentId`,
`account_type`.`account_type_name` AS `accountsclassfication`,
`creditor_payment`.`account_from_id` AS `accountId`,
`account`.`account_name` AS `accountName`,
`creditor_payment_item`.`description` AS `transactionName`,
CONCAT('Payment for invoice of ',' ',`orders`.`supplier_invoice_number`,' Ref. ', `creditor_payment`.`reference_number`)  AS `transactionDescription`,
0 AS `dr_amount`,
`creditor_payment_item`.`amount_paid` AS `cr_amount`,
`creditor_payment`.`transaction_date` AS `transactionDate`,
`creditor_payment`.`created` AS `createdAt`,
`creditor_payment_item`.`creditor_payment_item_status` AS `status`,
'creditor_payment.branch_id' AS `branch_id`,
'Creditor Payment' AS `transactionCategory`,
'Creditors Invoices Payments' AS `transactionClassification`,
'creditor_payment' AS `transactionTable`,
'creditor_payment_item' AS `referenceTable`
FROM
(
	(
		(
			`creditor_payment_item`,creditor_payment,orders
			
		)
		JOIN account ON(
			(
				account.account_id = creditor_payment.account_from_id
			)
		)
	)
	JOIN `account_type` ON(
		(
			account_type.account_type_id = account.account_type_id
		)
	)
	
)
WHERE creditor_payment_item.invoice_type = 1 
AND creditor_payment.creditor_payment_id = creditor_payment_item.creditor_payment_id  
AND creditor_payment.creditor_payment_status = 1 AND orders.order_id = creditor_payment_item.creditor_invoice_id

UNION ALL

SELECT
`creditor_payment_item`.`creditor_payment_item_id` AS `transactionId`,
`creditor_payment`.`creditor_payment_id` AS `referenceId`,
`creditor_payment_item`.`creditor_invoice_id` AS `payingFor`,
`creditor_payment`.`reference_number` AS `referenceCode`,
`creditor_payment`.`document_number` AS `transactionCode`,
'' AS `patient_id`,
`creditor_payment`.`creditor_id` AS `recepientId`,
`account`.`parent_account` AS `accountParentId`,
`account_type`.`account_type_name` AS `accountsclassfication`,
`creditor_payment`.`account_from_id` AS `accountId`,
`account`.`account_name` AS `accountName`,
`creditor_payment_item`.`description` AS `transactionName`,
CONCAT('Payment for creditor invoice',' Ref. ', `creditor_payment`.`reference_number`)  AS `transactionDescription`,
0 AS `dr_amount`,
`creditor_payment_item`.`amount_paid` AS `cr_amount`,
`creditor_payment`.`transaction_date` AS `transactionDate`,
`creditor_payment`.`created` AS `createdAt`,
`creditor_payment_item`.`creditor_payment_item_status` AS `status`,
'creditor_payment.branch_id' AS `branch_id`,
'Creditor Payment' AS `transactionCategory`,
'Creditors Invoices Payments' AS `transactionClassification`,
'creditor_payment' AS `transactionTable`,
'creditor_payment_item' AS `referenceTable`
FROM
(
	(
		(
			`creditor_payment_item`,creditor_payment,creditor
			
		)
		JOIN account ON(
			(
				account.account_id = creditor_payment.account_from_id
			)
		)
	)
	JOIN `account_type` ON(
		(
			account_type.account_type_id = account.account_type_id
		)
	)

)
WHERE creditor_payment_item.invoice_type = 2 
AND 
creditor_payment.creditor_payment_id = creditor_payment_item.creditor_payment_id AND creditor_payment.creditor_payment_status = 1
AND creditor.creditor_id = creditor_payment_item.creditor_id



UNION ALL

SELECT
`creditor_payment_item`.`creditor_payment_item_id` AS `transactionId`,
`creditor_payment`.`creditor_payment_id` AS `referenceId`,
`creditor_payment_item`.`creditor_invoice_id` AS `payingFor`,
`creditor_payment`.`reference_number` AS `referenceCode`,
`creditor_payment`.`document_number` AS `transactionCode`,
'' AS `patient_id`,
`creditor_payment`.`creditor_id` AS `recepientId`,
`account`.`parent_account` AS `accountParentId`,
`account_type`.`account_type_name` AS `accountsclassfication`,
`creditor_payment`.`account_from_id` AS `accountId`,
`account`.`account_name` AS `accountName`,
`creditor_payment_item`.`description` AS `transactionName`,
CONCAT('Payment on account',' Ref. ', `creditor_payment`.`reference_number`)  AS `transactionDescription`,
0 AS `dr_amount`,
`creditor_payment_item`.`amount_paid` AS `cr_amount`,
`creditor_payment`.`transaction_date` AS `transactionDate`,
`creditor_payment`.`created` AS `createdAt`,
`creditor_payment_item`.`creditor_payment_item_status` AS `status`,
'creditor_payment.branch_id' AS `branch_id`,
'Creditor Payment' AS `transactionCategory`,
'Creditors Invoices Payments' AS `transactionClassification`,
'creditor_payment' AS `transactionTable`,
'creditor_payment_item' AS `referenceTable`
FROM
(
	(
		(
			`creditor_payment_item`,creditor_payment,creditor
			
		)
		JOIN account ON(
			(
				account.account_id = creditor_payment.account_from_id
			)
		)
	)
	JOIN `account_type` ON(
		(
			account_type.account_type_id = account.account_type_id
		)
	)
	
)
WHERE creditor_payment_item.invoice_type = 3
AND creditor_payment.creditor_payment_id = creditor_payment_item.creditor_payment_id AND creditor_payment.creditor_payment_status = 1
AND creditor.creditor_id = creditor_payment_item.creditor_id

UNION ALL 


SELECT


	`journal_entry`.`journal_entry_id` AS `transactionId`,
	'' AS `referenceId`,
	'' AS `payingFor`,
	'' AS `referenceCode`,
	journal_entry.document_number AS `transactionCode`,
	'' AS `patient_id`,
	'' AS `recepientId`,
	account.parent_account AS `accountParentId`,
	`account_type`.`account_type_name` AS `accountsclassfication`,
	journal_entry.account_from_id AS `accountId`,
	account.account_name AS `accountName`,
	'Journal Entry ' AS `transactionName`,
	CONCAT(journal_entry.journal_entry_description) AS `transactionDescription`,
	0 AS `dr_amount`,
	`journal_entry`.`amount_paid` AS `cr_amount`,
	`journal_entry`.`payment_date` AS `transactionDate`,
	`journal_entry`.`payment_date` AS `createdAt`,
	`journal_entry`.`journal_entry_status` AS `status`,
	2 AS `branch_id`,
	'Journal Credit' AS `transactionCategory`,
	'Journal' AS `transactionClassification`,
	'journal_entry' AS `transactionTable`,
	'account' AS `referenceTable`
FROM
journal_entry,account
LEFT JOIN `account_type` ON(
			(
				account_type.account_type_id = account.account_type_id
			)
		)
WHERE journal_entry.account_from_id  = account.account_id AND journal_entry.journal_entry_deleted = 0 



UNION ALL 


SELECT
	`journal_entry`.`journal_entry_id` AS `transactionId`,
	'' AS `referenceId`,
	'' AS `payingFor`,
	'' AS `referenceCode`,
	journal_entry.document_number AS `transactionCode`,
	'' AS `patient_id`,
	'' AS `recepientId`,
	account.parent_account AS `accountParentId`,
	`account_type`.`account_type_name` AS `accountsclassfication`,
	journal_entry.account_to_id AS `accountId`,
	account.account_name AS `accountName`,
	'Journal Entry ' AS `transactionName`,
	CONCAT(journal_entry.journal_entry_description) AS `transactionDescription`,
	`journal_entry`.`amount_paid` AS `dr_amount`,
	0 AS `cr_amount`,
	`journal_entry`.`payment_date` AS `transactionDate`,
	`journal_entry`.`payment_date` AS `createdAt`,
	`journal_entry`.`journal_entry_status` AS `status`,
	2 AS `branch_id`,
	'Journal Debit' AS `transactionCategory`,
	'Journal' AS `transactionClassification`,
	'journal_entry' AS `transactionTable`,
	'account' AS `referenceTable`
FROM
journal_entry,account
LEFT JOIN `account_type` ON(
			(
				account_type.account_type_id = account.account_type_id
			)
		)
WHERE journal_entry.account_to_id  = account.account_id AND journal_entry.journal_entry_deleted = 0 

UNION ALL

SELECT
	`account_payments`.`account_payment_id` AS `transactionId`,
	'' AS `referenceId`,
	'' AS `payingFor`,
	'' AS `referenceCode`,
	'' AS `transactionCode`,
	'' AS `patient_id`,
  	'' AS `recepientId`,
	account_type.account_type_id AS `accountParentId`,
	account_type.account_type_name AS `accountsclassfication`,
	`account`.`account_id` AS `accountId`,
	`account`.`account_name` AS `accountName`,
	'' AS `transactionName`,
	account_payments.account_payment_description AS `transactionDescription`,
	`account_payments`.`amount_paid` AS `dr_amount`,
	'0' AS `cr_amount`,
	`account_payments`.`payment_date` AS `transactionDate`,
	`account_payments`.`payment_date` AS `createdAt`,
	`account_payments`.`account_payment_deleted` AS `status`,
	2 AS `branch_id`,
	'Expense' AS `transactionCategory`,
	'Purchase Payment' AS `transactionClassification`,
	'account_payments' AS `transactionTable`,
	'' AS `referenceTable`
FROM
account_payments
LEFT JOIN `account` ON 	account_payments.account_to_id = account.account_id
LEFT JOIN `account_type` ON	account_type.account_type_id = account.account_type_id		
WHERE account_payments.account_to_type = 4 AND account_payments.account_payment_deleted = 0

UNION ALL 

SELECT
	`account_payments`.`account_payment_id` AS `transactionId`,
	'' AS `referenceId`,
	'' AS `payingFor`,
	'' AS `referenceCode`,
	'' AS `transactionCode`,
	'' AS `patient_id`,
  	'' AS `recepientId`,
	account_type.account_type_id AS `accountParentId`,
	account_type.account_type_name AS `accountsclassfication`,
	`account`.`account_id` AS `accountId`,
	`account`.`account_name` AS `accountName`,
	'' AS `transactionName`,
	account_payments.account_payment_description AS `transactionDescription`,
	0 AS `dr_amount`,
	`account_payments`.`amount_paid` AS `cr_amount`,
	`account_payments`.`payment_date` AS `transactionDate`,
	`account_payments`.`payment_date` AS `createdAt`,
	`account_payments`.`account_payment_deleted` AS `status`,
	2 AS `branch_id`,
	'Expense Payment' AS `transactionCategory`,
	'Purchase Payment' AS `transactionClassification`,
	'account_payments' AS `transactionTable`,
	'' AS `referenceTable`
FROM
account_payments
LEFT JOIN `account` ON 	account_payments.account_from_id = account.account_id
LEFT JOIN `account_type` ON	account_type.account_type_id = account.account_type_id		
WHERE account_payments.account_to_type = 4  AND account_payments.account_payment_deleted = 0


UNION ALL


SELECT

	payments.payment_id AS `transactionId`,
	'' AS `referenceId`,
	'' AS `payingFor`,
	payments.confirm_number AS `referenceCode`,
	payments.confirm_number AS `transactionCode`,
	payments.patient_id AS `patient_id`,
  	'' AS `recepientId`,
	account_type.account_type_id AS `accountParentId`,
	account_type.account_type_name AS `accountsclassfication`,
	`account`.`account_id` AS `accountId`,
	account.account_name AS `accountName`,
	'' AS `transactionName`,
	CONCAT( "Patient Payment: ",payment_method.payment_method," <strong>Receipt No:</strong> ",payments.confirm_number) AS `transactionDescription`,
	SUM(payment_item.payment_item_amount) AS `dr_amount`,
	0 AS `cr_amount`,
	payments.payment_date AS `transactionDate`,
	payments.payment_date AS `createdAt`,
	`payments`.`cancel` AS `status`,
	2 AS `branch_id`,
	'Patient Income' AS `transactionCategory`,
	'Payments' AS `transactionClassification`,
	'account_payments' AS `transactionTable`,
	'' AS `referenceTable`
FROM
	payments,payment_item,payment_method,account,visit_invoice,account_type
	WHERE payments.cancel = 0 AND payments.payment_type = 1 AND visit_invoice.visit_invoice_id = payment_item.visit_invoice_id AND payment_item.invoice_type = 1
	AND payments.payment_id = payment_item.payment_id 
	AND payments.payment_method_id = payment_method.payment_method_id
	AND payment_method.account_id = account.account_id
	AND account.account_type_id = account_type.account_type_id
GROUP BY payment_item.payment_id


UNION ALL 


SELECT

	payments.payment_id AS `transactionId`,
	'' AS `referenceId`,
	'' AS `payingFor`,
	payments.confirm_number AS `referenceCode`,
	payments.confirm_number AS `transactionCode`,
	payments.patient_id AS `patient_id`,
  	'' AS `recepientId`,
	account_type.account_type_id AS `accountParentId`,
	account_type.account_type_name AS `accountsclassfication`,
	`account`.`account_id` AS `accountId`,
	account.parent_account AS `accountName`,
	'' AS `transactionName`,
	CONCAT("Payment On Account") AS `transactionDescription`,
	SUM(payment_item.payment_item_amount) AS `dr_amount`,
	0 AS `cr_amount`,
	payments.payment_date AS `transactionDate`,
	payments.payment_date AS `createdAt`,
	`payments`.`cancel` AS `status`,
	2 AS `branch_id`,
	'Patient Income' AS `transactionCategory`,
	'Payments' AS `transactionClassification`,
	'account_payments' AS `transactionTable`,
	'' AS `referenceTable`
FROM
	payments,payment_item,payment_method,account,account_type
	WHERE payments.cancel = 0 AND payments.payment_type = 1 AND payment_item.invoice_type = 3
	AND payments.payment_id = payment_item.payment_id 
	AND payments.payment_method_id = payment_method.payment_method_id
	AND payment_method.account_id = account.account_id
	AND account.account_type_id = account_type.account_type_id
GROUP BY payment_item.payment_id

UNION ALL
-- bank reconcilliation


SELECT

	bank_reconcilliation.recon_id AS `transactionId`,
	'' AS `referenceId`,
	'' AS `payingFor`,
	'' AS `referenceCode`,
	'' AS `transactionCode`,
	'' AS `patient_id`,
  	'' AS `recepientId`,
	account_type.account_type_id AS `accountParentId`,
	account_type.account_type_name AS `accountsclassfication`,
	`account`.`account_id` AS `accountId`,
	account.account_name AS `accountName`,
	'' AS `transactionName`,
	CONCAT("Deposit") AS `transactionDescription`,
	SUM(bank_reconcilliation.interest_earned) AS `dr_amount`,
	0 AS `cr_amount`,
	bank_reconcilliation.interest_date AS `transactionDate`,
	bank_reconcilliation.interest_date AS `createdAt`,
	bank_reconcilliation.recon_status AS `status`,
	2 AS `branch_id`,
	'Deposit' AS `transactionCategory`,
	'Payments' AS `transactionClassification`,
	'account_payments' AS `transactionTable`,
	'' AS `referenceTable`
FROM
	bank_reconcilliation,account,account_type
	WHERE bank_reconcilliation.recon_status = 2
	AND bank_reconcilliation.account_id = account.account_id
	AND account.account_type_id = account_type.account_type_id
GROUP BY bank_reconcilliation.recon_id



UNION ALL

SELECT

	bank_reconcilliation.recon_id AS `transactionId`,
	'' AS `referenceId`,
	'' AS `payingFor`,
	'' AS `referenceCode`,
	'' AS `transactionCode`,
	'' AS `patient_id`,
  	'' AS `recepientId`,
	account_type.account_type_id AS `accountParentId`,
	account_type.account_type_name AS `accountsclassfication`,
	`account`.`account_id` AS `accountId`,
	account.account_name AS `accountName`,
	'' AS `transactionName`,
	CONCAT("Deposit") AS `transactionDescription`,
	0 AS `dr_amount`,
	SUM(bank_reconcilliation.interest_earned) AS `cr_amount`,
	bank_reconcilliation.interest_date AS `transactionDate`,
	bank_reconcilliation.interest_date AS `createdAt`,
	bank_reconcilliation.recon_status AS `status`,
	2 AS `branch_id`,
	'Deposit' AS `transactionCategory`,
	'Payments' AS `transactionClassification`,
	'account_payments' AS `transactionTable`,
	'' AS `referenceTable`
FROM
	bank_reconcilliation,account,account_type
	WHERE bank_reconcilliation.recon_status = 2
	AND bank_reconcilliation.interest_account_id = account.account_id
	AND account.account_type_id = account_type.account_type_id
GROUP BY bank_reconcilliation.recon_id



UNION ALL



-- get the expense to be displayed



SELECT

	bank_reconcilliation.recon_id AS `transactionId`,
	'' AS `referenceId`,
	'' AS `payingFor`,
	'' AS `referenceCode`,
	'' AS `transactionCode`,
	'' AS `patient_id`,
  	'' AS `recepientId`,
	account_type.account_type_id AS `accountParentId`,
	account_type.account_type_name AS `accountsclassfication`,
	`account`.`account_id` AS `accountId`,
	account.account_name AS `accountName`,
	'' AS `transactionName`,
	CONCAT("Deposit") AS `transactionDescription`,
	0 AS `dr_amount`,
	SUM(bank_reconcilliation.service_charged) AS `cr_amount`,
	bank_reconcilliation.charged_date AS `transactionDate`,
	bank_reconcilliation.charged_date AS `createdAt`,
	bank_reconcilliation.recon_status AS `status`,
	2 AS `branch_id`,
	'Expense Payment' AS `transactionCategory`,
	'Payments' AS `transactionClassification`,
	'account_payments' AS `transactionTable`,
	'' AS `referenceTable`
FROM
	bank_reconcilliation,account,account_type
	WHERE bank_reconcilliation.recon_status = 2
	AND bank_reconcilliation.account_id = account.account_id
	AND account.account_type_id = account_type.account_type_id
GROUP BY bank_reconcilliation.recon_id



UNION ALL


SELECT

	bank_reconcilliation.recon_id AS `transactionId`,
	'' AS `referenceId`,
	'' AS `payingFor`,
	'' AS `referenceCode`,
	'' AS `transactionCode`,
	'' AS `patient_id`,
  	'' AS `recepientId`,
	account_type.account_type_id AS `accountParentId`,
	account_type.account_type_name AS `accountsclassfication`,
	`account`.`account_id` AS `accountId`,
	account.account_name AS `accountName`,
	'' AS `transactionName`,
	CONCAT("Deposit") AS `transactionDescription`,
	SUM(bank_reconcilliation.service_charged) AS `dr_amount`,
	0 AS `cr_amount`,
	bank_reconcilliation.charged_date AS `transactionDate`,
	bank_reconcilliation.charged_date AS `createdAt`,
	bank_reconcilliation.recon_status AS `status`,
	2 AS `branch_id`,
	'Expense Payment' AS `transactionCategory`,
	'Payments' AS `transactionClassification`,
	'account_payments' AS `transactionTable`,
	'' AS `referenceTable`
FROM
	bank_reconcilliation,account,account_type
	WHERE bank_reconcilliation.recon_status = 2
	AND bank_reconcilliation.expense_account_id = account.account_id
	AND account.account_type_id = account_type.account_type_id
GROUP BY bank_reconcilliation.recon_id




UNION ALL 

  SELECT
    `payroll_payment_item`.`payroll_payment_item_id` AS `transactionId`,
    `payroll_payment`.`payroll_payment_id` AS `referenceId`,
    `payroll_payment_item`.`payroll_invoice_id` AS `payingFor`,
    `payroll_payment`.`reference_number` AS `referenceCode`,
    `payroll_payment`.`document_number` AS `transactionCode`,
    '' AS `patient_id`,
    `statutory_accounts`.`statutory_account_id` AS `recepientId`,
    `account`.`parent_account` AS `accountParentId`,
    `account_type`.`account_type_name` AS `accountsclassfication`,
    `payroll_payment`.`account_from_id` AS `accountId`,
    `account`.`account_name` AS `accountName`,
    CONCAT(statutory_accounts.statutory_account_name,'-',MONTH(payroll_summary.payroll_created_for),' ',YEAR(payroll_summary.payroll_created_for)) AS `transactionName`,
    CONCAT('Payment for invoice of ',' ',`payroll_summary`.`payroll_created_for`)  AS `transactionDescription`,
    0 AS `dr_amount`,
    `payroll_payment_item`.`amount_paid` AS `cr_amount`,
    `payroll_payment`.`transaction_date` AS `transactionDate`,
    `payroll_payment`.`created` AS `createdAt`,
    `payroll_payment_item`.`payroll_payment_item_status` AS `status`,
    2 AS `branch_id`,
    'Expense Payment' AS `transactionCategory`,
    'Creditors Invoices Payments' AS `transactionClassification`,
    'payroll_payment' AS `transactionTable`,
    'payroll_payment_item' AS `referenceTable`
  FROM
    (
      (
        (
          `payroll_payment_item`,payroll_payment,payroll_summary,statutory_accounts
          
        )
        JOIN account ON(
          (
            account.account_id = payroll_payment.account_from_id
          )
        )
      )
      JOIN `account_type` ON(
        (
          account_type.account_type_id = account.account_type_id
        )
      )
      
    )
    WHERE payroll_payment_item.invoice_type = 0 
    AND payroll_payment.payroll_payment_id = payroll_payment_item.payroll_payment_id 
    AND payroll_payment.payroll_payment_status = 1
    AND payroll_summary.payroll_summary_id = payroll_payment_item.payroll_payment_id 
    AND statutory_accounts.statutory_account_id = payroll_payment_item.payroll_id

UNION ALL



 SELECT
    `payroll_payment_item`.`payroll_payment_item_id` AS `transactionId`,
    `payroll_payment`.`payroll_payment_id` AS `referenceId`,
    `payroll_payment_item`.`payroll_invoice_id` AS `payingFor`,
    `payroll_payment`.`reference_number` AS `referenceCode`,
    `payroll_payment`.`document_number` AS `transactionCode`,
    '' AS `patient_id`,
    `statutory_accounts`.`statutory_account_id` AS `recepientId`,
    `account`.`parent_account` AS `accountParentId`,
    `account_type`.`account_type_name` AS `accountsclassfication`,
    statutory_accounts.account_id AS `accountId`,
    `account`.`account_name` AS `accountName`,
    CONCAT(statutory_accounts.statutory_account_name,'-',MONTH(payroll_summary.payroll_created_for),' ',YEAR(payroll_summary.payroll_created_for)) AS `transactionName`,
    CONCAT('Payment for invoice of ',' ',`payroll_summary`.`payroll_created_for`)  AS `transactionDescription`,
    `payroll_payment_item`.`amount_paid` AS `dr_amount`,
    0 AS `cr_amount`,
    `payroll_payment`.`transaction_date` AS `transactionDate`,
    `payroll_payment`.`created` AS `createdAt`,
    `payroll_payment_item`.`payroll_payment_item_status` AS `status`,
    2 AS `branch_id`,
    'Expense Payment' AS `transactionCategory`,
    'Creditors Invoices Payments' AS `transactionClassification`,
    'payroll_payment' AS `transactionTable`,
    'payroll_payment_item' AS `referenceTable`
  FROM
    (
      (
        (
          `payroll_payment_item`,payroll_payment,payroll_summary,statutory_accounts
          
        )
        JOIN account ON(
          (
            account.account_id = statutory_accounts.account_id
          )
        )
      )
      JOIN `account_type` ON(
        (
          account_type.account_type_id = account.account_type_id
        )
      )
      
    )
    WHERE payroll_payment_item.invoice_type = 0 
    AND payroll_payment.payroll_payment_id = payroll_payment_item.payroll_payment_id 
    AND payroll_payment.payroll_payment_status = 1
    AND payroll_summary.payroll_summary_id = payroll_payment_item.payroll_payment_id 
    AND statutory_accounts.statutory_account_id = payroll_payment_item.payroll_id

;





CREATE OR REPLACE VIEW v_account_ledger_by_date AS select * from v_account_ledger ORDER BY createdAt;
