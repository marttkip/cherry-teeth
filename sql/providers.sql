
CREATE OR REPLACE VIEW v_provider_ledger_aging AS
-- Creditr Invoices

SELECT
	`provider`.`provider_id` AS `transactionId`,
	'' AS `referenceId`,
	'' AS `payingFor`,
	'' AS `referenceCode`,
	'' AS `transactionCode`,
	'' AS `patient_id`,
    `provider`.`provider_id` AS `recepientId`,
	'' AS `accountParentId`,
	'' AS `accountsclassfication`,
	'' AS `accountId`,
	'' AS `accountName`,
	'' AS `transactionName`,
	CONCAT('Opening Balance from',' ',`provider`.`start_date`) AS `transactionDescription`,
  	0 AS `department_id`,
	`provider`.`opening_balance` AS `dr_amount`,
	'0' AS `cr_amount`,
	`provider`.`start_date` AS `transactionDate`,
	`provider`.`start_date` AS `createdAt`,
	`provider`.`start_date` AS `referenceDate`,
	`provider`.`provider_status` AS `status`,
	'Expense' AS `transactionCategory`,
	'provider Opening Balance' AS `transactionClassification`,
	'' AS `transactionTable`,
	'provider' AS `referenceTable`
FROM
provider
WHERE debit_id = 2

UNION ALL

SELECT
	`provider`.`provider_id` AS `transactionId`,
	'' AS `referenceId`,
	'' AS `payingFor`,
	'' AS `referenceCode`,
	'' AS `transactionCode`,
	'' AS `patient_id`,
  `provider`.`provider_id` AS `recepientId`,
	'' AS `accountParentId`,
	'' AS `accountsclassfication`,
	'' AS `accountId`,
	'' AS `accountName`,
	'' AS `transactionName`,
	CONCAT('Opening Balance from',' ',`provider`.`start_date`) AS `transactionDescription`,
  0 AS `department_id`,
	'0' AS `dr_amount`,
	`provider`.`opening_balance` AS `cr_amount`,
	`provider`.`start_date` AS `transactionDate`,
	`provider`.`start_date` AS `createdAt`,
	`provider`.`start_date` AS `referenceDate`,
	`provider`.`provider_status` AS `status`,
	'Expense' AS `transactionCategory`,
	'provider Opening Balance' AS `transactionClassification`,
	'' AS `transactionTable`,
	'provider' AS `referenceTable`
FROM
provider
WHERE debit_id = 1

UNION ALL
SELECT
	`provider_invoice_item`.`provider_invoice_item_id` AS `transactionId`,
	`provider_invoice`.`provider_invoice_id` AS `referenceId`,
	'' AS `payingFor`,
	`provider_invoice`.`invoice_number` AS `referenceCode`,
	`provider_invoice`.`document_number` AS `transactionCode`,
	'' AS `patient_id`,
  `provider_invoice`.`provider_id` AS `recepientId`,
	`account`.`parent_account` AS `accountParentId`,
	`account_type`.`account_type_name` AS `accountsclassfication`,
	`provider_invoice_item`.`account_to_id` AS `accountId`,
	`account`.`account_name` AS `accountName`,
	`provider_invoice_item`.`item_description` AS `transactionName`,
	`provider_invoice_item`.`item_description` AS `transactionDescription`,
	
  	'0' AS `department_id`,
	`provider_invoice_item`.`total_amount` AS `dr_amount`,
	'0' AS `cr_amount`,
	`provider_invoice`.`transaction_date` AS `transactionDate`,
	`provider_invoice`.`created` AS `createdAt`,
	`provider_invoice`.`transaction_date` AS `referenceDate`,
	`provider_invoice_item`.`provider_invoice_item_status` AS `status`,
	'Expense' AS `transactionCategory`,
	'providers Invoices' AS `transactionClassification`,
	'provider_invoice_item' AS `transactionTable`,
	'provider_invoice' AS `referenceTable`
FROM
	(
		(
			(
				`provider_invoice_item`,provider_invoice,provider
				
			)
			JOIN account ON(
				(
					account.account_id = provider_invoice_item.account_to_id
				)
			)
		)
		JOIN `account_type` ON(
			(
				account_type.account_type_id = account.account_type_id
			)
		)
	)
WHERE 
provider_invoice.provider_invoice_id = provider_invoice_item.provider_invoice_id 
AND provider_invoice.provider_invoice_status = 1
AND provider.provider_id = provider_invoice.provider_id 
AND provider_invoice.transaction_date >= provider.start_date


UNION ALL

SELECT
	`provider_credit_note_item`.`provider_credit_note_item_id` AS `transactionId`,
	`provider_credit_note`.`provider_credit_note_id` AS `referenceId`,
	`provider_credit_note_item`.`provider_invoice_id` AS `payingFor`,
	`provider_credit_note`.`invoice_number` AS `referenceCode`,
	`provider_credit_note`.`document_number` AS `transactionCode`,
	'' AS `patient_id`,
  `provider_credit_note`.`provider_id` AS `recepientId`,
	`account`.`parent_account` AS `accountParentId`,
	`account_type`.`account_type_name` AS `accountsclassfication`,
	`provider_credit_note`.`account_from_id` AS `accountId`,
	`account`.`account_name` AS `accountName`,
	`provider_credit_note_item`.`description` AS `transactionName`,
	`provider_credit_note_item`.`description` AS `transactionDescription`,
  0 AS `department_id`,
	0 AS `dr_amount`,
	`provider_credit_note_item`.`credit_note_amount` AS `cr_amount`,
	`provider_credit_note`.`transaction_date` AS `transactionDate`,
	`provider_credit_note`.`created` AS `createdAt`,
	`provider_invoice`.`transaction_date` AS `referenceDate`,
	`provider_credit_note_item`.`provider_credit_note_item_status` AS `status`,
	'Expense Payment' AS `transactionCategory`,
	'providers Credit Notes' AS `transactionClassification`,
	'provider_credit_note' AS `transactionTable`,
	'provider_credit_note_item' AS `referenceTable`
FROM
	(
		(
			(
				`provider_credit_note_item`,provider_credit_note,provider_invoice,provider
				
			)
			JOIN account ON(
				(
					account.account_id = provider_credit_note.account_from_id
				)
			)
			
		)
		JOIN `account_type` ON(
			(
				account_type.account_type_id = account.account_type_id
			)
		)
	)
WHERE 
	provider_credit_note.provider_credit_note_id = provider_credit_note_item.provider_credit_note_id 
	AND provider_credit_note.provider_credit_note_status = 1
	AND provider_invoice.provider_invoice_id = provider_credit_note_item.provider_invoice_id
	AND provider_invoice.provider_invoice_status = 1
	AND provider.provider_id = provider_invoice.provider_id 
	AND provider_invoice.transaction_date >= provider.start_date

UNION ALL
		-- credit invoice payments

		  SELECT
			`provider_payment_item`.`provider_payment_item_id` AS `transactionId`,
			`provider_payment`.`provider_payment_id` AS `referenceId`,
			`provider_payment_item`.`provider_invoice_id` AS `payingFor`,
			`provider_payment`.`reference_number` AS `referenceCode`,
			`provider_payment`.`document_number` AS `transactionCode`,
			'' AS `patient_id`,
		  	`provider_payment`.`provider_id` AS `recepientId`,
			`account`.`parent_account` AS `accountParentId`,
			`account_type`.`account_type_name` AS `accountsclassfication`,
			`provider_payment`.`account_from_id` AS `accountId`,
			`account`.`account_name` AS `accountName`,
			`provider_payment_item`.`description` AS `transactionName`,
			CONCAT('Payment for invoice of ',' ',`provider_invoice`.`invoice_number`)  AS `transactionDescription`,
      		0 AS `department_id`,
			0 AS `dr_amount`,
			`provider_payment_item`.`amount_paid` AS `cr_amount`,
			`provider_payment`.`transaction_date` AS `transactionDate`,
			`provider_payment`.`created` AS `createdAt`,
			`provider_invoice`.`transaction_date` AS `referenceDate`,
			`provider_payment_item`.`provider_payment_item_status` AS `status`,
			'Expense Payment' AS `transactionCategory`,
			'providers Invoices Payments' AS `transactionClassification`,
			'provider_payment' AS `transactionTable`,
			'provider_payment_item' AS `referenceTable`
		FROM
			(
				(
					(
						`provider_payment_item`,provider_payment,provider_invoice,provider
						
					)
					JOIN account ON(
						(
							account.account_id = provider_payment.account_from_id
						)
					)
				)
				JOIN `account_type` ON(
					(
						account_type.account_type_id = account.account_type_id
					)
				)
				
			)
			WHERE provider_payment_item.invoice_type = 0 
			AND provider_payment.provider_payment_id = provider_payment_item.provider_payment_id 
			AND provider_payment.provider_payment_status = 1
			AND provider_invoice.provider_invoice_id = provider_payment_item.provider_invoice_id 
			AND provider_invoice.provider_invoice_status = 1  
			AND provider.provider_id = provider_invoice.provider_id AND provider_invoice.transaction_date >= provider.start_date

	
UNION ALL

SELECT
			`provider_payment_item`.`provider_payment_item_id` AS `transactionId`,
			`provider_payment`.`provider_payment_id` AS `referenceId`,
			`provider_payment_item`.`provider_invoice_id` AS `payingFor`,
			`provider_payment`.`reference_number` AS `referenceCode`,
			`provider_payment`.`document_number` AS `transactionCode`,
			'' AS `patient_id`,
		  `provider_payment`.`provider_id` AS `recepientId`,
			`account`.`parent_account` AS `accountParentId`,
			`account_type`.`account_type_name` AS `accountsclassfication`,
			`provider_payment`.`account_from_id` AS `accountId`,
			`account`.`account_name` AS `accountName`,
			`provider_payment_item`.`description` AS `transactionName`,
			CONCAT('Payment of opening balance')  AS `transactionDescription`,
      		0 AS `department_id`,
			0 AS `dr_amount`,
			`provider_payment_item`.`amount_paid` AS `cr_amount`,
			`provider_payment`.`transaction_date` AS `transactionDate`,
			`provider_payment`.`created` AS `createdAt`,
			`provider`.`start_date` AS `referenceDate`,
			`provider_payment_item`.`provider_payment_item_status` AS `status`,
			'Expense Payment' AS `transactionCategory`,
			'providers Invoices Payments' AS `transactionClassification`,
			'provider_payment' AS `transactionTable`,
			'provider_payment_item' AS `referenceTable`
		FROM
			(
				(
					(
						`provider_payment_item`,provider_payment,provider
						
					)
					JOIN account ON(
						(
							account.account_id = provider_payment.account_from_id
						)
					)
				)
				JOIN `account_type` ON(
					(
						account_type.account_type_id = account.account_type_id
					)
				)
				
			)
			WHERE provider_payment_item.invoice_type = 2 
			AND provider_payment.provider_payment_id = provider_payment_item.provider_payment_id 
			AND provider_payment.provider_payment_status = 1
			AND provider.provider_id = provider_payment_item.provider_id
			AND provider_payment.transaction_date >= provider.start_date

			UNION ALL

			SELECT
			`provider_payment_item`.`provider_payment_item_id` AS `transactionId`,
			`provider_payment`.`provider_payment_id` AS `referenceId`,
			`provider_payment_item`.`provider_invoice_id` AS `payingFor`,
			`provider_payment`.`reference_number` AS `referenceCode`,
			`provider_payment`.`document_number` AS `transactionCode`,
			'' AS `patient_id`,
		  `provider_payment`.`provider_id` AS `recepientId`,
			`account`.`parent_account` AS `accountParentId`,
			`account_type`.`account_type_name` AS `accountsclassfication`,
			`provider_payment`.`account_from_id` AS `accountId`,
			`account`.`account_name` AS `accountName`,
			`provider_payment_item`.`description` AS `transactionName`,
			CONCAT('Payment on account')  AS `transactionDescription`,
      		0 AS `department_id`,
			0 AS `dr_amount`,
			`provider_payment_item`.`amount_paid` AS `cr_amount`,
			`provider_payment`.`transaction_date` AS `transactionDate`,
			`provider_payment`.`created` AS `createdAt`,
			`provider_payment`.`created` AS `referenceDate`,
			`provider_payment_item`.`provider_payment_item_status` AS `status`,
			'Expense Payment' AS `transactionCategory`,
			'providers Invoices Payments' AS `transactionClassification`,
			'provider_payment' AS `transactionTable`,
			'provider_payment_item' AS `referenceTable`
		FROM
			(
				(
					(
						`provider_payment_item`,provider_payment,provider
						
					)
					JOIN account ON(
						(
							account.account_id = provider_payment.account_from_id
						)
					)
				)
				JOIN `account_type` ON(
					(
						account_type.account_type_id = account.account_type_id
					)
				)
				
			)
			WHERE provider_payment_item.invoice_type = 3 AND provider_payment.provider_payment_id = provider_payment_item.provider_payment_id 
				AND provider_payment.provider_payment_status = 1 
				AND provider.provider_id = provider_payment.provider_id AND provider_payment.transaction_date >= provider.start_date ;


CREATE OR REPLACE VIEW v_provider_ledger_aging_by_date AS SELECT * FROM v_provider_ledger_aging ORDER BY transactionDate ASC;


CREATE OR REPLACE VIEW v_aged_providers AS
-- Creditr Invoices
SELECT
	provider.provider_id AS recepientId,
	provider.provider_name as payables,
	provider.branch_id as branch_id,
  (
    CASE
      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_provider_ledger_aging.referenceDate ) )  = 0, v_provider_ledger_aging.dr_amount, 0 )) = 0 -- condtion to see if the date has no bills
       THEN 0 -- Output
      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_provider_ledger_aging.referenceDate ) )  = 0, v_provider_ledger_aging.dr_amount, 0 )) > 0 -- Checking if the value of bills in the date range is greater than Zero
       THEN
       -- Add the paymensts made to the specifc invoice
        Sum(IF( DATEDIFF( CURDATE( ), date( v_provider_ledger_aging.referenceDate ) )  = 0, v_provider_ledger_aging.dr_amount, 0 ))
        - Sum(IF( DATEDIFF( CURDATE( ), date( v_provider_ledger_aging.referenceDate ) )  = 0, v_provider_ledger_aging.cr_amount, 0 ))
        END
  ) AS `coming_due`,
	(
    CASE
      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_provider_ledger_aging.referenceDate ) ) BETWEEN 1 AND 30, v_provider_ledger_aging.dr_amount, 0 )) = 0 -- condtion to see if the date has no bills
       THEN 0 -- Output
      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_provider_ledger_aging.referenceDate ) ) BETWEEN 1 AND 30, v_provider_ledger_aging.dr_amount, 0 )) > 0 -- Checking if the value of bills in the date range is greater than Zero
       THEN
       -- Add the paymensts made to the specifc invoice
        Sum(IF( DATEDIFF( CURDATE( ), date( v_provider_ledger_aging.referenceDate ) ) BETWEEN 1 AND 30, v_provider_ledger_aging.dr_amount, 0 ))
				  - Sum(IF( DATEDIFF( CURDATE( ), date( v_provider_ledger_aging.referenceDate ) )  BETWEEN 1 AND 30, v_provider_ledger_aging.cr_amount, 0 ))
			END
  ) AS `thirty_days`,
  (
    CASE
      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_provider_ledger_aging.referenceDate ) ) BETWEEN 31 AND 60, v_provider_ledger_aging.dr_amount, 0 )) = 0 -- condtion to see if the date has no bills
       THEN 0 -- Output
      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_provider_ledger_aging.referenceDate ) ) BETWEEN 31 AND 60, v_provider_ledger_aging.dr_amount, 0 )) > 0 -- Checking if the value of bills in the date range is greater than Zero
       THEN
       -- Add the paymensts made to the specifc invoice
        Sum(IF( DATEDIFF( CURDATE( ), date( v_provider_ledger_aging.referenceDate ) ) BETWEEN 31 AND 60, v_provider_ledger_aging.dr_amount, 0 ))
				   - Sum(IF( DATEDIFF( CURDATE( ), date( v_provider_ledger_aging.referenceDate ) ) BETWEEN 31 AND 60, v_provider_ledger_aging.cr_amount, 0 ))
			END
  ) AS `sixty_days`,
  (
    CASE
      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_provider_ledger_aging.referenceDate ) ) BETWEEN 61 AND 90, v_provider_ledger_aging.dr_amount, 0 )) = 0 -- condtion to see if the date has no bills
       THEN 0 -- Output
      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_provider_ledger_aging.referenceDate ) ) BETWEEN 61 AND 90, v_provider_ledger_aging.dr_amount, 0 )) > 0 -- Checking if the value of bills in the date range is greater than Zero
       THEN
       -- Add the paymensts made to the specifc invoice
        Sum(IF( DATEDIFF( CURDATE( ), date( v_provider_ledger_aging.referenceDate ) ) BETWEEN 61 AND 90, v_provider_ledger_aging.dr_amount, 0 ))
				   - Sum(IF( DATEDIFF( CURDATE( ), date( v_provider_ledger_aging.referenceDate ) )  BETWEEN 61 AND 90, v_provider_ledger_aging.cr_amount, 0 ))
		END
  ) AS `ninety_days`,
  (
    CASE
      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_provider_ledger_aging.referenceDate ) ) >90, v_provider_ledger_aging.dr_amount, 0 )) = 0 -- condtion to see if the date has no bills
       THEN 0 -- Output
      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_provider_ledger_aging.referenceDate ) ) >90, v_provider_ledger_aging.dr_amount, 0 )) > 0 -- Checking if the value of bills in the date range is greater than Zero
       THEN
       -- Add the paymensts made to the specifc invoice
        Sum(IF( DATEDIFF( CURDATE( ), date( v_provider_ledger_aging.referenceDate ) ) >90, v_provider_ledger_aging.dr_amount, 0 ))
				 - Sum(IF( DATEDIFF( CURDATE( ), date( v_provider_ledger_aging.referenceDate ) )  >90, v_provider_ledger_aging.cr_amount, 0 ))
			END
  ) AS `over_ninety_days`,

  (

    (
      CASE
        WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_provider_ledger_aging.referenceDate ) )  = 0, v_provider_ledger_aging.dr_amount, 0 )) = 0 -- condtion to see if the date has no bills
         THEN 0 -- Output
        WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_provider_ledger_aging.referenceDate ) )  = 0, v_provider_ledger_aging.dr_amount, 0 )) > 0 -- Checking if the value of bills in the date range is greater than Zero
         THEN
         -- Add the paymensts made to the specifc invoice
          Sum(IF( DATEDIFF( CURDATE( ), date( v_provider_ledger_aging.referenceDate ) )  = 0, v_provider_ledger_aging.dr_amount, 0 ))
					 - Sum(IF( DATEDIFF( CURDATE( ), date( v_provider_ledger_aging.referenceDate ) )  = 0, v_provider_ledger_aging.cr_amount, 0 ))
  		END
    )-- Getting the Value for 0 Days
    + (
      CASE
        WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_provider_ledger_aging.referenceDate ) ) BETWEEN 1 AND 30, v_provider_ledger_aging.dr_amount, 0 )) = 0 -- condtion to see if the date has no bills
         THEN 0 -- Output
        WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_provider_ledger_aging.referenceDate ) ) BETWEEN 1 AND 30, v_provider_ledger_aging.dr_amount, 0 )) > 0 -- Checking if the value of bills in the date range is greater than Zero
         THEN
         -- Add the paymensts made to the specifc invoice
          Sum(IF( DATEDIFF( CURDATE( ), date( v_provider_ledger_aging.referenceDate ) ) BETWEEN 1 AND 30, v_provider_ledger_aging.dr_amount, 0 ))
 - Sum(IF( DATEDIFF( CURDATE( ), date( v_provider_ledger_aging.referenceDate ) )  BETWEEN 1 AND 30, v_provider_ledger_aging.cr_amount, 0 ))
  		END
    ) --  AS `1-30 Days`
    +(
      CASE
        WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_provider_ledger_aging.referenceDate ) ) BETWEEN 31 AND 60, v_provider_ledger_aging.dr_amount, 0 )) = 0 -- condtion to see if the date has no bills
         THEN 0 -- Output
        WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_provider_ledger_aging.referenceDate ) ) BETWEEN 31 AND 60, v_provider_ledger_aging.dr_amount, 0 )) > 0 -- Checking if the value of bills in the date range is greater than Zero
         THEN
         -- Add the paymensts made to the specifc invoice
          Sum(IF( DATEDIFF( CURDATE( ), date( v_provider_ledger_aging.referenceDate ) ) BETWEEN 31 AND 60, v_provider_ledger_aging.dr_amount, 0 ))

				   - Sum(IF( DATEDIFF( CURDATE( ), date( v_provider_ledger_aging.referenceDate ) )  BETWEEN 31 AND 60, v_provider_ledger_aging.cr_amount, 0 ))
				END

    ) -- AS `31-60 Days`
    +(
      CASE
        WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_provider_ledger_aging.referenceDate ) ) BETWEEN 61 AND 90, v_provider_ledger_aging.dr_amount, 0 )) = 0 -- condtion to see if the date has no bills
         THEN 0 -- Output
        WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_provider_ledger_aging.referenceDate ) ) BETWEEN 61 AND 90, v_provider_ledger_aging.dr_amount, 0 )) > 0 -- Checking if the value of bills in the date range is greater than Zero
         THEN
         -- Add the paymensts made to the specifc invoice
          Sum(IF( DATEDIFF( CURDATE( ), date( v_provider_ledger_aging.referenceDate ) ) BETWEEN 61 AND 90, v_provider_ledger_aging.dr_amount, 0 ))
				   - Sum(IF( DATEDIFF( CURDATE( ), date( v_provider_ledger_aging.referenceDate ) ) BETWEEN 61 AND 90, v_provider_ledger_aging.cr_amount, 0 ))
  		END
    ) -- AS `61-90 Days`
    +(
      CASE
        WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_provider_ledger_aging.referenceDate ) ) >90, v_provider_ledger_aging.dr_amount, 0 )) = 0 -- condtion to see if the date has no bills
         THEN 0 -- Output
        WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_provider_ledger_aging.referenceDate ) ) >90, v_provider_ledger_aging.dr_amount, 0 )) > 0 -- Checking if the value of bills in the date range is greater than Zero
         THEN
         -- Add the paymensts made to the specifc invoice
          Sum(IF( DATEDIFF( CURDATE( ), date( v_provider_ledger_aging.referenceDate ) ) >90, v_provider_ledger_aging.dr_amount, 0 ))
				  - Sum(IF( DATEDIFF( CURDATE( ), date( v_provider_ledger_aging.referenceDate ) )  >90, v_provider_ledger_aging.cr_amount, 0 ))
  		END
    ) -- AS `>90 Days`
  ) AS `Total`,
SUM(v_provider_ledger_aging.dr_amount) AS total_dr,
SUM(v_provider_ledger_aging.cr_amount) AS total_cr

	FROM
		provider
	LEFT JOIN v_provider_ledger_aging ON v_provider_ledger_aging.recepientId = provider.provider_id AND v_provider_ledger_aging.recepientId > 0 AND v_provider_ledger_aging.referenceDate >= provider.start_date
	WHERE provider.provider_status = 0
	 GROUP BY provider.provider_id;