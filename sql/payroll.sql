CREATE OR REPLACE VIEW v_payroll AS
SELECT 
payroll.payroll_id,
(SELECT SUM(payroll_item.payroll_item_amount) FROM payroll_item,`table`
WHERE payroll_item.payroll_id = payroll.payroll_id AND `table`.table_id = payroll_item.table AND `table`.table_type = 1 AND `payroll_item`.table = 7)
as gross_payroll,
(SELECT SUM(payroll_item.payroll_item_amount) FROM payroll_item,`table`
WHERE payroll_item.payroll_id = payroll.payroll_id AND `table`.table_id = payroll_item.table AND `table`.table_type = 1)
as total_additions,
(SELECT SUM(payroll_item.payroll_item_amount) FROM payroll_item,`table`
WHERE payroll_item.payroll_id = payroll.payroll_id AND `table`.table_id = payroll_item.table AND `table`.table_type = 2)
as total_deductions,
(
(SELECT SUM(payroll_item.payroll_item_amount) FROM payroll_item,`table`
WHERE payroll_item.payroll_id = payroll.payroll_id AND `table`.table_id = payroll_item.table AND `table`.table_type = 1) - 
(SELECT SUM(payroll_item.payroll_item_amount) FROM payroll_item,`table`
WHERE payroll_item.payroll_id = payroll.payroll_id AND `table`.table_id = payroll_item.table AND `table`.table_type = 2)
)
as total_payroll,
(SELECT SUM(payroll_item.payroll_item_amount) FROM payroll_item,`table`
WHERE payroll_item.payroll_id = payroll.payroll_id AND `table`.table_id = payroll_item.table AND `table`.table_type = 2 AND payroll_item.table = 11)
AS total_nssf,
(SELECT SUM(payroll_item.payroll_item_amount) FROM payroll_item,`table`
WHERE payroll_item.payroll_id = payroll.payroll_id AND `table`.table_id = payroll_item.table AND `table`.table_type = 2 AND payroll_item.table = 6)
AS total_loans,
(SELECT SUM(payroll_item.payroll_item_amount) FROM payroll_item,`table`
WHERE payroll_item.payroll_id = payroll.payroll_id AND `table`.table_id = payroll_item.table AND `table`.table_type = 2 AND payroll_item.table = 12)
AS total_nhif,
((SELECT SUM(payroll_item.payroll_item_amount) FROM payroll_item,`table`
WHERE payroll_item.payroll_id = payroll.payroll_id AND `table`.table_id = payroll_item.table AND `table`.table_type = 2 AND payroll_item.table = 9) - (SELECT SUM(payroll_item.payroll_item_amount) FROM payroll_item,`table`
WHERE payroll_item.payroll_id = payroll.payroll_id AND `table`.table_id = payroll_item.table AND `table`.table_type = 1 AND payroll_item.table = 10))
AS total_paye,
(SELECT SUM(payroll_item.payroll_item_amount) FROM payroll_item,`table`
WHERE payroll_item.payroll_id = payroll.payroll_id AND `table`.table_id = payroll_item.table AND `table`.table_type = 1 AND payroll_item.table = 10)
AS total_relief,
payroll.payroll_year,payroll.month_id,payroll.payroll_created_for
from payroll
WHERE  payroll.payroll_status = 1 GROUP BY payroll.payroll_id;




CREATE OR REPLACE VIEW v_payroll_ledger_aging AS


SELECT
	payroll_summary.payroll_summary_id AS `transactionId`,
	payroll_summary.payroll_id AS `referenceId`,
	'' AS `payingFor`,
	CONCAT(`account`.`account_name`,'-',MONTH(payroll_summary.payroll_created_for),'-',YEAR(payroll_summary.payroll_created_for)) AS `referenceCode`,
	'' AS `transactionCode`,
	'' AS `patient_id`,
	statutory_accounts.statutory_account_id AS `recepientId`,
	`account`.`parent_account` AS `accountParentId`,
	`account_type`.`account_type_name` AS `accountsclassfication`,
	`account`.`account_id` AS `accountId`,
	`account`.`account_name` AS `accountName`,
	CONCAT('Payroll') AS `transactionName`,
	CONCAT('Payroll') AS `transactionDescription`,
	  0 AS `department_id`,
	payroll_summary.total_payroll AS `dr_amount`,
	0 AS `cr_amount`,
	payroll_summary.payroll_created_for AS `transactionDate`,
	payroll_summary.payroll_created_for  AS `createdAt`,
	payroll_summary.payroll_created_for  AS `referenceDate`,
	`payroll`.`payroll_status` AS `status`,
	'Payroll' AS `transactionCategory`,
	'Payroll Expense' AS `transactionClassification`,
	'finance_purchase' AS `transactionTable`,
	'' AS `referenceTable`
FROM
payroll_summary,payroll,statutory_accounts,account,account_type
WHERE payroll.payroll_id = payroll_summary.payroll_id
AND statutory_accounts.statutory_account_id = 1
AND account.account_id = statutory_accounts.account_id
AND payroll.payroll_status = 1
AND account_type.account_type_id = account.account_type_id


UNION ALL 


SELECT
	payroll_summary.payroll_summary_id AS `transactionId`,
	payroll_summary.payroll_id AS `referenceId`,
	'' AS `payingFor`,
	CONCAT(`account`.`account_name`,'-',MONTH(payroll_summary.payroll_created_for),'-',YEAR(payroll_summary.payroll_created_for)) AS `referenceCode`,
	'' AS `transactionCode`,
	'' AS `patient_id`,
	statutory_accounts.statutory_account_id AS `recepientId`,
	`account`.`parent_account` AS `accountParentId`,
	`account_type`.`account_type_name` AS `accountsclassfication`,
	`account`.`account_id` AS `accountId`,
	`account`.`account_name` AS `accountName`,
	CONCAT('PAYE') AS `transactionName`,
	CONCAT('PAYE') AS `transactionDescription`,
	  0 AS `department_id`,
	payroll_summary.paye AS `dr_amount`,
	0 AS `cr_amount`,
	payroll_summary.payroll_created_for AS `transactionDate`,
	payroll_summary.payroll_created_for  AS `createdAt`,
	payroll_summary.payroll_created_for  AS `referenceDate`,
	`payroll`.`payroll_status` AS `status`,
	'PAYE' AS `transactionCategory`,
	'Payroll Expense' AS `transactionClassification`,
	'finance_purchase' AS `transactionTable`,
	'' AS `referenceTable`
FROM
payroll_summary,payroll,statutory_accounts,account,account_type
WHERE payroll.payroll_id = payroll_summary.payroll_id
AND statutory_accounts.statutory_account_id = 2
AND account.account_id = statutory_accounts.account_id
			AND payroll.payroll_status = 1
AND account_type.account_type_id = account.account_type_id




UNION ALL 


SELECT
	payroll_summary.payroll_summary_id AS `transactionId`,
	payroll_summary.payroll_id AS `referenceId`,
	'' AS `payingFor`,
	CONCAT(`account`.`account_name`,'-',MONTH(payroll_summary.payroll_created_for),'-',YEAR(payroll_summary.payroll_created_for)) AS `referenceCode`,
	'' AS `transactionCode`,
	'' AS `patient_id`,
	statutory_accounts.statutory_account_id AS `recepientId`,
	`account`.`parent_account` AS `accountParentId`,
	`account_type`.`account_type_name` AS `accountsclassfication`,
	`account`.`account_id` AS `accountId`,
	`account`.`account_name` AS `accountName`,
	CONCAT('NSSF') AS `transactionName`,
	CONCAT('NSSF') AS `transactionDescription`,
	  0 AS `department_id`,
	payroll_summary.nssf AS `dr_amount`,
	0 AS `cr_amount`,
	payroll_summary.payroll_created_for AS `transactionDate`,
	payroll_summary.payroll_created_for  AS `createdAt`,
	payroll_summary.payroll_created_for  AS `referenceDate`,
	`payroll`.`payroll_status` AS `status`,
	'PAYE' AS `transactionCategory`,
	'Payroll Expense' AS `transactionClassification`,
	'finance_purchase' AS `transactionTable`,
	'' AS `referenceTable`
FROM
payroll_summary,payroll,statutory_accounts,account,account_type
WHERE payroll.payroll_id = payroll_summary.payroll_id
AND statutory_accounts.statutory_account_id = 3
AND account.account_id = statutory_accounts.account_id
AND payroll.payroll_status = 1
AND account_type.account_type_id = account.account_type_id


UNION ALL




SELECT
	payroll_summary.payroll_summary_id AS `transactionId`,
	payroll_summary.payroll_id AS `referenceId`,
	'' AS `payingFor`,
	CONCAT(`account`.`account_name`,'-',MONTH(payroll_summary.payroll_created_for),'-',YEAR(payroll_summary.payroll_created_for)) AS `referenceCode`,
	'' AS `transactionCode`,
	'' AS `patient_id`,
	statutory_accounts.statutory_account_id AS `recepientId`,
	`account`.`parent_account` AS `accountParentId`,
	`account_type`.`account_type_name` AS `accountsclassfication`,
	`account`.`account_id` AS `accountId`,
	`account`.`account_name` AS `accountName`,
	CONCAT('NHIF') AS `transactionName`,
	CONCAT('NHIF') AS `transactionDescription`,
	  0 AS `department_id`,
	payroll_summary.nhif AS `dr_amount`,
	0 AS `cr_amount`,
	payroll_summary.payroll_created_for AS `transactionDate`,
	payroll_summary.payroll_created_for  AS `createdAt`,
	payroll_summary.payroll_created_for  AS `referenceDate`,
	`payroll`.`payroll_status` AS `status`,
	'NHIF' AS `transactionCategory`,
	'Payroll Expense' AS `transactionClassification`,
	'finance_purchase' AS `transactionTable`,
	'' AS `referenceTable`
FROM
payroll_summary,payroll,statutory_accounts,account,account_type
WHERE payroll.payroll_id = payroll_summary.payroll_id
AND statutory_accounts.statutory_account_id = 4
AND account.account_id = statutory_accounts.account_id
AND payroll.payroll_status = 1
AND account_type.account_type_id = account.account_type_id



UNION ALL
		-- payroll invoice payments

SELECT
			`payroll_payment_item`.`payroll_payment_item_id` AS `transactionId`,
			`payroll_payment`.`payroll_payment_id` AS `referenceId`,
			`payroll_payment_item`.`payroll_invoice_id` AS `payingFor`,
			CONCAT(`statutory_accounts`.`statutory_account_name`,'-',MONTH(payroll_summary.payroll_created_for),'-',YEAR(payroll_summary.payroll_created_for)) AS `referenceCode`,
			`payroll_payment`.`document_number` AS `transactionCode`,
			'' AS `patient_id`,
		  `statutory_accounts`.`statutory_account_id` AS `recepientId`,
			`account`.`parent_account` AS `accountParentId`,
			`account_type`.`account_type_name` AS `accountsclassfication`,
			`payroll_payment`.`account_from_id` AS `accountId`,
			`account`.`account_name` AS `accountName`,
			`payroll_payment_item`.`description` AS `transactionName`,
			CONCAT('Payment for invoice of ',' ',`payroll_summary`.`payroll_created_for`)  AS `transactionDescription`,
      		0 AS `department_id`,
			0 AS `dr_amount`,
			`payroll_payment_item`.`amount_paid` AS `cr_amount`,
			`payroll_payment`.`transaction_date` AS `transactionDate`,
			`payroll_payment`.`created` AS `createdAt`,
			`payroll_summary`.`payroll_created_for` AS `referenceDate`,
			`payroll_payment_item`.`payroll_payment_item_status` AS `status`,
			'Payroll Payment' AS `transactionCategory`,
			'Payroll Payment' AS `transactionClassification`,
			'payroll_payment' AS `transactionTable`,
			'payroll_payment_item' AS `referenceTable`
		FROM
			(
				(
					(
						`payroll_payment_item`,payroll_payment,payroll_summary,statutory_accounts,payroll
						
					)
					JOIN account ON(
						(
							account.account_id = payroll_payment.account_from_id
						)
					)
				)
				JOIN `account_type` ON(
					(
						account_type.account_type_id = account.account_type_id
					)
				)
				
			)
			WHERE payroll_payment_item.invoice_type = 0 
			AND payroll_payment.payroll_payment_id = payroll_payment_item.payroll_payment_id 
			AND payroll.payroll_id = payroll_summary.payroll_id
			AND payroll.payroll_status = 1
			AND payroll_payment.payroll_payment_status = 1
			AND payroll_summary.payroll_summary_id = payroll_payment_item.payroll_invoice_id
			AND statutory_accounts.statutory_account_id = payroll_payment_item.payroll_id

GROUP BY payroll_payment_item.payroll_payment_id 






UNION ALL

SELECT
	`payroll_invoice_item`.`statutory_invoice_item_id` AS `transactionId`,
	`payroll_invoice`.`statutory_invoice_id` AS `referenceId`,
	'' AS `payingFor`,
	`payroll_invoice`.`invoice_number` AS `referenceCode`,
	`payroll_invoice`.`document_number` AS `transactionCode`,
	'' AS `patient_id`,
  `payroll_invoice`.`statutory_id` AS `recepientId`,
	`account`.`parent_account` AS `accountParentId`,
	`account_type`.`account_type_name` AS `accountsclassfication`,
	`payroll_invoice_item`.`account_to_id` AS `accountId`,
	`account`.`account_name` AS `accountName`,
	`payroll_invoice_item`.`item_description` AS `transactionName`,
	`payroll_invoice_item`.`item_description` AS `transactionDescription`,
	
  	'0' AS `department_id`,
	SUM(`payroll_invoice_item`.`total_amount`) AS `dr_amount`,
	'0' AS `cr_amount`,
	`payroll_invoice`.`transaction_date` AS `transactionDate`,
	`payroll_invoice`.`created` AS `createdAt`,
	`payroll_invoice`.`transaction_date` AS `referenceDate`,
	`payroll_invoice_item`.`statutory_invoice_item_status` AS `status`,
	'Expense' AS `transactionCategory`,
	'Payroll Invoices' AS `transactionClassification`,
	'payroll_invoice_item' AS `transactionTable`,
	'payroll_invoice' AS `referenceTable`
FROM
	(
		(
			(
				`payroll_invoice_item`,payroll_invoice,statutory_accounts
				
			)
			JOIN account ON(
				(
					account.account_id = payroll_invoice_item.account_to_id
				)
			)
		)
		JOIN `account_type` ON(
			(
				account_type.account_type_id = account.account_type_id
			)
		)
	)
WHERE 
payroll_invoice.statutory_invoice_id = payroll_invoice_item.statutory_invoice_id 
AND payroll_invoice.statutory_invoice_status = 1

AND statutory_accounts.statutory_account_id = payroll_invoice.statutory_id 

GROUP BY `payroll_invoice_item`.`statutory_invoice_id` 

UNION ALL


SELECT
			`payroll_payment_item`.`payroll_payment_item_id` AS `transactionId`,
			`payroll_payment`.`payroll_payment_id` AS `referenceId`,
			`payroll_payment_item`.`payroll_invoice_id` AS `payingFor`,
			`payroll_payment`.`reference_number` AS `referenceCode`,
			`payroll_payment`.`document_number` AS `transactionCode`,
			'' AS `patient_id`,
		  `statutory_accounts`.`statutory_account_id` AS `recepientId`,
			`account`.`parent_account` AS `accountParentId`,
			`account_type`.`account_type_name` AS `accountsclassfication`,
			`payroll_payment`.`account_from_id` AS `accountId`,
			`account`.`account_name` AS `accountName`,
			`payroll_payment_item`.`description` AS `transactionName`,
			CONCAT('Payment for invoice of ',' ',`payroll_invoice`.`transaction_date`)  AS `transactionDescription`,
      		0 AS `department_id`,
			0 AS `dr_amount`,
			`payroll_payment_item`.`amount_paid` AS `cr_amount`,
			`payroll_payment`.`transaction_date` AS `transactionDate`,
			`payroll_payment`.`created` AS `createdAt`,
			`payroll_invoice`.`transaction_date` AS `referenceDate`,
			`payroll_payment_item`.`payroll_payment_item_status` AS `status`,
			'Payroll Payment' AS `transactionCategory`,
			'Payroll Payment' AS `transactionClassification`,
			'payroll_payment' AS `transactionTable`,
			'payroll_payment_item' AS `referenceTable`
		FROM
			(
				(
					(
						`payroll_payment_item`,payroll_payment,statutory_accounts,payroll_invoice
						
					)
					JOIN account ON(
						(
							account.account_id = payroll_payment.account_from_id
						)
					)
				)
				JOIN `account_type` ON(
					(
						account_type.account_type_id = account.account_type_id
					)
				)
				
			)
			WHERE payroll_payment_item.invoice_type = 1
			AND payroll_payment.payroll_payment_id = payroll_payment_item.payroll_payment_id 
			AND payroll_payment.payroll_payment_status = 1
			AND payroll_invoice.statutory_invoice_id = payroll_payment_item.payroll_invoice_id 
			AND statutory_accounts.statutory_account_id = payroll_invoice.statutory_id

GROUP BY payroll_payment_item.payroll_payment_id 


;

CREATE OR REPLACE VIEW v_payroll_ledger_aging_by_date AS SELECT * FROM v_payroll_ledger_aging ORDER BY transactionDate ASC;




CREATE OR REPLACE VIEW v_aged_statutories AS
-- Creditr Invoices
SELECT
	statutory_accounts.statutory_account_id AS recepientId,
	statutory_accounts.statutory_account_name as payables,
	2 as branch_id,
  (
    CASE
      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_payroll_ledger_aging.referenceDate ) )  = 0, v_payroll_ledger_aging.dr_amount, 0 )) = 0 -- condtion to see if the date has no bills
       THEN 0 -- Output
      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_payroll_ledger_aging.referenceDate ) )  = 0, v_payroll_ledger_aging.dr_amount, 0 )) > 0 -- Checking if the value of bills in the date range is greater than Zero
       THEN
       -- Add the paymensts made to the specifc invoice
        Sum(IF( DATEDIFF( CURDATE( ), date( v_payroll_ledger_aging.referenceDate ) )  = 0, v_payroll_ledger_aging.dr_amount, 0 ))
        - Sum(IF( DATEDIFF( CURDATE( ), date( v_payroll_ledger_aging.referenceDate ) )  = 0, v_payroll_ledger_aging.cr_amount, 0 ))
        END
  ) AS `coming_due`,
	(
    CASE
      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_payroll_ledger_aging.referenceDate ) ) BETWEEN 1 AND 30, v_payroll_ledger_aging.dr_amount, 0 )) = 0 -- condtion to see if the date has no bills
       THEN 0 -- Output
      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_payroll_ledger_aging.referenceDate ) ) BETWEEN 1 AND 30, v_payroll_ledger_aging.dr_amount, 0 )) > 0 -- Checking if the value of bills in the date range is greater than Zero
       THEN
       -- Add the paymensts made to the specifc invoice
        Sum(IF( DATEDIFF( CURDATE( ), date( v_payroll_ledger_aging.referenceDate ) ) BETWEEN 1 AND 30, v_payroll_ledger_aging.dr_amount, 0 ))
				  - Sum(IF( DATEDIFF( CURDATE( ), date( v_payroll_ledger_aging.referenceDate ) )  BETWEEN 1 AND 30, v_payroll_ledger_aging.cr_amount, 0 ))
			END
  ) AS `thirty_days`,
  (
    CASE
      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_payroll_ledger_aging.referenceDate ) ) BETWEEN 31 AND 60, v_payroll_ledger_aging.dr_amount, 0 )) = 0 -- condtion to see if the date has no bills
       THEN 0 -- Output
      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_payroll_ledger_aging.referenceDate ) ) BETWEEN 31 AND 60, v_payroll_ledger_aging.dr_amount, 0 )) > 0 -- Checking if the value of bills in the date range is greater than Zero
       THEN
       -- Add the paymensts made to the specifc invoice
        Sum(IF( DATEDIFF( CURDATE( ), date( v_payroll_ledger_aging.referenceDate ) ) BETWEEN 31 AND 60, v_payroll_ledger_aging.dr_amount, 0 ))
				   - Sum(IF( DATEDIFF( CURDATE( ), date( v_payroll_ledger_aging.referenceDate ) ) BETWEEN 31 AND 60, v_payroll_ledger_aging.cr_amount, 0 ))
			END
  ) AS `sixty_days`,
  (
    CASE
      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_payroll_ledger_aging.referenceDate ) ) BETWEEN 61 AND 90, v_payroll_ledger_aging.dr_amount, 0 )) = 0 -- condtion to see if the date has no bills
       THEN 0 -- Output
      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_payroll_ledger_aging.referenceDate ) ) BETWEEN 61 AND 90, v_payroll_ledger_aging.dr_amount, 0 )) > 0 -- Checking if the value of bills in the date range is greater than Zero
       THEN
       -- Add the paymensts made to the specifc invoice
        Sum(IF( DATEDIFF( CURDATE( ), date( v_payroll_ledger_aging.referenceDate ) ) BETWEEN 61 AND 90, v_payroll_ledger_aging.dr_amount, 0 ))
				   - Sum(IF( DATEDIFF( CURDATE( ), date( v_payroll_ledger_aging.referenceDate ) )  BETWEEN 61 AND 90, v_payroll_ledger_aging.cr_amount, 0 ))
		END
  ) AS `ninety_days`,
  (
    CASE
      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_payroll_ledger_aging.referenceDate ) ) >90, v_payroll_ledger_aging.dr_amount, 0 )) = 0 -- condtion to see if the date has no bills
       THEN 0 -- Output
      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_payroll_ledger_aging.referenceDate ) ) >90, v_payroll_ledger_aging.dr_amount, 0 )) > 0 -- Checking if the value of bills in the date range is greater than Zero
       THEN
       -- Add the paymensts made to the specifc invoice
        Sum(IF( DATEDIFF( CURDATE( ), date( v_payroll_ledger_aging.referenceDate ) ) >90, v_payroll_ledger_aging.dr_amount, 0 ))
				 - Sum(IF( DATEDIFF( CURDATE( ), date( v_payroll_ledger_aging.referenceDate ) )  >90, v_payroll_ledger_aging.cr_amount, 0 ))
			END
  ) AS `over_ninety_days`,

  (

    (
      CASE
        WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_payroll_ledger_aging.referenceDate ) )  = 0, v_payroll_ledger_aging.dr_amount, 0 )) = 0 -- condtion to see if the date has no bills
         THEN 0 -- Output
        WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_payroll_ledger_aging.referenceDate ) )  = 0, v_payroll_ledger_aging.dr_amount, 0 )) > 0 -- Checking if the value of bills in the date range is greater than Zero
         THEN
         -- Add the paymensts made to the specifc invoice
          Sum(IF( DATEDIFF( CURDATE( ), date( v_payroll_ledger_aging.referenceDate ) )  = 0, v_payroll_ledger_aging.dr_amount, 0 ))
					 - Sum(IF( DATEDIFF( CURDATE( ), date( v_payroll_ledger_aging.referenceDate ) )  = 0, v_payroll_ledger_aging.cr_amount, 0 ))
  		END
    )-- Getting the Value for 0 Days
    + (
      CASE
        WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_payroll_ledger_aging.referenceDate ) ) BETWEEN 1 AND 30, v_payroll_ledger_aging.dr_amount, 0 )) = 0 -- condtion to see if the date has no bills
         THEN 0 -- Output
        WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_payroll_ledger_aging.referenceDate ) ) BETWEEN 1 AND 30, v_payroll_ledger_aging.dr_amount, 0 )) > 0 -- Checking if the value of bills in the date range is greater than Zero
         THEN
         -- Add the paymensts made to the specifc invoice
          Sum(IF( DATEDIFF( CURDATE( ), date( v_payroll_ledger_aging.referenceDate ) ) BETWEEN 1 AND 30, v_payroll_ledger_aging.dr_amount, 0 ))
 - Sum(IF( DATEDIFF( CURDATE( ), date( v_payroll_ledger_aging.referenceDate ) )  BETWEEN 1 AND 30, v_payroll_ledger_aging.cr_amount, 0 ))
  		END
    ) --  AS `1-30 Days`
    +(
      CASE
        WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_payroll_ledger_aging.referenceDate ) ) BETWEEN 31 AND 60, v_payroll_ledger_aging.dr_amount, 0 )) = 0 -- condtion to see if the date has no bills
         THEN 0 -- Output
        WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_payroll_ledger_aging.referenceDate ) ) BETWEEN 31 AND 60, v_payroll_ledger_aging.dr_amount, 0 )) > 0 -- Checking if the value of bills in the date range is greater than Zero
         THEN
         -- Add the paymensts made to the specifc invoice
          Sum(IF( DATEDIFF( CURDATE( ), date( v_payroll_ledger_aging.referenceDate ) ) BETWEEN 31 AND 60, v_payroll_ledger_aging.dr_amount, 0 ))

				   - Sum(IF( DATEDIFF( CURDATE( ), date( v_payroll_ledger_aging.referenceDate ) )  BETWEEN 31 AND 60, v_payroll_ledger_aging.cr_amount, 0 ))
				END

    ) -- AS `31-60 Days`
    +(
      CASE
        WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_payroll_ledger_aging.referenceDate ) ) BETWEEN 61 AND 90, v_payroll_ledger_aging.dr_amount, 0 )) = 0 -- condtion to see if the date has no bills
         THEN 0 -- Output
        WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_payroll_ledger_aging.referenceDate ) ) BETWEEN 61 AND 90, v_payroll_ledger_aging.dr_amount, 0 )) > 0 -- Checking if the value of bills in the date range is greater than Zero
         THEN
         -- Add the paymensts made to the specifc invoice
          Sum(IF( DATEDIFF( CURDATE( ), date( v_payroll_ledger_aging.referenceDate ) ) BETWEEN 61 AND 90, v_payroll_ledger_aging.dr_amount, 0 ))
				   - Sum(IF( DATEDIFF( CURDATE( ), date( v_payroll_ledger_aging.referenceDate ) ) BETWEEN 61 AND 90, v_payroll_ledger_aging.cr_amount, 0 ))
  		END
    ) -- AS `61-90 Days`
    +(
      CASE
        WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_payroll_ledger_aging.referenceDate ) ) >90, v_payroll_ledger_aging.dr_amount, 0 )) = 0 -- condtion to see if the date has no bills
         THEN 0 -- Output
        WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_payroll_ledger_aging.referenceDate ) ) >90, v_payroll_ledger_aging.dr_amount, 0 )) > 0 -- Checking if the value of bills in the date range is greater than Zero
         THEN
         -- Add the paymensts made to the specifc invoice
          Sum(IF( DATEDIFF( CURDATE( ), date( v_payroll_ledger_aging.referenceDate ) ) >90, v_payroll_ledger_aging.dr_amount, 0 ))
				  - Sum(IF( DATEDIFF( CURDATE( ), date( v_payroll_ledger_aging.referenceDate ) )  >90, v_payroll_ledger_aging.cr_amount, 0 ))
  		END
    ) -- AS `>90 Days`
  ) AS `Total`,
SUM(v_payroll_ledger_aging.dr_amount) AS total_dr,
SUM(v_payroll_ledger_aging.cr_amount) AS total_cr

	FROM
		statutory_accounts,v_payroll_ledger_aging,account
	WHERE v_payroll_ledger_aging.recepientId = statutory_accounts.statutory_account_id  
	AND account.account_id = statutory_accounts.account_id
	 GROUP BY statutory_accounts.statutory_account_id;